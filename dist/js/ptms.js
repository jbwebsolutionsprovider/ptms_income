function convertToSlug(Text){
	return Text
	.toLowerCase()
	.replace(/[^\w ]+/g,'')
	.replace(/ +/g,'-')
	;
}

/**
* function: initializeCombo()
* parameter:  string container (combo id)
* description: this will Initialize combo box
**/
function initializeCombo(container, default_text){
	var y=document.createElement('option');
	document.getElementById(container).innerHTML = '';
	y.setAttribute('value','');
	y.text=default_text;
	var x=document.getElementById(container);
	//x.add(y,null); // IE only  }
	if (navigator.appName=="Microsoft Internet Explorer") {
		x.add(y); // IE only  
	} else {
		x.add(y,null);
	}
	return;
}