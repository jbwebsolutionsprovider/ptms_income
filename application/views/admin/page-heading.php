<div class="content-wrapper">
	<section class="content-header">
		<h1>
			<?php echo $page_title ?>
			<small><?php echo $page_sub_title ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard/view') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"><?php echo $module_name ?></li>
		</ol>
	</section>