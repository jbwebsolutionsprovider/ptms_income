			</div>
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b> 1.0
				</div>
				<strong>Copyright &copy; 2014-<?php echo date('Y') ?> <a href="<?php echo site_url() ?>">PTMS Income Monitoring System</a>.</strong> All rights reserved.
			</footer>
		</div>

		<!-- Bootstrap 3.3.2 JS -->
		<script src="<?php echo base_url('bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
		<!-- SlimScroll -->
		<script src="<?php echo base_url('plugins/slimScroll/jquery.slimScroll.min.js') ?>" type="text/javascript"></script>
		<!-- FastClick -->
		<script src='<?php echo base_url('plugins/fastclick/fastclick.min.js') ?>'></script>
		<!-- AdminLTE App -->
		<script src="<?php echo base_url('dist/js/app.min.js') ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('dist/js/ptms.js') ?>" type="text/javascript"></script>

	</body>
</html>