<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Admin Login | PTMS Income Monitoring System</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link href="<?php echo base_url('bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('dist/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('dist/css/ptms.min.css') ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('dist/css/style.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('plugins/iCheck/square/blue.css') ?>" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="login-page">
		<div class="login-box">
			<div class="login-logo">
				<div><img src="<?php echo base_url('dist/img/logo.png') ?>" alt="PTMS"></div>
				<div><a href="#"><b>PTMS Income</b> Monitoring System</a></div>
			</div>
			<div class="login-box-body">
				<p class="login-box-msg">Sign in to start your session</p>
				<form action="" method="post">
					<?php
						if($message){
					?>
							<div class="alert alert-info alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-check"></i> User Account</h4>
								<p><?php echo $message ?></p>
							</div>
					<?php
						}
					?>
					<?php
						$error_message = strlen(validation_errors());
						if($error_message > 0){
					?>
							<div class="alert alert-danger alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-ban"></i> Error</h4>
								<?php echo validation_errors(); ?>
							</div>
					<?php
						}
					?>
					<div class="form-group has-feedback">
						<input type="text" class="form-control" name="username" placeholder="Username"/>
							<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" class="form-control" name="userpassword" placeholder="Password"/>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-8">    
							<div class="checkbox icheck">
								<label>
									<input type="checkbox"> Remember Me
								</label>
							</div>
						</div>
						<div class="col-xs-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<script src="<?php echo base_url('plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
		<script src="<?php echo base_url('bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
		<script src="<?php echo base_url('plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>
		<script>
			$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
					increaseArea: '20%'
				});
			});
		</script>
	</body>
</html>