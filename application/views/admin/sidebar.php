<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo base_url('dist/img/avatar5.png') ?>" class="img-circle" alt="User Image" />
			</div>
			<div class="pull-left info">
				<p><?php echo $this->session->userdata('firstname') ?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>

			<?php
				if($this->session->userdata('has_role_dashboard') == 'yes'){
			?>
					<li class="treeview <?php if($this->uri->segment(1) == 'dashboard'){ echo 'active'; } ?>">
						<a href="#">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li <?php if($this->uri->segment(1) == 'dashboard' && $this->uri->segment(2) == 'view'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('dashboard/view') ?>"><i class="fa fa-circle-o"></i> Home Dashboard</a></li>
							<li <?php if($this->uri->segment(1) == 'dashboard' && $this->uri->segment(2) == 'income_summary'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('dashboard/income_summary') ?>"><i class="fa fa-circle-o"></i> Income Summary</a></li>
							<li <?php if($this->uri->segment(1) == 'dashboard' && $this->uri->segment(2) == 'billing_monitoring'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('dashboard/billing_monitoring') ?>"><i class="fa fa-circle-o"></i> Billing Monitoring</a></li>
							<li <?php if($this->uri->segment(1) == 'dashboard' && $this->uri->segment(2) == 'payment_monitoring'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('dashboard/payment_monitoring') ?>"><i class="fa fa-circle-o"></i> Payment Monitoring</a></li>
						</ul>
					</li>
			<?php
				}
			?>

			<?php
				if($this->session->userdata('has_role_forms') == 'yes'){
			?>
					<li class="treeview <?php if($this->uri->segment(1) == 'income' || $this->uri->segment(1) == 'billing' || $this->uri->segment(1) == 'payment'){ echo 'active'; } ?>">
						<a href="#">
							<i class="fa fa-building"></i> <span>Forms</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li <?php if($this->uri->segment(1) == 'income' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('income/listing') ?>"><i class="fa fa-circle-o"></i> List of Income</a></li>
							<li <?php if($this->uri->segment(1) == 'income' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('income/add') ?>"><i class="fa fa-circle-o"></i> Add Income</a></li>
							<li <?php if($this->uri->segment(1) == 'billing' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('billing/listing') ?>"><i class="fa fa-circle-o"></i> List of Billing</a></li>
							<li <?php if($this->uri->segment(1) == 'billing' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('billing/add') ?>"><i class="fa fa-circle-o"></i> Add Billing</a></li>
							<li <?php if($this->uri->segment(1) == 'payment' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('payment/listing') ?>"><i class="fa fa-circle-o"></i> List of Payments</a></li>
							<li <?php if($this->uri->segment(1) == 'payment' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('payment/add') ?>"><i class="fa fa-circle-o"></i> Add Payment</a></li>
						</ul>
					</li>
			<?php
				}
			?>

			<?php
				if($this->session->userdata('has_role_settings') == 'yes'){
			?>
					<li class="treeview <?php if($this->uri->segment(1) == 'client' || $this->uri->segment(1) == 'project' || $this->uri->segment(1) == 'income_type' || $this->uri->segment(1) == 'income_category' || $this->uri->segment(1) == 'unit_type' || $this->uri->segment(1) == 'company' || $this->uri->segment(1) == 'transport' || $this->uri->segment(1) == 'place' || $this->uri->segment(1) == 'cargo' || $this->uri->segment(1) == 'bank' || $this->uri->segment(1) == 'mode_payment' || $this->uri->segment(1) == 'student'){ echo 'active'; } ?>">
						<a href="#">
							<i class="fa fa-gear"></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li <?php if($this->uri->segment(1) == 'project'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Projects
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'project' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('project/listing') ?>"><i class="fa fa-circle-o"></i> List of Projects</a></li>
									<li <?php if($this->uri->segment(1) == 'project' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('project/add') ?>"><i class="fa fa-circle-o"></i> Add Project</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'company'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Companies
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'company' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('company/listing') ?>"><i class="fa fa-circle-o"></i> List of Company</a></li>
									<li <?php if($this->uri->segment(1) == 'company' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('company/add') ?>"><i class="fa fa-circle-o"></i> Add Company</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'student'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Students
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'student' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('student/listing') ?>"><i class="fa fa-circle-o"></i> List of Students</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'client'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Clients
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'client' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('client/listing') ?>"><i class="fa fa-circle-o"></i> List of Clients</a></li>
									<li <?php if($this->uri->segment(1) == 'client' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('client/add') ?>"><i class="fa fa-circle-o"></i> Add Client</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'mode_payment'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Mode of Payment
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'mode_payment' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('mode_payment/listing') ?>"><i class="fa fa-circle-o"></i> List of Mode Payment</a></li>
									<li <?php if($this->uri->segment(1) == 'mode_payment' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('mode_payment/add') ?>"><i class="fa fa-circle-o"></i> Add Mode Payment</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'income_type'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Income Type
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'income_type' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('income_type/listing') ?>"><i class="fa fa-circle-o"></i> List of Income Type</a></li>
									<li <?php if($this->uri->segment(1) == 'income_type' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('income_type/add') ?>"><i class="fa fa-circle-o"></i> Add Income Type</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'income_category'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Income Category
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'income_category' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('income_category/listing') ?>"><i class="fa fa-circle-o"></i> List of Income Category</a></li>
									<li <?php if($this->uri->segment(1) == 'income_category' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('income_category/add') ?>"><i class="fa fa-circle-o"></i> Add Income Category</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'unit_type'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Unit Type
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'unit_type' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('unit_type/listing') ?>"><i class="fa fa-circle-o"></i> List of Unit Type</a></li>
									<li <?php if($this->uri->segment(1) == 'unit_type' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('unit_type/add') ?>"><i class="fa fa-circle-o"></i> Add Unit Type</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'transport'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Transport
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'transport' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('transport/listing') ?>"><i class="fa fa-circle-o"></i> List of Transport</a></li>
									<li <?php if($this->uri->segment(1) == 'transport' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('transport/add') ?>"><i class="fa fa-circle-o"></i> Add Transport</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'place'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Place
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'place' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('place/listing') ?>"><i class="fa fa-circle-o"></i> List of Place</a></li>
									<li <?php if($this->uri->segment(1) == 'place' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('place/add') ?>"><i class="fa fa-circle-o"></i> Add Place</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'cargo'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Cargo
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'cargo' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('cargo/listing') ?>"><i class="fa fa-circle-o"></i> List of Cargo</a></li>
									<li <?php if($this->uri->segment(1) == 'cargo' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('cargo/add') ?>"><i class="fa fa-circle-o"></i> Add Cargo</a></li>
								</ul>
							</li>
							<li <?php if($this->uri->segment(1) == 'bank'){ echo 'class="active"'; } ?>>
								<a href="#">
									<i class="fa fa-circle"></i>
										Bank
									<i class="fa fa-angle-left pull-right"></i>
								</a>
								<ul class="treeview-menu">
									<li <?php if($this->uri->segment(1) == 'bank' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('bank/listing') ?>"><i class="fa fa-circle-o"></i> List of Bank</a></li>
									<li <?php if($this->uri->segment(1) == 'bank' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('bank/add') ?>"><i class="fa fa-circle-o"></i> Add Bank</a></li>
								</ul>
							</li>
						</ul>
					</li>
			<?php
				}
			?>

			<?php
				if($this->session->userdata('has_role_reports') == 'yes'){
			?>
					<li class="treeview <?php if($this->uri->segment(1) == 'report'){ echo 'active'; } ?>">
						<a href="#">
							<i class="fa fa-list-alt"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li <?php if($this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'summary'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('report/summary') ?>"><i class="fa fa-circle-o"></i> Summary</a></li>
							<li <?php if($this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'company_summary'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('report/company_summary') ?>"><i class="fa fa-circle-o"></i> Company Summary</a></li>
							<li <?php if($this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'transport_summary'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('report/transport_summary') ?>"><i class="fa fa-circle-o"></i> Transport Summary</a></li>
							<li <?php if($this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'transport_summary_monthly'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('report/transport_summary_monthly') ?>"><i class="fa fa-circle-o"></i> Transport Summary Monthly</a></li>
						</ul>
					</li>
			<?php
				}
			?>

			<?php
				if($this->session->userdata('has_role_user') == 'yes'){
			?>
					<li class="treeview <?php if($this->uri->segment(1) == 'user_group' || $this->uri->segment(1) == 'user'){ echo 'active'; } ?>">
						<a href="#">
							<i class="fa fa-users"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li <?php if($this->uri->segment(1) == 'user' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('user/listing') ?>"><i class="fa fa-circle-o"></i> List of User</a></li>
							<li <?php if($this->uri->segment(1) == 'user' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('user/add') ?>"><i class="fa fa-circle-o"></i> Add User</a></li>
							<li <?php if($this->uri->segment(1) == 'user_group' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('user_group/listing') ?>"><i class="fa fa-circle-o"></i> List of User Group</a></li>
							<li <?php if($this->uri->segment(1) == 'user_group' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('user_group/add') ?>"><i class="fa fa-circle-o"></i> Add User Group</a></li>
						</ul>
					</li>
			<?php
				}
			?>

			<?php
				if($this->session->userdata('has_role_admin') == 'yes'){
			?>
					<li class="treeview <?php if($this->uri->segment(1) == 'role' || $this->uri->segment(1) == 'module'){ echo 'active'; } ?>">
						<a href="#">
							<i class="fa fa-gears"></i> <span>Administration</span> <i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li <?php if($this->uri->segment(1) == 'module' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('module/listing') ?>"><i class="fa fa-circle-o"></i> List of Modules</a></li>
							<li <?php if($this->uri->segment(1) == 'module' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('module/add') ?>"><i class="fa fa-circle-o"></i> Add Module</a></li>
							<li <?php if($this->uri->segment(1) == 'role' && $this->uri->segment(2) == 'listing'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('role/listing') ?>"><i class="fa fa-circle-o"></i> List of Roles</a></li>
							<li <?php if($this->uri->segment(1) == 'role' && $this->uri->segment(2) == 'add'){ echo 'class="active"'; } ?>><a href="<?php echo site_url('role/add') ?>"><i class="fa fa-circle-o"></i> Add Role</a></li>
						</ul>
					</li>
			<?php
				}
			?>
		</ul>
	</section>
</aside>