<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">No Access</h3>
		</div>
		<div class="box-body">
			Sorry. You don't have access to this page.
		</div>
	</div>
</section>