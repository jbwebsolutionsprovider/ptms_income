<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>PTMS Income Monitoring System | MyAdmin</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link href="<?php echo base_url('bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('dist/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('dist/css/ionicons.min.css') ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('dist/css/ptms.min.css') ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('dist/css/skins/_all-skins.min.css') ?>" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url('plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
		<script src="<?php echo base_url('plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>
		<link href="<?php echo base_url('plugins/iCheck/square/blue.css') ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url('plugins/iCheck/all.css') ?>" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="skin-blue fixed">
		<div class="wrapper">
			<header class="main-header">
				<a href="<?php echo site_url('dashboard/view') ?>" class="logo"><img src="<?php echo base_url('dist/img/logo_small.png') ?>" alt="PTMS"></a>
				<nav class="navbar navbar-static-top" role="navigation">
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo base_url('dist/img/avatar5.png') ?>" class="user-image" alt="User Image"/>
									<span class="hidden-xs"><?php echo $this->session->userdata('firstname').' '.$this->session->userdata('lastname') ?></span>
								</a>
								<ul class="dropdown-menu">
									<li class="user-header">
										<img src="<?php echo base_url('dist/img/avatar5.png') ?>" class="img-circle" alt="User Image" />
										<p>
											<?php echo $this->session->userdata('firstname').' '.$this->session->userdata('lastname') ?>
										</p>
									</li>
									<li class="user-footer">
										<div class="pull-right">
											<a href="<?php echo site_url('logout') ?>" class="btn btn-default btn-flat">Sign out</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>