<!-- Main content -->
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<link href="<?php echo base_url('dist/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('plugins/iCheck/square/blue.css') ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$('#expiry').datepicker();
</script>
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
					</div>
					<div class="box-header">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="firstname" class="col-sm-4 control-label">Firstname <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('firstname')){
										$value = set_value('firstname');
									} else {
										$value = $firstname;
									}
								?>
								<input type="text" class="form-control" name="firstname" id="firstname" value="<?php echo $value ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="lastname" class="col-sm-4 control-label">Lastname <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('lastname')){
										$value = set_value('lastname');
									} else {
										$value = $lastname;
									}
								?>
								<input type="text" class="form-control" name="lastname" id="lastname" value="<?php echo $value ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="expiry" class="col-sm-4 control-label">Expiry <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('expiry')){
										$value = set_value('expiry');
									} else {
										$value = $expiry;
									}
								?>
								<input type="text" class="form-control" data-provide="datepicker" name="expiry" id="expiry" value="<?php echo $value ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="company_id" class="col-sm-4 control-label">Company</label>
							<div class="col-sm-8">
								<?php
									if(is_array($companies)){
										foreach($companies as $company){
									?>
											<div class="checkbox icheck">
												<label>
													<input type="checkbox" name="company[]" id="company[]" value="<?php echo $company['id'] ?>"
													<?php
														if(is_array($user_companies)){
															foreach($user_companies as $user_company){
																if($user_company['id'] == $company['id']){
																	echo 'checked="checked"';
																}
															}
														}
													?>
													> <?php echo $company['company_name'] ?>
												</label>
											</div>
									<?php
										}
									}
								?>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	});
</script>