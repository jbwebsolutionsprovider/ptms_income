<!-- Main content -->
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<link href="<?php echo base_url('dist/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('plugins/iCheck/square/blue.css') ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$('#expiry').datepicker();
</script>
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
					</div>
					<div class="box-header">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="username" class="col-sm-4 control-label">User Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="username" id="username" value="<?php echo set_value('username') ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="user_password" class="col-sm-4 control-label">Password <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="password" class="form-control" name="user_password" id="user_password" value="<?php echo set_value('user_password') ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="firstname" class="col-sm-4 control-label">Firstname</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="firstname" id="firstname" value="<?php echo set_value('firstname') ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="lastname" class="col-sm-4 control-label">Lastname</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="lastname" id="lastname" value="<?php echo set_value('lastname') ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="user_group" class="col-sm-4 control-label">Account Type <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(is_array($user_groups)){
								?>
										<select name="user_group" id="user_group" class="form-control">
											<option value=""> - Select User Group - </option>
											<?php
												foreach($user_groups as $user_group){
											?>
													<option value="<?php echo $user_group['id'] ?>" <?php if(set_value('user_group') == $user_group['id']){ echo 'selected="selected"'; } ?>><?php echo $user_group['user_group_name'] ?></option>
											<?php
												}
											?>
										</select>
								<?php
									}
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="expiry" class="col-sm-4 control-label">Expiry</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" data-provide="datepicker" name="expiry" id="expiry" value="" />
							</div>
						</div>
						<div class="form-group">
							<label for="company_id" class="col-sm-4 control-label">Company</label>
							<div class="col-sm-8">
								<?php
									if(is_array($companies)){
										foreach($companies as $company){
									?>
											<div class="checkbox icheck">
												<label>
													<input type="checkbox" name="company[]" id="company[]" value="<?php echo $company['id'] ?>"> <?php echo $company['company_name'] ?>
												</label>
											</div>
									<?php
										}
									}
								?>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	});
</script>