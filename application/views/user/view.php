<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Personal Information</h3>
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td width="25%"><strong>Firstname</strong></td>
								<td width="25%"><?php echo $firstname ?></td>
								<td width="25%"><strong>Lastname</strong></td>
								<td width="25%"><?php echo $lastname ?></td>
							</tr>
							<tr>
								<td><strong>Company</strong></td>
								<td><?php echo $company_name ?></td>
								<td><strong>Status</strong></td>
								<td><?php echo ucfirst($status) ?></td>
							</tr>
							<tr>
								<td width="25%"><strong>User Group</strong></td>
								<td width="75%" colspan="3"><?php echo $user_group_name ?></td>
							</tr>
						</tbody>
					</table>
					<hr />
					<div class="box-header">
						<h3 class="box-title">Login Information</h3>
					</div>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td width="25%"><strong>Username</strong></td>
								<td width="25%"><?php echo $username ?></td>
								<td width="25%"><strong>Password</strong></td>
								<td width="25%">****************</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">User Option</h3>
				</div>
				<div class="box-body">
					<?php
						if($reset_password_role == 'yes'){
					?>
							<button id="cmdResetPassword" class="btn btn-info btn-sm btn-flat">Reset Password</button>
					<?php
						}
					?>

					<?php
						if($status == 'active'){
					?>
							<?php
								if($deactivate_role == 'yes'){
							?>
									<button id="cmdDeactivate" class="btn btn-info btn-sm btn-flat">Deactivate</button>
							<?php
								}
							?>

					<?php
						} else {
					?>
							<?php
								if($activate_role == 'yes'){
							?>
									<button id="cmdActivate" class="btn btn-info btn-sm btn-flat">Activate</button>
							<?php
								}
							?>

					<?php
						}
					?>
				</div>
			</div>

			<div class="modal fade modal-primary" id="deactivate_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title">Deactivate User Account</h4>
						</div>
						<div class="modal-body">
							<p>Proceed in deactivating the user account?</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success" id="cmdYesDeactivate">Yes</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>

			<script type="text/javascript">
				$('#cmdDeactivate').click(
					function(){
						$('#deactivate_modal').modal('show');
					}
				)

				$('#cmdYesDeactivate').click(
					function(){
						$.ajax({
							url: '<?php echo site_url('user/deactivate') ?>',
							type: 'POST',
							data: {
									user_id: <?php echo $user_id ?>
							},
							success: function(data){
								window.location = '<?php echo current_url() ?>';
							}
						})
					}
				)
			</script>

			<div class="modal fade modal-primary" id="activate_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title">Activate User Account</h4>
						</div>
						<div class="modal-body">
							<p>Proceed in activating the user account?</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success" id="cmdYesActivate">Yes</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
						</div>
					</div>
				</div>
			</div>

			<script type="text/javascript">
				$('#cmdActivate').click(
					function(){
						$('#activate_modal').modal('show');
					}
				)

				$('#cmdYesActivate').click(
					function(){
						$.ajax({
							url: '<?php echo site_url('user/activate') ?>',
							type: 'POST',
							data: {
									user_id: <?php echo $user_id ?>
							},
							success: function(data){
								window.location = '<?php echo current_url() ?>';
							}
						})
					}
				)
			</script>

		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">User Roles</h3>
				</div>
				<div class="box-body">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#existing" data-toggle="tab">Existing Roles</a></li>
							<li><a href="#add_roles" data-toggle="tab">Add Roles</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="existing">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th width="5%">#</th>
											<th>Module Name</th>
											<th>Role Name</th>
											<th width="20%">Action</th>
										</tr>
									</thead>
									<?php
										if(is_array($user_roles)){
											$cnt = 1;
									?>
											<tbody>
												<?php
													foreach($user_roles as $user_role){
												?>
														<tr>
															<td><?php echo $cnt ?>.</td>
															<td><?php echo $user_role['module_name'] ?></td>
															<td><?php echo $user_role['role_name'] ?></td>
															<td><button class="btn btn-info btn-sm btn-flat" onclick="delete_item(<?php echo $user_role['id'] ?>)">Remove</button></td>
														</tr>
												<?php
														$cnt++;
													}
												?>
											</tbody>
									<?php
										}
									?>
								</table>
							</div>
							<div class="tab-pane" id="add_roles">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th width="5%">#</th>
											<th>Module Name</th>
											<th>Role Name</th>
											<th width="20%">Action</th>
										</tr>
									</thead>
									<?php
										if(is_array($roles)){
											$cnt = 1;
									?>
											<tbody>
												<?php
													foreach($roles as $role){
												?>
														<tr>
															<td><?php echo $cnt ?>.</td>
															<td><?php echo $role['module_name'] ?></td>
															<td><?php echo $role['role_name'] ?></td>
															<td><button class="btn btn-info btn-sm btn-flat" onclick="add_item(<?php echo $role['id'] ?>)">Add</button></td>
														</tr>
												<?php
														$cnt++;
													}
												?>
											</tbody>
									<?php
										}
									?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="user_role_id" id="user_role_id" value="" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete User Role</h4>
			</div>
			<div class="modal-body">
				<p>Proceed in deleting the user role?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDelete">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(user_role_id){
		$('#user_role_id').val(user_role_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDelete').click(
		function(){
			$('#delete_modal').modal('hide');
			$.ajax({
				url: '<?php echo site_url('user/remove_role') ?>',
				type: 'POST',
				data: {
						user_role_id: $('#user_role_id').val()
				},
				success: function(data){
					window.location = '<?php echo current_url() ?>';
				}
			})
		}
	)

	function add_item(user_role_id){
		$.ajax({
			url: '<?php echo site_url('user/add_role') ?>',
			type: 'POST',
			data: {
					user_id: '<?php echo $user_id ?>',
					user_role_id: user_role_id
			},
			success: function(){
				window.location = '<?php echo current_url() ?>';
			}
		})
	}
</script>

<div class="modal fade modal-primary" id="reset_password_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Reset Password</h4>
			</div>
			<form role="form" class="form-horizontal" method="POST">
				<div class="modal-body">
					<div id="display_reset_password_message" class="hide">
						<div class="alert alert-danger alert-dismissable margin-top-30">
							<i class="fa fa-info"></i>
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<p></p>
						</div>
					</div>
					<div class="form-group">
						<label for="new_password" class="col-sm-4 control-label">New Password <span class="required">*</span></label>
						<div class="col-sm-8">
							<input type="password" class="form-control" name="new_password" id="new_password" value="" />
						</div>
					</div>
					<div class="form-group">
						<label for="retype_password" class="col-sm-4 control-label">Retype Password <span class="required">*</span></label>
						<div class="col-sm-8">
							<input type="password" class="form-control" name="retype_password" id="retype_password" value="" />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="cmdReset">Save New Password</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade modal-primary" id="reset_password_modal_message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Reset Password</h4>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#cmdResetPassword').click(
		function(){
			$('#reset_password_modal').modal('show');
		}
	)

	$('#cmdReset').click(
		function(){
			var error = 0;
			var message = '';
			if($('#new_password').val() == ''){
				error = 1;
				message = 'Please enter new password.<br>';
			}

			if($('#retype_password').val() == ''){
				error = 1;
				message = message + 'Please retype password.';
			}

			if(error == 1){
				$('#display_reset_password_message').removeClass('hide');
				$('#display_reset_password_message p').html(message);
			} else {
				// check if password are match
				if($('#new_password').val() != $('#retype_password').val()){
					$('#display_reset_password_message').removeClass('hide');
					$('#display_reset_password_message p').html('Password mismatch. Please re-check the password.');
				} else {
					$.ajax({
						url: '<?php echo site_url('user/change_password') ?>',
						type: 'POST',
						data: {
								user_id: '<?php echo $user_id ?>',
								password: $('#new_password').val()
						},
						success: function(result){
							if(result == 'SUCCESS'){
								$('#new_password').val('');
								$('#retype_password').val('');
								$('#reset_password_modal').modal('hide');
								$('#reset_password_modal_message').modal('show');
								$('#reset_password_modal_message p').html('Password successfully changed.');
								setTimeout('hide_modal()', 3000);
							}
						}
					})
				}
			}
		}
	)

	function hide_modal(){
		$('#reset_password_modal_message').modal('hide');
	}
</script>