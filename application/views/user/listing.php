<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="5%">#</th>
								<th>Username</th>
								<th>User Group</th>
								<th>Company Name</th>
								<th>Firstname</th>
								<th>Lastname</th>
								<th width="15%">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$cnt = 1;
							if(is_array($users)){
								foreach($users as $user){
						?>
									<tr>
										<td><?php echo $cnt ?></td>
										<td><?php echo $user['username'] ?></td>
										<td><?php echo $user['user_group_name'] ?></td>
										<td><?php echo $user['company_name'] ?></td>
										<td><?php echo $user['firstname'] ?></td>
										<td><?php echo $user['lastname'] ?></td>
										<td>
											<?php
												if($view_role == 'yes'){
											?>
													<a data-toggle="tooltip" data-placement="top" title="View User" href="<?php echo site_url('user/view/'.$user['id']) ?>"><i class="fa fa-search"> </i></a>
											<?php
												}
											?>
											
											<?php
												if($edit_role == 'yes'){
											?>
													&nbsp;&nbsp;&nbsp;<a data-toggle="tooltip" data-placement="top" title="Edit User" href="<?php echo site_url('user/edit/'.$user['id']) ?>"><i class="fa fa-pencil"> </i></a>
											<?php
												}
											?>
										</td>
									</tr>
						<?php
									$cnt++;
								}
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>