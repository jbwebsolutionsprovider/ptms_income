<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="5%">#</th>
								<th width="95%">Role</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$cnt = 1;
							if(is_array($roles)){
								foreach($roles as $role){
						?>
									<tr>
										<td><?php echo $cnt ?>.</td>
										<td><?php echo ucfirst($role['role_name']) ?></td>
									</tr>
						<?php
									$cnt++;
								}
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>