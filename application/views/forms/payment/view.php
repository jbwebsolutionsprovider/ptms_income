<!-- Main content -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('uploadify/uploadify.css') ?>" />
<script type="text/javascript" src="<?php echo base_url('uploadify/jquery.uploadify.js') ?>"></script>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<?php
						if($message){
					?>
							<div class="alert alert-success alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-check"></i> Save</h4>
								<p><?php echo $message ?></p>
							</div>
					<?php
						}
					?>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td width="25%"><strong>Company Name</strong></td>
								<td width="25%"><?php echo $company_name ?></td>
								<td width="25%"><strong>Client Name</strong></td>
								<td width="25%"><?php echo $client_name ?></td>
							</tr>
							<tr>
								<td><strong>Date Created</strong></td>
								<td><?php echo date('F d, Y', strtotime($date_created)) ?></td>
								<td><strong>Total</strong></td>
								<td><?php echo number_format($total,2,'.',',') ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Payment Details</h3>
				</div>
				<div class="box-body">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Date</th>
								<th>Project Code</th>
								<th>Project Name</th>
								<th>Date Done</th>
								<th>Date Billed</th>
								<th>Amount</th>
								<th>Mode of Payment</th>
							</tr>
						</thead>
						<?php
							if(is_array($payment_details)){
						?>
								<tbody>
									<?php
										$cnt = 1;
										foreach($payment_details as $payment_detail){
									?>
											<tr>
												<td><?php echo $cnt ?>.</td>
												<td><?php echo date('F d, Y', strtotime($payment_detail['transaction_date'])) ?></td>
												<td><?php echo $payment_detail['project_code'] ?></td>
												<td><?php echo $payment_detail['project_name'] ?></td>
												<td><?php echo date('F d, Y', strtotime($payment_detail['date_done'])) ?></td>
												<td><?php echo date('F d, Y', strtotime($payment_detail['date_created'])) ?></td>
												<td><?php echo number_format($payment_detail['paid_amount'],2,'.',',') ?></td>
												<td><?php echo ucfirst($payment_detail['mode_payment']) ?></td>
											</tr>
									<?php
											$cnt++;
										}
									?>
								</tbody>
						<?php
							}
						?>
					</table>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">File Attachement</h3>
				</div>
				<div class="box-body">
					<form enctype="multipart/form-data">
						<div id="queue"></div>
						<input id="file_upload" name="file_upload" type="file" multiple="true">
					</form>
					<div id="display_files">
						<?php
							if(is_array($files)){
						?>
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Filename</th>
										</tr>
									</thead>
									<tbody>
										<?php
											foreach($files as $file){
										?>
												<tr>
													<td><?php echo $file['filename'] ?></td>
												</tr>
										<?php
											}
										?>
									</tbody>
								</table>
						<?php
							}
						?>
					</div>
					<script type="text/javascript">
						<?php $timestamp = time();?>
						$(function() {
							$('#file_upload').uploadify({
								'formData'     : {
									'timestamp' : '<?php echo $timestamp;?>',
									'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
									'subfolder': 'files'
								},
								'swf'      : '<?php echo base_url("uploadify/uploadify.swf") ?>',
								'uploader' : '<?php echo base_url("uploadify/uploadify.php") ?>',
								'onUploadSuccess' : function(file, data, response) {
									// store the image information
									$.ajax({
										url: '<?php echo site_url('payment/save_file') ?>',
										type: 'POST',
										data: {
												payment_id: '<?php echo $id ?>',
												filename: file.name
										},
										success: function(data){
											$('#display_files').html(data);
										}
									})
								}
							});
						});
					</script>
				</div>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="payment_id" id="payment_id" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete Payment</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure to delete the payment?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDeleteYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(payment_id){
		$('#payment_id').val(payment_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDeleteYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("payment/delete") ?>',
				type: 'POST',
				data: {
						payment_id: $('#payment_id').val()
				},
				success: function(){
					window.location = '<?php echo site_url("payment/listing") ?>'
				}
			})
		}
	)
</script>