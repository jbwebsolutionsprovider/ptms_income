<!-- Main content -->
<link href="<?php echo base_url('plugins/select2/css/select2.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/select2/js/select2.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('uploadify/uploadify.css') ?>" />
<script type="text/javascript" src="<?php echo base_url('uploadify/jquery.uploadify.js') ?>"></script>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<form role="form" class="form-horizontal" method="POST">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="company_id" class="col-sm-3 control-label">Company Name <span class="required">*</span></label>
							<div class="col-sm-5">
								<select name="company_id" id="company_id" class="form-control">
									<option value=""> - Select Company - </option>
									<?php
										if(is_array($companies)){
											foreach($companies as $company){
									?>
												<option value="<?php echo $company['id'] ?>" <?php if(set_value('company_id') == $company['id']){ echo 'selected="selected"'; } ?>><?php echo $company['company_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="client_id" class="col-sm-3 control-label">Client <span class="required">*</span></label>
							<div class="col-sm-5">
								<select name="client_id" id="client_id" class="form-control client">
									<option value=""> - Select Client - </option>
									<?php
										if(is_array($clients)){
											foreach($clients as $client){
									?>
												<option value="<?php echo $client['id'] ?>" <?php if(set_value("client_id") == $client['id']){ echo 'selected="selected"'; } ?>><?php echo $client['client_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
								<script type="text/javascript">
									$(".client").select2({dropdownCssClass : 'form-control'});
								</script>
								<script type="text/javascript">
									$('#client_id').change(
										function(){
											if($('#client_id').val() != ''){
												// get the client unbilled projects
												$.ajax({
													url: '<?php echo site_url("payment/get_unpaid_client_projects") ?>',
													type: 'POST',
													data: {
															company_id: $('#company_id').val(),
															client_id: $('#client_id').val()
													},
													success: function(data){
														if(data != 'NONE'){
															$('#display_projects').html(data);
															$('#display_projects').fadeIn('slow');
															$('#display_projects').removeClass('hide');
														}
													}
												})
											}
										}
									)
								</script>
							</div>
						</div>
						<script type="text/javascript">
							$( document ).ready(function() {
								if($('#client_id').val()){
									// get the client unpaid projects
									$.ajax({
										url: '<?php echo site_url("payment/get_unpaid_client_projects") ?>',
										type: 'POST',
										data: {
												client_id: $('#client_id').val()
										},
										success: function(data){
											if(data != 'NONE'){
												$('#display_projects').html(data);
												$('#display_projects').fadeIn('slow');
												$('#display_projects').removeClass('hide');
											}
										}
									})
								}
							});
						</script>
						<div id="display_projects" class="hide"></div>
					</div>
				</div>
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">File Attachement(s)</h3>
					</div>
					<div class="box-body">
						<form enctype="multipart/form-data">
							<div id="queue"></div>
							<input id="file_upload" name="file_upload" type="file" multiple="true">
						</form>
						<div id="display_files">
							<?php
								if(is_array($files)){
							?>
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th width="40%">Filename</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
												foreach($files as $file){
											?>
													<tr>
														<td><?php echo $file['name'] ?></td>
														<td><a data-toggle="tooltip" data-placement="top" title="Delete Income File" href="javascript:void(0)" onclick="delete_item('<?php echo $file['rowid'] ?>','<?php echo $file['name'] ?>')"><i class="fa fa-trash"> </i></a></td>
													</tr>
											<?php
												}
											?>
										</tbody>
									</table>
							<?php
								}
							?>
						</div>

						<script type="text/javascript">
							function delete_item(rowid, filename){
								$.ajax({
									url: '<?php echo site_url("payment/delete_file") ?>',
									type: 'POST',
									data: {
											rowid: rowid,
											filename: filename
									},
									success: function(data){
										$('#display_files').html(data);
									}
								})
							}
						</script>

						<script type="text/javascript">
							<?php $timestamp = time();?>
							$(function() {
								$('#file_upload').uploadify({
									'formData'     : {
										'timestamp' : '<?php echo $timestamp;?>',
										'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
										'subfolder': 'files/payment'
									},
									'swf'      : '<?php echo base_url("uploadify/uploadify.swf") ?>',
									'uploader' : '<?php echo base_url("uploadify/uploadify.php") ?>',
									'onUploadSuccess' : function(file, data, response) {
										// store the image information
										$.ajax({
											url: '<?php echo site_url('payment/add_file') ?>',
											type: 'POST',
											data: {
													filename: file.name
											},
											success: function(data){
												$('#display_files').html(data);
											}
										})
									}
								});
							});
						</script>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>