<!-- Main content -->
<link href="<?php echo base_url('plugins/select2/css/select2.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/select2/js/select2.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('dist/js/numeral.min.js') ?>"></script>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<form role="form" class="form-horizontal" method="POST">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<?php
								if($company_id){
							?>
								<label for="company_id" class="col-sm-3 control-label">Company Name</label>
								<div class="col-sm-5">
									<input type="text" disabled="disabled" class="form-control" value="<?php echo $company_name ?>" />
									<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?php echo $company_id ?>" />
								</div>
							<?php
								} else {
							?>
									<label for="company_id" class="col-sm-3 control-label">Company Name <span class="required">*</span></label>
									<div class="col-sm-5">
										<?php
											if(set_value('company_id')){
												$value = set_value('company_id');
											} else {
												$value = $company_id;
											}
										?>
										<select name="company_id" id="company_id" class="form-control">
											<option value=""> - Select Company - </option>
											<?php
												if(is_array($companies)){
													foreach($companies as $company){
											?>
														<option value="<?php echo $company['id'] ?>" <?php if($value == $company['id']){ echo 'selected="selected"'; } ?>><?php echo $company['company_name'] ?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
							<?php
								}
							?>
						</div>
						<div class="form-group">
							<label for="client_id" class="col-sm-3 control-label">Client</label>
							<div class="col-sm-5">
								<input type="text" value="<?php echo $client_name ?>" disabled="disabled" class="form-control" />
								<input type="hidden" name="client_id" id="client_id" value="<?php echo $client_id ?>"  />
							</div>
						</div>
					</div>
				</div>
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Projects Attached to this Payment</h3>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>Date</th>
									<th>Project Code</th>
									<th>Project Name</th>
									<th>Date Done</th>
									<th>Date Billed</th>
									<th>Amount</th>
									<th>Mode of Payment</th>
								</tr>
							</thead>
							<?php
								if(is_array($projects)){
							?>
									<tbody>
										<?php
											$income_id = '';
											foreach($projects as $project){
												$income_id .= '-'.$project['billing_detail_id'];
										?>
												<tr>
													<td>
														<div class="checkbox icheck">
															<label>
																<input type="checkbox" name="projects[]" id="projects[]" value="<?php echo $project['paid_amount'].'-'.$project['billing_detail_id'] ?>" checked="checked">
															</label>
														</div>
													</td>
													<td><?php echo date('F d, Y', strtotime($project['transaction_date'])) ?></td>
													<td><?php echo $project['project_code'] ?></td>
													<td><?php echo $project['project_name'] ?></td>
													<td><?php echo date('F d, Y', strtotime($project['date_done'])) ?></td>
													<td><?php echo date('F d, Y') ?></td>
													<td>
														<div align="right" class="hide" id="display_paid_amount_<?php echo $project['billing_detail_id'] ?>"><?php echo number_format($project['paid_amount'],2,'.',',') ?></div>
														<input type="text" class="form-control show" name="paid_amount_<?php echo $project['billing_detail_id'] ?>" id="paid_amount_<?php echo $project['billing_detail_id'] ?>" value="<?php echo $project['paid_amount'] ?>" />
													</td>
													<td><?php echo $project['mode_payment'] ?></td>
												</tr>
												<script type="text/javascript">
													$('#paid_amount_'+<?php echo $project['billing_detail_id'] ?>).keyup(
														function(){
															var total_income = 0;
															var selected_income = $('#selected_income').val();
															var items = selected_income.split('-');
															var selected_income_length = items.length;
															for($i=1; $i < selected_income_length; $i++){
																var total_income = parseFloat(total_income) + parseFloat($('#paid_amount_'+items[$i]).val());
															}
															$('#total').val(total_income);
															$('#display_total').html(numeral(total_income).format('0,0.00'));
														}
													)
												</script>
										<?php
											}
										?>
										<tr>
											<td colspan="6"><div align="right"><strong>Total</strong></div></td>
											<td>
												<div align="right"><div id="display_total"><?php echo number_format($total,2,'.',',') ?></div></div>
												<input type="hidden" name="total" id="total" value="<?php echo $total ?>" />
											</td>
											<td>&nbsp;</td>
										</tr>
									</tbody>
							<?php
								}
							?>
						</table>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>

<input type="hidden" id="selected_income" value="<?php echo $income_id ?>" />
<script src="<?php echo base_url('plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>
<script>
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	});

	$('input').on('ifChecked', function(event){
		var contract_price = $(this).val();
		var price = contract_price.split('-');
		var total_income = parseFloat(price[0]) + parseFloat($('#total').val());
		$('#total').val(total_income);
		$('#display_total').html(numeral(total_income).format('0,0.00'));
		$('#display_paid_amount_'+price[1]).removeClass('show');
		$('#display_paid_amount_'+price[1]).addClass('hide');
		$('#paid_amount_'+price[1]).removeClass('hide');
		$('#paid_amount_'+price[1]).addClass('show');
		$('#selected_income').val($('#selected_income').val()+'-'+price[1]);
	});

	$('input').on('ifUnchecked', function(event){
		var contract_price = $(this).val();
		var price = contract_price.split('-');
		var total_income = parseFloat($('#total').val()) - parseFloat(price[0]);
		$('#total').val(total_income);
		$('#display_total').html(numeral(total_income).format('0,0.00'));
		$('#display_paid_amount_'+price[1]).removeClass('hide');
		$('#display_paid_amount_'+price[1]).addClass('show');
		$('#paid_amount_'+price[1]).val(price[0]);
		$('#paid_amount_'+price[1]).removeClass('show');
		$('#paid_amount_'+price[1]).addClass('hide');
		var selected_income = $('#selected_income').val();
		var new_selected_income = selected_income.replace("-"+price[1],"");
		$('#selected_income').val(new_selected_income);
	});
</script>