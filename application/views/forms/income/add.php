<!-- Main content -->
<link href="<?php echo base_url('plugins/select2/css/select2.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/select2/js/select2.min.js') ?>"></script>
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	$('#date_done').datepicker();
</script>
<link href="<?php echo base_url('dist/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('plugins/iCheck/square/blue.css') ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('dist/js/numeral.min.js') ?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('uploadify/uploadify.css') ?>" />
<script type="text/javascript" src="<?php echo base_url('uploadify/jquery.uploadify.js') ?>"></script>

<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<form role="form" class="form-horizontal" method="POST">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="company_id" class="col-sm-3 control-label">Company Name <span class="required">*</span></label>
							<div class="col-sm-5">
								<select name="company_id" id="company_id" class="form-control">
									<option value=""> - Select Company - </option>
									<?php
										if(is_array($companies)){
											foreach($companies as $company){
									?>
												<option value="<?php echo $company['id'] ?>" <?php if(set_value('company_id') == $company['id']){ echo 'selected="selected"'; } ?>><?php echo $company['company_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="date_done" class="col-sm-3 control-label">Date Done <span class="required">*</span></label>
							<div class="col-sm-5">
								<input type="text" data-provide="datepicker" class="form-control" value="<?php echo set_value('date_done') ?>" name="date_done" id="date_done" />
							</div>
						</div>
						<div class="form-group">
							<label for="client_id" class="col-sm-3 control-label">Client <span class="required">*</span></label>
							<div class="col-sm-5">
								<select name="client_id" id="client_id" class="form-control client">
									<option value=""> - Select Client - </option>
									<?php
										if(is_array($clients)){
											foreach($clients as $client){
									?>
												<option value="<?php echo $client['id'] ?>" <?php if(set_value("client_id") == $client['id']){ echo 'selected="selected"'; } ?>><?php echo $client['client_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
								<script type="text/javascript">
									$(".client").select2({dropdownCssClass : 'form-control'});
								</script>
								<script type="text/javascript">
									$('#client_id').change(
										function(){
											if($('#client_id').val() != ''){
												initializeCombo("project_id"," - Select Project - ");
												// get the company projects
												$.ajax({
													url: '<?php echo site_url("project/get_client_projects") ?>',
													type: 'POST',
													data: {
															client_id: $('#client_id').val()
													},
													success: function(data){
														if(data != 'NONE'){
															var myData = JSON.parse(data);
															for(c = 0; c < myData.length; c++){
																var y=document.createElement('option');
																y.setAttribute('value',myData[c].id);
																y.text=myData[c].project_name;
																var x=document.getElementById("project_id");
																//x.add(y,null); // IE only  }
																if (navigator.appName=="Microsoft Internet Explorer") {
																	x.add(y); // IE only
																} else {
																	x.add(y,null);
																}
															}
														}
													}
												})
											}
										}
									)
								</script>
							</div>
						</div>
						<div class="form-group">
							<label for="project_id" class="col-sm-3 control-label">Project Name <span class="required">*</span></label>
							<div class="col-sm-5">
								<select name="project_id" id="project_id" class="form-control client">
									<option value=""> - Select Project - </option>
									<?php
										if(is_array($projects)){
											foreach($projects as $project){
									?>
												<option value="<?php echo $project['id'] ?>" <?php if(set_value('project_id') == $project['id']){ echo 'selected="selected"'; } ?>><?php echo $project['project_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="income_type_id" class="col-sm-3 control-label">Income Type <span class="required">*</span></label>
							<div class="col-sm-5">
								<select name="income_type_id" id="income_type_id" class="form-control client">
									<option value=""> - Select Income Type - </option>
									<?php
										if(is_array($income_types)){
											foreach ($income_types as $income_type) {
									?>
												<option value="<?php echo $income_type['id'] ?>" <?php if(set_value('income_type_id') == $income_type['id']){ echo 'selected="selected"'; } ?>><?php echo $income_type['income_type_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="income_category_id" class="col-sm-3 control-label">Income Category <span class="required">*</span></label>
							<div class="col-sm-5">
								<select name="income_category_id" id="income_category_id" class="form-control client">
									<option value=""> - Select Income Category - </option>
									<?php
										if(is_array($income_categories)){
											foreach ($income_categories as $income_category) {
									?>
												<option value="<?php echo $income_category['id'] ?>" <?php if(set_value('income_category_id') == $income_category['id']){ echo 'selected="selected"'; } ?>><?php echo $income_category['income_category_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="contract_price" class="col-sm-3 control-label">Contract Price <span class="required">*</span></label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo set_value('contract_price') ?>" name="contract_price" id="contract_price" />
							</div>
						</div>
						<div class="form-group">
							<label for="total_income" class="col-sm-3 control-label">Total Income <span class="required">*</span></label>
							<div class="col-sm-5">
								<input type="text" class="form-control" value="<?php echo set_value('total_income') ?>" name="total_income" id="total_income" />
							</div>
						</div>
						<div class="form-group">
							<label for="total_income_type" class="col-sm-3 control-label">&nbsp;</label>
							<div class="col-sm-5">
								<div class="checkbox icheck">
									<label>
										<input type="radio" name="total_income_type" id="total_income_type" value="calculated" <?php if(set_value('total_income_type') == 'calculated'){ echo 'checked="checked"'; } ?>>
										Calculated
									</label>
								</div>
								<div class="checkbox icheck">
									<label>
										<input type="radio" name="total_income_type" id="total_income_type" value="fixed" <?php if(set_value('total_income_type') == 'fixed'){ echo 'checked="checked"'; } ?>>
										Fixed
									</label>
								</div>
							</div>
							<script type="text/javascript">

								$('input#total_income_type').on('ifChecked', function(event){
									$('#total_income_type_selected').val($(this).val());
									if($(this).val() == 'calculated'){
										total_income = 0;
										if(parseInt($('#quantity').val()) > 0 && parseFloat($('#price_per_unit').val()) > 0){
											var total_income = parseInt($('#quantity').val()) * parseFloat($('#price_per_unit').val());
										}
										$('#total_income').val(total_income);
										$('div#income_category_table').removeClass('hide');
										$('div#income_category_table').addClass('show');
									} else {
										$('div#income_category_table').removeClass('show');
										$('div#income_category_table').addClass('hide');
									}
								});

								function uncheck_total_income_type(){
									$('input').iCheck('uncheck');
								}

							</script>
						</div>
						<div class="hide" id="income_category_table">
							<table class="table">
								<tbody>
									<tr>
										<td width="50%">
											<div class="form-group">
												<label for="transport" class="col-sm-3 control-label">Transport</label>
												<div class="col-sm-5">
													<select name="transport" id="transport" class="form-control">
														<option value=""> - select transport - </option>
														<?php
															if(is_array($transports)){
																foreach($transports as $transport){
														?>
																	<option value="<?php echo $transport['id'] ?>" <?php if(set_value('transport') == $transport['id']){ echo 'selected="selected"'; } ?>><?php echo $transport['transport_name'] ?></option>
														<?php
																}
															}
														?>
													</select>
												</div>
											</div>
										</td>
										<td width="50%">
											<div class="form-group">
												<label for="unit_type_id" class="col-sm-3 control-label">Unit</label>
												<div class="col-sm-5">
													<select name="unit_type_id" id="unit_type_id" class="form-control">
														<option value=""> - Select Unit Type - </option>
														<?php
															if(is_array($unit_types)){
																foreach ($unit_types as $unit_type) {
														?>
																	<option value="<?php echo $unit_type['id'] ?>" <?php if(set_value('unit_type_id') == $unit_type['id']){ echo 'selected="selected"'; } ?>><?php echo $unit_type['unit_type_name'] ?></option>
														<?php
																}
															}
														?>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group">
												<label for="quantity" class="col-sm-3 control-label">Quantity</label>
												<div class="col-sm-5">
													<input type="text" class="form-control" value="<?php echo set_value('quantity') ?>" name="quantity" id="quantity" />
													<script type="text/javascript">
														$('#quantity').keyup(
															function(){
																if($('#total_income_type_selected').val() == 'calculated'){
																	var total_income = 0
																	if(parseFloat($('#price_per_unit').val()) > 0){
																		var total_income = parseInt($('#quantity').val()) * parseFloat($('#price_per_unit').val());
																	}
																	$('#total_income').val(total_income);
																}
															}
														)
													</script>
												</div>
											</div>
										</td>
										<td>
											<div class="form-group">
												<label for="place_from" class="col-sm-3 control-label">Place From</label>
												<div class="col-sm-5">
													<select name="place_from" id="place_from" class="form-control">
														<option value=""> - select place from - </option>
														<?php
															if(is_array($places)){
																foreach($places as $place){
														?>
																	<option value="<?php echo $place['id'] ?>" <?php if(set_value('place_from') == $place['id']){ echo 'selected="selected"'; } ?>><?php echo $place['place_name'] ?></option>
														<?php
																}
															}
														?>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group">
												<label for="cargoes" class="col-sm-3 control-label">Cargoes</label>
												<div class="col-sm-5">
													<select name="cargoes" id="cargoes" class="form-control">
														<option value=""> - select cargoes - </option>
														<?php
															if(is_array($cargoes)){
																foreach($cargoes as $cargo){
														?>
																	<option value="<?php echo $cargo['id'] ?>" <?php if(set_value('cargoes') == $cargo['id']){ echo 'selected="selected"'; } ?>><?php echo $cargo['cargo_name'] ?></option>
														<?php
																}
															}
														?>
													</select>
												</div>
											</div>
										</td>
										<td>
											<div class="form-group">
												<label for="place_to" class="col-sm-3 control-label">Place To</label>
												<div class="col-sm-5">
													<select name="place_to" id="place_to" class="form-control">
														<option value=""> - select place to - </option>
														<?php
															if(is_array($places)){
																foreach($places as $place){
														?>
																	<option value="<?php echo $place['id'] ?>" <?php if(set_value('place_to') == $place['id']){ echo 'selected="selected"'; } ?>><?php echo $place['place_name'] ?></option>
														<?php
																}
															}
														?>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group">
												<label for="price_per_unit" class="col-sm-3 control-label">Price Per Unit</label>
												<div class="col-sm-5">
													<input type="text" class="form-control" value="<?php echo set_value('price_per_unit') ?>" name="price_per_unit" id="price_per_unit" />
													<script type="text/javascript">
														$('#price_per_unit').keyup(
															function(){
																if($('#total_income_type_selected').val() == 'calculated'){
																	var total_income = 0
																	if(parseInt($('#quantity').val()) > 0){
																		var total_income = parseInt($('#quantity').val()) * parseFloat($('#price_per_unit').val());
																	}
																	$('#total_income').val(total_income);
																}
															}
														)
													</script>
												</div>
											</div>
										</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2">
											<div class="form-group">
												<label for="remarks" class="col-sm-2 control-label">Remarks</label>
												<div class="col-sm-10">
													<textarea class="form-control" value="<?php echo set_value('remarks') ?>" name="remarks" id="remarks"></textarea>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">File Attachement</h3>
					</div>
					<div class="box-body">
						<form enctype="multipart/form-data">
							<div id="queue"></div>
							<input id="file_upload" name="file_upload" type="file" multiple="true">
						</form>
						<div id="display_files">
							<?php
								if(is_array($files)){
							?>
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th width="40%">Filename</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php
												foreach($files as $file){
											?>
													<tr>
														<td><?php echo $file['name'] ?></td>
														<td><a data-toggle="tooltip" data-placement="top" title="Delete Income File" href="javascript:void(0)" onclick="delete_item('<?php echo $file['rowid'] ?>', '<?php echo $file['name'] ?>')"><i class="fa fa-trash"> </i></a></td>
													</tr>
											<?php
												}
											?>
										</tbody>
									</table>
							<?php
								}
							?>
						</div>
						<script type="text/javascript">
							function delete_item(rowid, filename){
								$.ajax({
									url: '<?php echo site_url("income/delete_file") ?>',
									type: 'POST',
									data: {
											rowid: rowid,
											filename: filename
									},
									success: function(data){
										$('#display_files').html(data);
									}
								})
							}
						</script>

						<script type="text/javascript">
							<?php $timestamp = time();?>
							$(function() {
								$('#file_upload').uploadify({
									'formData'     : {
										'timestamp' : '<?php echo $timestamp;?>',
										'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
										'subfolder': 'files/income'
									},
									'swf'      : '<?php echo base_url("uploadify/uploadify.swf") ?>',
									'uploader' : '<?php echo base_url("uploadify/uploadify.php") ?>',
									'onUploadSuccess' : function(file, data, response) {
										// store the image information
										$.ajax({
											url: '<?php echo site_url('income/add_file') ?>',
											type: 'POST',
											data: {
													filename: file.name
											},
											success: function(data){
												$('#display_files').html(data);
											}
										})
									}
								});
							});
						</script>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<input type="hidden" id="total_income_type_selected" value="" />
<script src="<?php echo base_url('plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>
<script>
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	});
</script>

<div class="modal fade modal-primary" id="income_category_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Income Category</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Calculated will work if Income Category is Transport or Service.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>