<!-- Main content -->
<link href="<?php echo base_url('plugins/select2/css/select2.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/select2/js/select2.min.js') ?>"></script>
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	$('#date_done').datepicker();
</script>
<link href="<?php echo base_url('dist/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('plugins/iCheck/square/blue.css') ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('dist/js/numeral.min.js') ?>"></script>
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<?php
								if($company_id){
							?>
								<label for="company_id" class="col-sm-4 control-label">Company Name</label>
								<div class="col-sm-8">
									<input type="text" disabled="disabled" class="form-control" value="<?php echo $company_name ?>" />
									<input type="hidden" class="form-control" name="company_id" id="company_id" value="<?php echo $company_id ?>" />
								</div>
							<?php
								} else {
							?>
									<label for="company_id" class="col-sm-4 control-label">Company Name <span class="required">*</span></label>
									<div class="col-sm-8">
										<?php
											if(set_value('company_id')){
												$value = set_value('company_id');
											} else {
												$value = $company_id;
											}
										?>
										<select name="company_id" id="company_id" class="form-control">
											<option value=""> - Select Company - </option>
											<?php
												if(is_array($companies)){
													foreach($companies as $company){
											?>
														<option value="<?php echo $company['id'] ?>" <?php if($value == $company['id']){ echo 'selected="selected"'; } ?>><?php echo $company['company_name'] ?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
							<?php
								}
							?>
						</div>
						<div class="form-group">
							<label for="date_done" class="col-sm-4 control-label">Date Done <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('date_done')){
										$value = set_value('date_done');
									} else {
										$value = $date_done;
									}
								?>
								<input type="text" data-provide="datepicker" class="form-control" value="<?php echo $value ?>" name="date_done" id="date_done" />
							</div>
						</div>
						<div class="form-group">
							<label for="client_id" class="col-sm-4 control-label">Client</label>
							<div class="col-sm-8">
								<input type="text" disabled="disabled" value="<?php echo $client_name ?>" class="form-control"/>
								<input type="hidden" name="client_id" value="<?php echo $client_id ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="project_id" class="col-sm-4 control-label">Project Name</label>
							<div class="col-sm-8">
								<input type="text" disabled="disabled" value="<?php echo $project_name ?>" class="form-control"/>
								<input type="hidden" name="project_id" value="<?php echo $project_id ?>" />
							</div>
						</div>
						<div class="form-group">
							<label for="income_type_id" class="col-sm-4 control-label">Income Type <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('income_type_id')){
										$value = set_value('income_type_id');
									} else {
										$value = $income_type_id;
									}
								?>
								<select name="income_type_id" id="income_type_id" class="form-control client">
									<option value=""> - Select Income Type - </option>
									<?php
										if(is_array($income_types)){
											foreach ($income_types as $income_type) {
									?>
												<option value="<?php echo $income_type['id'] ?>" <?php if($value == $income_type['id']){ echo 'selected="selected"'; } ?>><?php echo $income_type['income_type_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="income_category_id" class="col-sm-4 control-label">Income Category <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('income_category_id')){
										$value = set_value('income_category_id');
									} else {
										$value = $income_category_id;
									}
								?>
								<select name="income_category_id" id="income_category_id" class="form-control client">
									<option value=""> - Select Income Category - </option>
									<?php
										if(is_array($income_categories)){
											foreach ($income_categories as $income_category) {
									?>
												<option value="<?php echo $income_category['id'] ?>" <?php if($value == $income_category['id']){ echo 'selected="selected"'; } ?>><?php echo $income_category['income_category_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
								<script type="text/javascript">
									$('#income_category_id').change(
										function(){
											if($('#income_category_id').val() == 3){
												$('#income_category_table').removeClass('hide');
												$('#income_category_table').addClass('show');
											} else {
												$('#income_category_table').removeClass('show');
												$('#income_category_table').addClass('hide');
											}
										}
									)
								</script>
							</div>
						</div>
						<div class="form-group">
							<label for="contract_price" class="col-sm-4 control-label">Contract Price <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('contract_price')){
										$value = set_value('contract_price');
									} else {
										$value = $contract_price;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="contract_price" id="contract_price" />
							</div>
						</div>
						<div class="form-group">
							<label for="total_income" class="col-sm-4 control-label">Total Income <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('total_income')){
										$value = set_value('total_income');
									} else {
										$value = $total_income;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="total_income" id="total_income" />
							</div>
						</div>
						<div class="form-group">
							<label for="total_income_type" class="col-sm-4 control-label">&nbsp;</label>
							<div class="col-sm-8">
								<?php
									if(set_value('total_income_type')){
										$value = set_value('total_income_type');
									} else {
										$value = $total_income_type;
									}
								?>
								<div class="checkbox icheck">
									<label>
										<input type="radio" name="total_income_type" id="total_income_type" value="calculated" <?php if($value == 'calculated'){ echo 'checked="checked"'; } ?>>
										Calculated
									</label>
								</div>
								<div class="checkbox icheck">
									<label>
										<input type="radio" name="total_income_type" id="total_income_type" value="fixed" <?php if($value == 'fixed'){ echo 'checked="checked"'; } ?>>
										Fixed
									</label>
								</div>
							</div>
							<script type="text/javascript">
								
								$('input#total_income_type').on('ifChecked', function(event){
									$('#total_income_type_selected').val($(this).val());
									if($(this).val() == 'calculated'){
										if($('#income_category_id').val() != 3){
											$('#income_category_modal').modal('show');
											setTimeout('uncheck_total_income_type()', 1000);
										} else {
											total_income = 0;
											if(parseInt($('#quantity').val()) > 0 && parseFloat($('#price_per_unit').val()) > 0){
												var total_income = parseInt($('#quantity').val()) * parseFloat($('#price_per_unit').val());
											}
											$('#total_income').val(total_income);
										}
									}
								});

								function uncheck_total_income_type(){
									$('input').iCheck('uncheck');
								}
								
							</script>
						</div>
						<div <?php if($income_category_id == 3 || $income_category_id == 1){ echo 'class="show"'; } else { echo 'class="hide"'; } ?> id="income_category_table">
							<table class="table">
								<tbody>
									<tr>
										<td width="50%">
											<?php
												if(set_value('transport')){
													$value = set_value('transport');
												} else {
													$value = $transport;
												}
											?>
											<div class="form-group">
												<label for="transport" class="col-sm-4 control-label">Transport</label>
												<div class="col-sm-8">
													<select name="transport" id="transport" class="form-control">
														<option value=""> - select transport - </option>
														<?php
															if(is_array($transports)){
																foreach($transports as $transport){
														?>
																	<option value="<?php echo $transport['id'] ?>" <?php if($value == $transport['id']){ echo 'selected="selected"'; } ?>><?php echo $transport['transport_name'] ?></option>
														<?php
																}
															}
														?>
													</select>
												</div>
											</div>
										</td>
										<td width="50%">
											<div class="form-group">
												<label for="unit_type_id" class="col-sm-4 control-label">Unit</label>
												<div class="col-sm-8">
													<?php
														if(set_value('unit_type_id')){
															$value = set_value('unit_type_id');
														} else {
															$value = $unit_type_id;
														}
													?>
													<select name="unit_type_id" id="unit_type_id" class="form-control">
														<option value=""> - Select Unit Type - </option>
														<?php
															if(is_array($unit_types)){
																foreach ($unit_types as $unit_type) {
														?>
																	<option value="<?php echo $unit_type['id'] ?>" <?php if($value == $unit_type['id']){ echo 'selected="selected"'; } ?>><?php echo $unit_type['unit_type_name'] ?></option>
														<?php
																}
															}
														?>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group">
												<label for="quantity" class="col-sm-4 control-label">Quantity</label>
												<div class="col-sm-8">
													<?php
														if(set_value('quantity')){
															$value = set_value('quantity');
														} else {
															$value = $quantity;
														}
													?>
													<input type="text" class="form-control" value="<?php echo $value ?>" name="quantity" id="quantity" />
													<script type="text/javascript">
														$('#quantity').keyup(
															function(){
																if($('#total_income_type_selected').val() == 'calculated'){
																	var total_income = 0
																	if(parseFloat($('#price_per_unit').val()) > 0){
																		var total_income = parseInt($('#quantity').val()) * parseFloat($('#price_per_unit').val());
																	}
																	$('#total_income').val(total_income);
																}
															}
														)
													</script>
												</div>
											</div>
										</td>
										<td>
											<div class="form-group">
												<label for="place_from" class="col-sm-4 control-label">Place From</label>
												<div class="col-sm-8">
													<?php
														if(set_value('place_from')){
															$value = set_value('place_from');
														} else {
															$value = $place_from;
														}
													?>
													<select name="place_from" id="place_from" class="form-control">
														<option value=""> - select place from - </option>
														<?php
															if(is_array($places)){
																foreach($places as $place){
														?>
																	<option value="<?php echo $place['id'] ?>" <?php if($value == $place['id']){ echo 'selected="selected"'; } ?>><?php echo $place['place_name'] ?></option>
														<?php
																}
															}
														?>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group">
												<label for="cargoes" class="col-sm-4 control-label">Cargoes</label>
												<div class="col-sm-8">
													<?php
														if(set_value('cargoes')){
															$value = set_value('cargoes');
														} else {
															$value = $cargoes_selected;
														}
													?>
													<select name="cargoes" id="cargoes" class="form-control">
														<option value=""> - select cargoes - </option>
														<?php
															if(is_array($cargoes)){
																foreach($cargoes as $cargo){
														?>
																	<option value="<?php echo $cargo['id'] ?>" <?php if($value == $cargo['id']){ echo 'selected="selected"'; } ?>><?php echo $cargo['cargo_name'] ?></option>
														<?php
																}
															}
														?>
													</select>
												</div>
											</div>
										</td>
										<td>
											<div class="form-group">
												<label for="place_to" class="col-sm-4 control-label">Place To</label>
												<div class="col-sm-8">
													<?php
														if(set_value('place_to')){
															$value = set_value('place_to');
														} else {
															$value = $place_to;
														}
													?>
													<select name="place_to" id="place_to" class="form-control">
														<option value=""> - select place to - </option>
														<?php
															if(is_array($places)){
																foreach($places as $place){
														?>
																	<option value="<?php echo $place['id'] ?>" <?php if($value == $place['id']){ echo 'selected="selected"'; } ?>><?php echo $place['place_name'] ?></option>
														<?php
																}
															}
														?>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="form-group">
												<label for="price_per_unit" class="col-sm-4 control-label">Price Per Unit</label>
												<div class="col-sm-8">
													<?php
														if(set_value('price_per_unit')){
															$value = set_value('price_per_unit');
														} else {
															$value = $price_per_unit;
														}
													?>
													<input type="text" class="form-control" value="<?php echo $value ?>" name="price_per_unit" id="price_per_unit" />
													<script type="text/javascript">
														$('#price_per_unit').keyup(
															function(){
																if($('#total_income_type_selected').val() == 'calculated'){
																	var total_income = 0
																	if(parseInt($('#quantity').val()) > 0){
																		var total_income = parseInt($('#quantity').val()) * parseFloat($('#price_per_unit').val());
																	}
																	$('#total_income').val(total_income);
																}
															}
														)
													</script>
												</div>
											</div>
										</td>
										<td>
											<div class="form-group">
												<label for="remarks" class="col-sm-4 control-label">Remarks</label>
												<div class="col-sm-8">
													<?php
														if(set_value('remarks')){
															$value = set_value('remarks');
														} else {
															$value = $remarks;
														}
													?>
													<input type="text" class="form-control" value="<?php echo $value ?>" name="remarks" id="remarks" />
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<input type="hidden" id="total_income_type_selected" value="<?php echo $total_income_type ?>" />
<script src="<?php echo base_url('plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>
<script>
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	});
</script>

<div class="modal fade modal-primary" id="income_category_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Income Category</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Calculated will work if Income Category is Transport.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>