<!-- Main content -->
<link href="<?php echo base_url('plugins/select2/css/select2.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/select2/js/select2.min.js') ?>"></script>
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	$('#date_from').datepicker();
	$('#date_to').datepicker();
</script>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<?php
						if($message){
					?>
							<div class="alert alert-success alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-check"></i> Delete</h4>
								<p><?php echo $message ?></p>
							</div>
					<?php
						}
					?>
					<form class="form-inline" method="POST">
						<table class="table table-bordered table-striped">
							<tr>
								<td width="25%"><strong>Company Name</strong></td>
								<td width="25%">
									<select name="company_id" id="company_id" class="form-control">
										<option value=""> - Select Company - </option>
										<?php
											if(is_array($companies)){
												foreach($companies as $company){
										?>
													<option value="<?php echo $company['id'] ?>" <?php if($company_id == $company['id']){ echo 'selected="selected"'; } ?>><?php echo $company['company_name'] ?></option>
										<?php
												}
											}
										?>
									</select>
								</td>
								<td width="25%"><strong>Client Name</strong></td>
								<td width="25%">
									<select name="client_id" id="client_id" class="form-control client">
										<option value=""> - All Client - </option>
										<?php
											if(is_array($clients)){
												foreach($clients as $client){
										?>
													<option value="<?php echo $client['id'] ?>" <?php if($client_id == $client['id']){ echo 'selected="selected"'; } ?>><?php echo $client['client_name'] ?></option>
										<?php
												}
											}
										?>
									</select>
									<script type="text/javascript">
										$(".client").select2({dropdownCssClass : 'form-control'});
									</script>
								</td>
							</tr>
							<tr>
								<td><strong>Date From</strong></td>
								<td><input type="text" data-provide="datepicker" class="form-control date_from" value="<?php echo $date_from ?>" name="date_from" id="date_from" /></td>
								<td><strong>Date To</strong></td>
								<td><input type="text" data-provide="datepicker" class="form-control date_to" value="<?php echo $date_to ?>" name="date_to" id="date_to" /></td>
							</tr>
							<tr>
								<td colspan="4">
									<div align="right">
										<button type="submit" class="btn btn-primary btn-flat">Search</button>
										<button type="button" class="btn btn-primary btn-flat" id="cmdClear">Clear</button>
									</div>
									<script type="text/javascript">
										$('#cmdClear').click(
											function(){
												$.ajax({
													url: '<?php echo site_url("income/clear_search") ?>',
													success: function(){
														window.location = '<?php echo current_url() ?>'
													}
												})
											}
										)
									</script>
								</td>
							</tr>
						</table>
					</form>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="18%">Company Name</th>
								<th width="18%">Client Name</th>
								<th width="20%">Project Name</th>
								<th width="10%">Date Done</th>
								<th width="10%">Contract Price</th>
								<th width="10%">Total Income</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$cnt = 1;
							if(is_array($incomes)){
								foreach($incomes as $income){
						?>
									<tr>
										<td><?php echo $income['company_name'] ?></td>
										<td><?php echo $income['client_name'] ?></td>
										<td><?php echo $income['project_name'] ?></td>
										<td><?php echo date('F d, Y', strtotime($income['date_done'])) ?></td>
										<td><div align="right"><?php echo number_format($income['contract_price'],2,'.',',') ?></div></td>
										<td><div align="right"><?php echo number_format($income['total_income'],2,'.',',') ?></div></td>
										<td>
											<?php
												if($view_role == 'yes'){
											?>
													<a data-toggle="tooltip" data-placement="top" title="View Income" href="<?php echo site_url('income/view/'.$income['id']); ?>"><i class="fa fa-search"> </i></a>
											<?php
												}
											?>
											<?php
												if($edit_role == 'yes'){
											?>
													&nbsp;&nbsp;&nbsp;<a data-toggle="tooltip" data-placement="top" title="Edit Income" href="<?php echo site_url('income/edit/'.$income['id']); ?>"><i class="fa fa-pencil"> </i></a>
											<?php
												}
											?>
											<?php
												if($delete_role == 'yes'){
											?>
													&nbsp;&nbsp;&nbsp;<a data-toggle="tooltip" data-placement="top" title="Delete Income" href="javascript:void(0)" onclick="delete_item(<?php echo $income['id'] ?>)"><i class="fa fa-trash"> </i></a>
											<?php
												}
											?>
										</td>
									</tr>
						<?php
									$cnt++;
								}
							}
						?>
						</tbody>
					</table>
				</div>
				<?php
					if($pages){
				?>
						<div class="box-footer clearfix">
							<?php echo $pages ?>
						</div>
				<?php
					}
				?>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="income_id" id="income_id" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete Income</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure to delete the income?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDeleteYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(income_id){
		$('#income_id').val(income_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDeleteYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("income/delete") ?>',
				type: 'POST',
				data: {
						income_id: $('#income_id').val()
				},
				success: function(){
					window.location = '<?php echo current_url() ?>'
				}
			})
		}
	)
</script>
