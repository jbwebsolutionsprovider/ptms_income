<!-- Main content -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('uploadify/uploadify.css') ?>" />
<script type="text/javascript" src="<?php echo base_url('uploadify/jquery.uploadify.js') ?>"></script>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="row">
						<div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
							<h3 class="box-title"><?php echo $page_sub_title ?></h3>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<?php
								if($edit_role == 'yes'){
							?>
									<a class="btn btn-sm btn-info btn-flat" href="<?php echo site_url('income/edit/'.$id); ?>">Edit</a>
							<?php
								}
							?>
							<?php
								if($delete_role == 'yes'){
							?>
									<a class="btn btn-sm btn-info btn-flat" href="javascript:void(0)" onclick="delete_item(<?php echo $id ?>)">Delete</a>
							<?php
								}
							?>
						</div>
					</div>
				</div>
				<div class="box-body">
					<?php
						if($message){
					?>
							<div class="alert alert-success alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-check"></i> Save</h4>
								<p><?php echo $message ?></p>
							</div>
					<?php
						}
					?>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td width="25%"><strong>Company Name</strong></td>
								<td width="25%"><?php echo $company_name ?></td>
								<td width="25%"><strong>Client Name</strong></td>
								<td width="25%"><?php echo $client_name ?></td>
							</tr>
							<tr>
								<td><strong>Project Name</strong></td>
								<td><?php echo $project_name ?></td>
								<td><strong>Date Done</strong></td>
								<td><?php echo date('F d, Y', strtotime($date_done)) ?></td>
							</tr>
							<tr>
								<td><strong>Income Type</strong></td>
								<td><?php echo ucfirst($income_type_name) ?></td>
								<td><strong>Income Category</strong></td>
								<td><?php echo ucfirst($income_category_name) ?></td>
							</tr>
							<tr>
								<td><strong>Contract Price</strong></td>
								<td><?php echo number_format($contract_price,2,'.',',') ?></td>
								<td><strong>Total Income</strong></td>
								<td><?php echo number_format($total_income,2,'.',',') ?></td>
							</tr>
							<tr>
								<td width="25%"><strong>Total Income Type</strong></td>
								<td width="75%" colspan="3"><?php echo ucfirst($total_income_type) ?></td>
							</tr>
							<?php
								if($income_category_id == 3 || $income_category_id == 1){
							?>
									<tr>
										<td width="25%"><strong>Transport</strong></td>
										<td width="25%"><?php echo $transport_name ?></td>
										<td width="25%"><strong>Unit Type</strong></td>
										<td width="25%"><?php echo $unit_type_name ?></td>
									</tr>
									<tr>
										<td><strong>Quantity</strong></td>
										<td><?php echo $quantity ?></td>
										<td><strong>Place From</strong></td>
										<td><?php echo ucfirst($place_name) ?></td>
									</tr>
									<tr>
										<td><strong>Cargoes</strong></td>
										<td><?php echo $cargo_name ?></td>
										<td><strong>Place To</strong></td>
										<td><?php echo ucfirst($place_to) ?></td>
									</tr>
									<tr>
										<td><strong>Price per Unit</strong></td>
										<td><?php echo number_format($price_per_unit,2,'.',',') ?></td>
										<td><strong>Remarks</strong></td>
										<td><?php echo $remarks ?></td>
									</tr>
							<?php
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">File Attachement</h3>
				</div>
				<div class="box-body">
					<form enctype="multipart/form-data">
						<div id="queue"></div>
						<input id="file_upload" name="file_upload" type="file" multiple="true">
					</form>
					<div id="display_files">
						<?php
							if(is_array($files)){
						?>
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Filename</th>
										</tr>
									</thead>
									<tbody>
										<?php
											foreach($files as $file){
										?>
												<tr>
													<td><?php echo $file['filename'] ?></td>
												</tr>
										<?php
											}
										?>
									</tbody>
								</table>
						<?php
							}
						?>
					</div>
					<script type="text/javascript">
						<?php $timestamp = time();?>
						$(function() {
							$('#file_upload').uploadify({
								'formData'     : {
									'timestamp' : '<?php echo $timestamp;?>',
									'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
									'subfolder': 'files'
								},
								'swf'      : '<?php echo base_url("uploadify/uploadify.swf") ?>',
								'uploader' : '<?php echo base_url("uploadify/uploadify.php") ?>',
								'onUploadSuccess' : function(file, data, response) {
									// store the image information
									$.ajax({
										url: '<?php echo site_url('income/save_file') ?>',
										type: 'POST',
										data: {
												income_id: '<?php echo $id ?>',
												filename: file.name
										},
										success: function(data){
											$('#display_files').html(data);
										}
									})
								}
							});
						});
					</script>
				</div>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="income_id" id="income_id" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete Income</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure to delete the income?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDeleteYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(income_id){
		$('#income_id').val(income_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDeleteYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("income/delete") ?>',
				type: 'POST',
				data: {
						income_id: $('#income_id').val()
				},
				success: function(){
					window.location = '<?php echo site_url("income/listing") ?>'
				}
			})
		}
	)
</script>