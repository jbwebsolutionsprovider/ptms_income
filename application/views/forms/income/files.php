<?php
	if(is_array($files)){
?>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Filename</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach($files as $file){
				?>
						<tr>
							<td><?php echo $file['filename'] ?></td>
						</tr>
				<?php
					}
				?>
			</tbody>
		</table>
<?php
	}
?>