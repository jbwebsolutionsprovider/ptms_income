<link href="<?php echo base_url('dist/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('plugins/iCheck/square/blue.css') ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('dist/js/numeral.min.js') ?>"></script>
<?php
	if(is_array($projects)){
?>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Date</th>
					<th>Project Code</th>
					<th>Project Name</th>
					<th>Date Done</th>
					<th>Date Billed</th>
					<th width="10%">Amount</th>
					<th>Mode of Payment</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach($projects as $project){
				?>
						<tr>
							<td>
								<div class="checkbox icheck">
									<label>
										<input type="checkbox" name="projects[]" id="projects[]" value="<?php echo $project['unpaid_amount'].'-'.$project['id'] ?>">
									</label>
								</div>
							</td>
							<td><?php echo date('F d, Y', strtotime($project['transaction_date'])) ?></td>
							<td><?php echo $project['project_code'] ?></td>
							<td><?php echo $project['project_name'] ?></td>
							<td><?php echo date('F d, Y', strtotime($project['date_done'])) ?></td>
							<td><?php echo date('F d, Y') ?></td>
							<td>
								<div align="right" class="show" id="display_unpaid_amount_<?php echo $project['id'] ?>"><?php echo number_format($project['unpaid_amount'],2,'.',',') ?></div>
								<input type="text" class="form-control hide" name="unpaid_amount_<?php echo $project['id'] ?>" id="unpaid_amount_<?php echo $project['id'] ?>" value="<?php echo $project['unpaid_amount'] ?>" />
							</td>
							<td>
								<?php
									if(is_array($mode_payments)){
								?>
										<select name="mode_payment_<?php echo $project['id'] ?>" class="form-control">
											<?php
													foreach($mode_payments as $mode_payment){
											?>
														<option value="<?php echo $mode_payment['id'] ?>"><?php echo $mode_payment['mode_payment'] ?></option>
											<?php
													}
											?>
										</select>
								<?php
									}
								?>
							</td>
						</tr>
						<script type="text/javascript">
							$('#unpaid_amount_'+<?php echo $project['id'] ?>).keyup(
								function(){
									var total_income = 0;
									var selected_income = $('#selected_income').val();
									var items = selected_income.split('-');
									var selected_income_length = items.length;
									for($i=1; $i < selected_income_length; $i++){
										var total_income = parseFloat(total_income) + parseFloat($('#unpaid_amount_'+items[$i]).val());
									}
									$('#total').val(total_income);
									$('#display_total').html(numeral(total_income).format('0,0.00'));
								}
							)
						</script>
				<?php
					}
				?>
				<tr>
					<td colspan="6"><div align="right"><strong>Total</strong></div></td>
					<td>
						<div align="right"><div id="display_total"></div></div>
						<input type="hidden" name="total" id="total" value="0" />
					</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>
<?php
	}
?>
<input type="hidden" id="selected_income" value="" />
<script src="<?php echo base_url('plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>
<script>
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	});

	$('input').on('ifChecked', function(event){
		var contract_price = $(this).val();
		var price = contract_price.split('-');
		var total_income = parseFloat(price[0]) + parseFloat($('#total').val());
		$('#total').val(total_income);
		$('#display_total').html(numeral(total_income).format('0,0.00'));
		$('#display_unpaid_amount_'+price[1]).removeClass('show');
		$('#display_unpaid_amount_'+price[1]).addClass('hide');
		$('#unpaid_amount_'+price[1]).removeClass('hide');
		$('#unpaid_amount_'+price[1]).addClass('show');
		$('#selected_income').val($('#selected_income').val()+'-'+price[1]);
	});

	$('input').on('ifUnchecked', function(event){
		var contract_price = $(this).val();
		var price = contract_price.split('-');
		var total_income = parseFloat($('#total').val()) - parseFloat(price[0]);
		$('#total').val(total_income);
		$('#display_total').html(numeral(total_income).format('0,0.00'));
		$('#display_unpaid_amount_'+price[1]).removeClass('hide');
		$('#display_unpaid_amount_'+price[1]).addClass('show');
		$('#unpaid_amount_'+price[1]).val(price[0]);
		$('#unpaid_amount_'+price[1]).removeClass('show');
		$('#unpaid_amount_'+price[1]).addClass('hide');
		var selected_income = $('#selected_income').val();
		var new_selected_income = selected_income.replace("-"+price[1],"");
		$('#selected_income').val(new_selected_income);
	});
</script>