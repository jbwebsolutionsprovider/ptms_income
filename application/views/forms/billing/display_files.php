<?php
	if(is_array($files)){
?>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th width="40%">Filename</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach($files as $file){
				?>
						<tr>
							<td><?php echo $file['name'] ?></td>
							<td><a data-toggle="tooltip" data-placement="top" title="Delete Income File" href="javascript:void(0)" onclick="delete_item('<?php echo $file['rowid'] ?>', '<?php echo $file['name'] ?>')"><i class="fa fa-trash"> </i></a></td>
						</tr>
				<?php
					}
				?>
			</tbody>
		</table>
<?php
	}
?>