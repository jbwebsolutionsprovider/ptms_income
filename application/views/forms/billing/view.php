<!-- Main content -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('uploadify/uploadify.css') ?>" />
<script type="text/javascript" src="<?php echo base_url('uploadify/jquery.uploadify.js') ?>"></script>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="row">
						<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
							<h3 class="box-title"><?php echo $page_sub_title ?></h3>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
							<?php
								if($edit_role == 'yes'){
							?>
									<a class="btn btn-sm btn-info btn-flat" href="<?php echo site_url('billing/edit/'.$id); ?>">Edit</a>
							<?php
								}
							?>
							<?php
								if($delete_role == 'yes'){
							?>
									<a class="btn btn-sm btn-info btn-flat" href="javascript:void(0)" onclick="delete_item(<?php echo $id ?>)">Delete</a>
							<?php
								}
							?>
						</div>
					</div>
				</div>
				<div class="box-body">
					<?php
						if($message){
					?>
							<div class="alert alert-success alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-check"></i> Save</h4>
								<p><?php echo $message ?></p>
							</div>
					<?php
						}
					?>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td width="25%"><strong>Billing No.</strong></td>
								<td width="25%"><?php echo $billing_no ?></td>
								<td width="25%"><strong>Company Name</strong></td>
								<td width="25%"><?php echo $company_name ?></td>
							</tr>
							<tr>
								<td><strong>Client Name</strong></td>
								<td><?php echo $client_name ?></td>
								<td><strong>Date Created</strong></td>
								<td><?php echo date('F d, Y', strtotime($date_created)) ?></td>
							</tr>
							<tr>
								<td><strong>Billed By</strong></td>
								<td><?php echo $billed ?></td>
								<td><strong>Total</strong></td>
								<td><?php echo number_format($total,2,'.',',') ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Billing Details</h3>
				</div>
				<div class="box-body">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Date</th>
								<th>Project Code</th>
								<th>Project Name</th>
								<th>Date Done</th>
								<th>Date Billed</th>
								<th>Amount</th>
								<th>Mode of Payment</th>
							</tr>
						</thead>
						<?php
							if(is_array($billing_details)){
						?>
								<tbody>
									<?php
										$cnt = 1;
										foreach($billing_details as $billing_detail){
									?>
											<tr>
												<td><?php echo $cnt ?>.</td>
												<td><?php echo date('F d, Y', strtotime($billing_detail['transaction_date'])) ?></td>
												<td><?php echo $billing_detail['project_code'] ?></td>
												<td><?php echo $billing_detail['project_name'] ?></td>
												<td><?php echo date('F d, Y', strtotime($billing_detail['date_done'])) ?></td>
												<td><?php echo date('F d, Y', strtotime($date_created)) ?></td>
												<td><?php echo number_format($billing_detail['billed_amount'],2,'.',',') ?></td>
												<td><?php echo ucfirst($billing_detail['mode_payment']) ?></td>
											</tr>
									<?php
											$cnt++;
										}
									?>
								</tbody>
						<?php
							}
						?>
					</table>
				</div>
			</div>
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">File Attachement</h3>
				</div>
				<div class="box-body">
					<form enctype="multipart/form-data">
						<div id="queue"></div>
						<input id="file_upload" name="file_upload" type="file" multiple="true">
					</form>
					<div id="display_files">
						<?php
							if(is_array($files)){
						?>
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Filename</th>
										</tr>
									</thead>
									<tbody>
										<?php
											foreach($files as $file){
										?>
												<tr>
													<td><?php echo $file['filename'] ?></td>
												</tr>
										<?php
											}
										?>
									</tbody>
								</table>
						<?php
							}
						?>
					</div>
					<script type="text/javascript">
						<?php $timestamp = time();?>
						$(function() {
							$('#file_upload').uploadify({
								'formData'     : {
									'timestamp' : '<?php echo $timestamp;?>',
									'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
									'subfolder': 'files'
								},
								'swf'      : '<?php echo base_url("uploadify/uploadify.swf") ?>',
								'uploader' : '<?php echo base_url("uploadify/uploadify.php") ?>',
								'onUploadSuccess' : function(file, data, response) {
									// store the image information
									$.ajax({
										url: '<?php echo site_url('billing/save_file') ?>',
										type: 'POST',
										data: {
												billing_id: '<?php echo $id ?>',
												filename: file.name
										},
										success: function(data){
											$('#display_files').html(data);
										}
									})
								}
							});
						});
					</script>
				</div>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="billing_id" id="billing_id" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete Billing</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure to delete the billing?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDeleteYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(billing_id){
		$('#billing_id').val(billing_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDeleteYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("billing/delete") ?>',
				type: 'POST',
				data: {
						billing_id: $('#billing_id').val()
				},
				success: function(){
					window.location = '<?php echo site_url("billing/listing") ?>'
				}
			})
		}
	)
</script>