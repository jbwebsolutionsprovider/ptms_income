<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="5%">#</th>
								<th width="80%">User Group</th>
								<th width="15%">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$cnt = 1;
							if(is_array($user_groups)){
								foreach($user_groups as $user_group){
						?>
									<tr>
										<td><?php echo $cnt ?>.</td>
										<td><?php echo ucfirst($user_group['user_group_name']) ?></td>
										<td>
										<?php
											if($view_role == 'yes'){
										?>
												<a class="btn btn-info btn-sm btn-flat" href="<?php echo site_url('user_group/view/'.$user_group['id']); ?>">View Roles</a>
										<?php
											}
										?>
										</td>
									</tr>
						<?php
									$cnt++;
								}
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>