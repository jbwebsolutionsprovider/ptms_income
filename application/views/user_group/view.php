<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td width="25%"><strong>User Group Name</strong></td>
								<td width="75%"><?php echo $user_group_name ?></td>
							</tr>
						</tbody>
					</table>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs pull-right">
							<li class="pull-left header">
								<i class="fa fa-th"></i>
								User Group Roles
							</li>
							<li class="active"><a href="#existing" data-toggle="tab">Existing Roles</a></li>
							<li><a href="#add_roles" data-toggle="tab">Add Roles</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="existing">
								<div id="display_model_roles">
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th width="5%">#</th>
												<th>Module Name</th>
												<th>Role Name</th>
												<th width="20%">Action</th>
											</tr>
										</thead>
										<?php
											if(is_array($user_group_roles)){
												$cnt = 1;
										?>
												<tbody>
													<?php
														foreach($user_group_roles as $user_group_role){
													?>
															<tr>
																<td><?php echo $cnt ?>.</td>
																<td><?php echo $user_group_role['module_name'] ?></td>
																<td><?php echo $user_group_role['role_name'] ?></td>
																<td><button class="btn btn-info btn-sm btn-flat" onclick="delete_item(<?php echo $user_group_role['id'] ?>)">Remove</button></td>
															</tr>
													<?php
															$cnt++;
														}
													?>
												</tbody>
										<?php
											}
										?>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="add_roles">
								<div id="display_model_add_roles">
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th width="5%">#</th>
												<th>Module Name</th>
												<th>Role Name</th>
												<th width="20%">Action</th>
											</tr>
										</thead>
										<?php
											if(is_array($roles)){
												$cnt = 1;
										?>
												<tbody>
													<?php
														foreach($roles as $role){
													?>
															<tr>
																<td><?php echo $cnt ?>.</td>
																<td><?php echo $role['module_name'] ?></td>
																<td><?php echo $role['role_name'] ?></td>
																<td><button class="btn btn-info btn-sm btn-flat" onclick="add_item(<?php echo $role['id'] ?>)">Add</button></td>
															</tr>
													<?php
															$cnt++;
														}
													?>
												</tbody>
										<?php
											}
										?>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="user_group_role_id" id="user_group_role_id" value="" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete User Group Role</h4>
			</div>
			<div class="modal-body">
				<p>Proceed in deleting the user group role?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDelete">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(user_group_role_id){
		$('#user_group_role_id').val(user_group_role_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDelete').click(
		function(){
			$('#delete_modal').modal('hide');
			$.ajax({
				url: '<?php echo site_url('user_group/remove_role') ?>',
				type: 'POST',
				data: {
						user_group_role_id: $('#user_group_role_id').val()
				},
				success: function(data){
					window.location = '<?php echo current_url() ?>';
				}
			})
		}
	)

	function add_item(user_group_role_id){
		$.ajax({
			url: '<?php echo site_url('user_group/add_role') ?>',
			type: 'POST',
			data: {
					user_group_id: '<?php echo $user_group_id ?>',
					user_group_role_id: user_group_role_id
			},
			success: function(){
				window.location = '<?php echo current_url() ?>';
			}
		})
	}
</script>