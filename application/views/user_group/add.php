<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="user_group_name" class="col-sm-4 control-label">User Group Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="" name="user_group_name" id="user_group_name" />
							</div>
						</div>
					</div>
					<div class="box-header">
						<h3 class="box-title">Select Default User Group Roles</h3>
					</div>
					<div class="box-body">
						<?php
							if(is_array($modules)){
						?>
								<table class="table table-striped table-bordered">
									<tbody>
										<?php
											foreach($modules as $module){
										?>
												<tr>
													<td><?php echo $module['module_name'] ?></td>
												</tr>
												<tr>
													<td>
														<?php
															if(is_array($module['roles'])){
																foreach($module['roles'] as $role){
														?>
																	<div class="checkbox icheck">
																		<label>
																			<input type="checkbox" name="role[]" id="role[]" value="<?php echo $role['module_roles_id'] ?>">
																			<?php echo $role['role_name'] ?>
																		</label>
																	</div>
														<?php
																}
															}
														?>
													</td>
												</tr>
										<?php
											}
										?>
									</tbody>
								</table>
						<?php
							}
						?>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script>
	$(function () {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	});
</script>