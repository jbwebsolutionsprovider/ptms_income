<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="5%">#</th>
								<th width="80%">Module</th>
								<th width="15%">&nbsp;</th>
							</tr>
						</thead>
						<?php
							$cnt = 1;
							if(is_array($modules)){
								foreach($modules as $module){
						?>
									<tr>
										<td><?php echo $cnt ?>.</td>
										<td><?php echo ucfirst($module['module_name']) ?></td>
										<td><a class="btn btn-info btn-sm btn-flat" href="<?php echo site_url('module/view/'.$module['id']); ?>">View Roles</a></td>
									</tr>
						<?php
									$cnt++;
								}
							}
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>