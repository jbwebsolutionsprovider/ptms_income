<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="5%">#</th>
			<th>Role Name</th>
			<th width="20%">Action</th>
		</tr>
	</thead>
	<?php
		if(is_array($module_roles)){
			$cnt = 1;
	?>
			<tbody>
				<?php
					foreach($module_roles as $module_role){
				?>
						<tr>
							<td><?php echo $cnt ?>.</td>
							<td><?php echo $module_role['role_name'] ?></td>
							<td><button class="btn btn-info btn-sm" onclick="delete_item('<?php echo $module_role['module_id'] ?>','<?php echo $module_role['role_id'] ?>')">Remove</button></td>
						</tr>
				<?php
						$cnt++;
					}
				?>
			</tbody>
	<?php
		}
	?>
</table>