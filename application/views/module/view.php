<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td width="25%"><strong>Module Name</strong></td>
								<td width="75%"><?php echo $module ?></td>
							</tr>
						</tbody>
					</table>
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs pull-right">
							<li class="pull-left header">
								<i class="fa fa-th"></i>
								Module Roles
							</li>
							<li class="active"><a href="#existing" data-toggle="tab">Existing Roles</a></li>
							<li><a href="#add_roles" data-toggle="tab">Add Roles</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="existing">
								<div id="display_model_roles">
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th width="5%">#</th>
												<th>Role Name</th>
												<th width="20%">Action</th>
											</tr>
										</thead>
										<?php
											if(is_array($module_roles)){
												$cnt = 1;
										?>
												<tbody>
													<?php
														foreach($module_roles as $module_role){
													?>
															<tr>
																<td><?php echo $cnt ?>.</td>
																<td><?php echo $module_role['role_name'] ?></td>
																<td><button class="btn btn-info btn-sm btn-flat" onclick="delete_item('<?php echo $module_id ?>','<?php echo $module_role['role_id'] ?>')">Remove</button></td>
															</tr>
													<?php
															$cnt++;
														}
													?>
												</tbody>
										<?php
											}
										?>
									</table>
								</div>
							</div>
							<div class="tab-pane" id="add_roles">
								<div id="display_model_add_roles">
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th width="5%">#</th>
												<th>Role Name</th>
												<th width="20%">Action</th>
											</tr>
										</thead>
										<?php
											if(is_array($roles)){
												$cnt = 1;
										?>
												<tbody>
													<?php
														foreach($roles as $role){
													?>
															<tr>
																<td><?php echo $cnt ?>.</td>
																<td><?php echo $role['role_name'] ?></td>
																<td><button class="btn btn-info btn-sm btn-flat" onclick="add_item('<?php echo $module_id ?>','<?php echo $role['id'] ?>')">Add</button></td>
															</tr>
													<?php
															$cnt++;
														}
													?>
												</tbody>
										<?php
											}
										?>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="module_id" id="module_id" value="" />
<input type="hidden" name="role_id" id="role_id" value="" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Remove Role</h4>
			</div>
			<div class="modal-body">
				<p>Proceed in removing the role?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-flat" id="cmdDelete">Yes</button>
				<button type="button" class="btn btn-warning btn-flat" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(module_id, role_id){
		$('#module_id').val(module_id);
		$('#role_id').val(role_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDelete').click(
		function(){
			$('#delete_modal').modal('hide');
			$.ajax({
				url: '<?php echo site_url('module/remove_role') ?>',
				type: 'POST',
				data: {
						module_id: $('#module_id').val(),
						role_id: $('#role_id').val()
				},
				success: function(data){
					window.location = '<?php echo current_url() ?>';
				}
			})
		}
	)

	function add_item(module_id, role_id){
		$.ajax({
			url: '<?php echo site_url('module/add_role') ?>',
			type: 'POST',
			data: {
					module_id: module_id,
					role_id: role_id
			},
			success: function(){
				window.location = '<?php echo current_url() ?>';
			}
		})
	}
</script>