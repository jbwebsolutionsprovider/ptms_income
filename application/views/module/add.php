<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="module_name" class="col-sm-4 control-label">Module Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="" name="module_name" id="module_name" />
							</div>
						</div>
						<div class="form-group">
							<label for="alias_name" class="col-sm-4 control-label">Alias Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="" name="alias_name" id="alias_name" />
							</div>
						</div>
						<div class="form-group">
							<label for="roles" class="col-sm-4 control-label">Module Roles <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(is_array($roles)){
										foreach($roles as $role){
								?>
											<div class="checkbox icheck">
												<label>
													<input type="checkbox" name="role[]" id="role[]" value="<?php echo $role['id'] ?>">
													<?php echo $role['role_name'] ?>
												</label>
											</div>
								<?php
										}
									}
								?>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	});
</script>