<!-- Main content -->
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	$('#transaction_date').datepicker();
	$('#date_to').datepicker();
</script>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<form role="form" class="form-inline" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="date_from">Date From</label>
							<input type="text" data-provide="datepicker" class="form-control" name="date_from" id="date_from"  placeholder="Date From" value="<?php echo $this->input->post('date_from') ?>"/>
						</div>
						<div class="form-group">
							<label for="date_to">Date To</label>
							<input type="text" data-provide="datepicker" class="form-control" id="date_to" name="date_to" placeholder="Date To" value="<?php echo $this->input->post('date_to') ?>">
						</div>
						<button type="submit" class="btn btn-primary btn-flat">Search</button>
					</div>
					<hr />
					<div class="box-header with-border">
						<h3 class="box-title">Results</h3>
					</div>
					<div class="box-body">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>No.</th>
									<th>Client</th>
									<th>Projects</th>
									<th>Contract Price</th>
									<th>Paid</th>
									<th>Unpaid</th>
									<th>Percentage</th>
								</tr>
							</thead>
							<?php
								if(is_array($summary)){
							?>
									<tbody>
										<?php
											$cnt = 1;
											foreach($summary as $report){
										?>
												<tr>
													<td><?php echo $cnt ?></td>
													<td><?php echo $report['client_name'] ?></td>
													<td><?php echo $report['total_projects'] ?></td>
													<td><?php echo number_format($report['total_contract_price'],2,'.',',') ?></td>
													<td><?php echo number_format($report['total_paid_amount'],2,'.',',') ?></td>
													<td><?php echo number_format($report['total_unpaid_amount'],2,'.',',') ?></td>
													<td>
														<?php
															$percent = $report['total_paid_amount'] / $report['total_contract_price'];
															echo number_format( $percent * 100, 2 ) . '%';
														?>														
													</td>
												</tr>
										<?php
												$cnt++;
											}
										?>
									</tbody>
							<?php
								}
							?>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>