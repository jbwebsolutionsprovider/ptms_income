<!-- Main content -->
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	$('#date_from').datepicker();
	$('#date_to').datepicker();
</script>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<form role="form" class="form-inline" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="transport_name">Name of Transport</label>
							<select name="transport" id="transport" class="form-control">
								<option value=""> - Select Transport - </option>
								<?php
									if(is_array($transport)){
										foreach($transport as $list){
								?>
											<option value="<?php echo $list['id'] ?>" <?php if($this->input->post('transport') == $list['id']){ echo 'selected="selected"'; } ?>><?php echo $list['transport_name'] ?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="date_from">Date From</label>
							<input type="text" data-provide="datepicker" class="form-control date_from" value="<?php echo set_value('date_from') ?>" name="date_from" id="date_from" />
						</div>
						<div class="form-group">
							<label for="date_to">Date To</label>
							<input type="text" data-provide="datepicker" class="form-control date_to" value="<?php echo set_value('date_to') ?>" name="date_to" id="date_to" />
						</div>
						<button type="submit" class="btn btn-primary btn-flat">Search</button>
					</div>
					<hr />
					<div class="box-header with-border">
						<h3 class="box-title">Results</h3>
					</div>
					<div class="box-body">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Transport Name</th>
									<th>Quantity</th>
									<th>Total Income</th>
									<th>Paid</th>
									<th>Unpaid</th>
									<th>Balance</th>
								</tr>
							</thead>
							<?php
								if(is_array($transport_summary)){
							?>
									<tbody>
										<?php
											foreach($transport_summary as $report){
										?>
												<tr>
													<td><?php echo $report['transport_name'] ?></td>
													<td><?php echo $report['quantity'] ?></td>
													<td><?php echo number_format($report['total_income'],2,'.',',') ?></td>
													<td><?php echo number_format($report['paid_amount'],2,'.',',') ?></td>
													<td><?php echo number_format($report['paid_amount'],2,'.',',') ?></td>
													<td><?php echo number_format($report['unpaid_amount'],2,'.',',') ?></td>
												</tr>
										<?php
											}
										?>
									</tbody>
							<?php
								}
							?>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>