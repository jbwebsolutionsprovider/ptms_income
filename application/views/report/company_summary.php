<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<form role="form" class="form-inline" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="company_id">Company</label>
							<select name="company_id" id="company_id" class="form-control">
								<option value=""> - Select Company - </option>
								<?php
									if(is_array($companies)){
										foreach($companies as $company){
								?>
											<option value="<?php echo $company['id'] ?>" <?php if($this->input->post('company_id') == $company['id']){ echo 'selected="selected"'; } ?>><?php echo $company['company_name'] ?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
						<button type="submit" class="btn btn-primary btn-flat">Search</button>
					</div>
					<hr />
					<div class="box-header with-border">
						<h3 class="box-title">Results</h3>
					</div>
					<div class="box-body">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>No.</th>
									<th>Project Category</th>
									<th>Date Done</th>
									<th>Billed Date</th>
									<th>Paid</th>
									<th>Unpaid</th>
									<th>Balance</th>
								</tr>
							</thead>
							<?php
								if(is_array($company_summary)){
							?>
									<tbody>
										<?php
											$cnt = 1;
											foreach($company_summary as $report){
										?>
												<tr>
													<td><?php echo $cnt ?></td>
													<td><?php echo $report['income_category_name'] ?></td>
													<td><?php echo date('F d, Y', strtotime($report['date_done'])) ?></td>
													<td><?php echo date('F d, Y', strtotime($report['date_created'])) ?></td>
													<td><?php echo number_format($report['paid_amount'],2,'.',',') ?></td>
													<td><?php echo number_format($report['unpaid_amount'],2,'.',',') ?></td>
													<td><?php echo number_format($report['unpaid_amount'],2,'.',',') ?></td>
												</tr>
										<?php
												$cnt++;
											}
										?>
									</tbody>
							<?php
								}
							?>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>