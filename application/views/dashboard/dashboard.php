<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Monthly Income Summary <?php echo $selected_year ?></h3>
		</div>
		<div class="box-body">
			<form class="form-inline" method="POST">
				 <div class="form-group">
					<label for="select_year">Select Year</label>
					<select name="select_year" id="select_year" class="form-control">
						<option value=""> - select year - </option>
						<?php
							for($year = date('Y'); $year >= 2014; $year--){
						?>
								<option value="<?php echo $year ?>" <?php if($selected_year == $year){ echo 'selected="selected"'; } ?>><?php echo $year ?></option>
						<?php
							}
						?>
					</select>
				</div>
				<button type="submit" class="btn btn-primary">View Report</button>
			</form>
			<hr />
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Company</th>
						<th>January</th>
						<th>February</th>
						<th>March</th>
						<th>April</th>
						<th>May</th>
						<th>June</th>
						<th>July</th>
						<th>August</th>
						<th>September</th>
						<th>October</th>
						<th>November</th>
						<th>December</th>
					</tr>
				</thead>
				<?php
					if(is_array($monthly)){
				?>
						<tbody>
							<?php
								foreach($monthly as $income){
							?>
									<tr>
										<td><?php echo $income['company_name'] ?></td>
										<?php
											if(is_array($income['monthly_income'])){
												foreach($income['monthly_income'] as $detail){
										?>
													<td><?php echo number_format($detail['income'],2,'.',',') ?></td>
										<?php
												}
											}
										?>
									</tr>
							<?php
								}
							?>
						</tbody>
				<?php
					}
				?>
			</table>
		</div>
	</div>
</section>