<!-- Main content -->
<link href="<?php echo base_url('plugins/select2/css/select2.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/select2/js/select2.min.js') ?>"></script>
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	$('#date_from').datepicker();
	$('#date_to').datepicker();
</script>
<section class="content">
	<div class="box box-primary">
		<form class="form-inline" method="POST">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $page_sub_title ?></h3>
			</div>
			<div class="box-body">
				
				<?php
					$error_message = strlen(validation_errors());
					if($error_message > 0){
				?>
						<div class="alert alert-danger alert-dismissable">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<h4><i class="icon fa fa-ban"></i> Error</h4>
							<?php echo validation_errors(); ?>
						</div>
				<?php
					}
				?>
				<div class="form-group">
					<label for="company_id">Company Name</label>
					<select name="company_id" id="company_id" class="form-control">
						<option value=""> - Select Company - </option>
						<?php
							if(is_array($companies)){
								foreach($companies as $company){
						?>
									<option value="<?php echo $company['id'] ?>" <?php if($this->input->post('company_id') == $company['id']){ echo 'selected="selected"'; } ?>><?php echo $company['company_name'] ?></option>
						<?php
								}
							}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="client_id">Client Name</label>
					<select name="client_id" id="client_id" class="form-control client">
						<option value=""> - All Client - </option>
						<?php
							if(is_array($clients)){
								foreach($clients as $client){
						?>
									<option value="<?php echo $client['id'] ?>" <?php if($this->input->post('client_id') == $client['id']){ echo 'selected="selected"'; } ?>><?php echo $client['client_name'] ?></option>
						<?php
								}
							}
						?>
					</select>
					<script type="text/javascript">
						$(".client").select2({dropdownCssClass : 'form-control'});
					</script>
				</div>
				<div class="form-group">
					<label for="date_from">Date From</label>
					<input type="text" data-provide="datepicker" class="form-control date_from" value="<?php echo set_value('date_from') ?>" name="date_from" id="date_from" />
				</div>
				<div class="form-group">
					<label for="date_to">Date To</label>
					<input type="text" data-provide="datepicker" class="form-control date_to" value="<?php echo set_value('date_to') ?>" name="date_to" id="date_to" />
				</div>
				<button type="submit" class="btn btn-primary btn-flat">Search</button>
			</div>
			<hr />
			<div class="box-header with-border">
				<h3 class="box-title">Results</h3>
			</div>
			<div class="box-body">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Date Done</th>
							<th>Project Name</th>
							<th>Client Name</th>
							<th>Income Category</th>
							<th>Contract Price</th>
							<th>Amount Paid</th>
							<th>Balance</th>
						</tr>
					</thead>
					<?php
						if(is_array($income_summary)){
					?>
							<tbody>
								<?php
									foreach($income_summary as $dashboard){
								?>
										<tr>
											<td><?php echo date('F d, Y', strtotime($dashboard['date_done'])) ?></td>
											<td><?php echo $dashboard['project_name'] ?></td>
											<td><?php echo $dashboard['client_name'] ?></td>
											<td><?php echo $dashboard['income_category_name'] ?></td>
											<td><?php echo number_format($dashboard['contract_price'],2,'.',',') ?></td>
											<td><?php echo number_format($dashboard['paid_amount'],2,'.',',') ?></td>
											<td><?php echo number_format($dashboard['unpaid_amount'],2,'.',',') ?></td>
										</tr>
								<?php
									}
								?>
							</tbody>
					<?php
						}
					?>
				</table>
			</div>
		</form>
	</div>
</section>