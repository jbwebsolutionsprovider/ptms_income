<!-- Main content -->
<link href="<?php echo base_url('plugins/select2/css/select2.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/select2/js/select2.min.js') ?>"></script>
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	$('#date_from').datepicker();
	$('#date_to').datepicker();
</script>
<section class="content">
	<div class="box box-primary">
		<form method="POST">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $page_sub_title ?></h3>
			</div>
			<div class="box-body">
				<?php
					$error_message = strlen(validation_errors());
					if($error_message > 0){
				?>
						<div class="alert alert-danger alert-dismissable">
							<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
							<h4><i class="icon fa fa-ban"></i> Error</h4>
							<?php echo validation_errors(); ?>
						</div>
				<?php
					}
				?>
				<table class="table table-bordered">
					<tbody>
						<tr>
							<td width="25%"><strong>Company Name</strong></td>
							<td width="25%">
								<select name="company_id" id="company_id" class="form-control">
									<option value=""> - Select Company - </option>
									<?php
										if(is_array($companies)){
											foreach($companies as $company){
									?>
												<option value="<?php echo $company['id'] ?>" <?php if($this->input->post('company_id') == $company['id']){ echo 'selected="selected"'; } ?>><?php echo $company['company_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
							</td>
							<td width="25%"><strong>Client Name</strong></td>
							<td width="25%">
								<select name="client_id" id="client_id" class="form-control client">
									<option value=""> - All Client - </option>
									<?php
										if(is_array($clients)){
											foreach($clients as $client){
									?>
												<option value="<?php echo $client['id'] ?>" <?php if($this->input->post('client_id') == $client['id']){ echo 'selected="selected"'; } ?>><?php echo $client['client_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
								<script type="text/javascript">
									$(".client").select2({dropdownCssClass : 'form-control'});
								</script>
							</td>
						</tr>
						<tr>
							<td><strong>Date From</strong></td>
							<td><input type="text" data-provide="datepicker" class="form-control date_from" value="<?php echo set_value('date_from') ?>" name="date_from" id="date_from" /></td>
							<td><strong>Date To</strong></td>
							<td><input type="text" data-provide="datepicker" class="form-control date_to" value="<?php echo set_value('date_to') ?>" name="date_to" id="date_to" /></td>
						</tr>
						<tr>
							<td><strong>Status</strong></td>
							<td>
								<select name="billing_status" id="billing_status" class="form-control">
									<option value="closed" <?php if($this->input->post('billing_status') == 'closed'){ echo 'selected="selected"'; } ?>> Closed </option>
									<option value="open" <?php if($this->input->post('billing_status') == 'open'){ echo 'selected="selected"'; } ?>> Open </option>
								</select>
							</td>
							<td><strong>Payment Status</strong></td>
							<td>
								<select name="is_paid" id="is_paid" class="form-control">
									<option value=""> - select payment status - </option>
									<option value="yes" <?php if($this->input->post('is_paid') == 'yes'){ echo 'selected="selected"'; } ?>> Paid </option>
									<option value="no" <?php if($this->input->post('is_paid') == 'no'){ echo 'selected="selected"'; } ?>> Unpaid </option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="4"><div align="right"><button type="submit" class="btn btn-primary btn-flat">Search</button></div></td>
						</tr>
					</tbody>
				</table>
			</div>
			<hr />
			<div class="box-header with-border">
				<h3 class="box-title">Results</h3>
			</div>
			<div class="box-body">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Date Billed</th>
							<th>Billing No</th>
							<th>Project Name</th>
							<th>Date Done</th>
							<th>Billed By</th>
							<th>Amount</th>
							<th>Bill Status</th>
							<th>Aging Days</th>
						</tr>
					</thead>
					<?php
						if(is_array($billing_monitoring)){
					?>
							<tbody>
								<?php
									foreach($billing_monitoring as $dashboard){
								?>
										<tr>
											<td><?php echo date('F d, Y', strtotime($dashboard['date_created'])) ?></td>
											<td><?php echo $dashboard['billing_no'].'-'.$dashboard['billing_year'].'-'.str_pad($dashboard['series_no'], 3, '0', STR_PAD_LEFT) ?></td>
											<td><?php echo $dashboard['project_name'] ?></td>
											<td><?php echo date('F d, Y', strtotime($dashboard['date_done'])) ?></td>
											<td><?php echo $dashboard['firstname'].' '.$dashboard['lastname'] ?></td>
											<td><?php echo $dashboard['total'] ?></td>
											<td>
												<?php
													if($dashboard['is_paid'] == 'yes'){
														echo 'Close';
													} else {
														echo 'Open';
													}
												?>
											</td>
											<td>
												<?php
													$billed_date = strtotime($dashboard['date_created']);
													$date_done = strtotime($dashboard['date_done']);
													$days = $billed_date - $date_done;
													echo floor($days / (24 * 60 * 60 ));
												?>
											</td>
										</tr>
								<?php
									}
								?>
							</tbody>
					<?php
						}
					?>
				</table>
			</div>
		</form>
	</div>
</section>