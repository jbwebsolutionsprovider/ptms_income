<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="mode_payment" class="col-sm-4 control-label">Mode of Payment <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo set_value('mode_payment') ?>" name="mode_payment" id="mode_payment" />
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="col-sm-4 control-label">Description</label>
							<div class="col-sm-8">
								<textarea name="description" id="description" class="form-control"><?php echo set_value('description') ?></textarea>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>