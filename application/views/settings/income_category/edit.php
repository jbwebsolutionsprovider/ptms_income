<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="income_category_name" class="col-sm-4 control-label">Income Category Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('income_category_name')){
										$value = set_value('income_category_name');
									} else {
										$value = $income_category_name;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="income_category_name" id="income_category_name" />
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="col-sm-4 control-label">Description</label>
							<div class="col-sm-8">
								<?php
									if(set_value('description')){
										$value = set_value('description');
									} else {
										$value = $description;
									}
								?>
								<textarea name="description" id="description" class="form-control"><?php echo $value ?></textarea>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>