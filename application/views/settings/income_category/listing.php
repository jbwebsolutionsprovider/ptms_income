<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="5%">#</th>
								<th width="45%">Income Category</th>
								<th width="45%">Description</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$cnt = 1;
							if(is_array($income_categories)){
								foreach($income_categories as $income_category){
						?>
									<tr>
										<td><?php echo $cnt ?>.</td>
										<td><?php echo ucfirst($income_category['income_category_name']) ?></td>
										<td><?php echo ucfirst($income_category['description']) ?></td>
										<td>
											<?php
												if($edit_role == 'yes'){
											?>
													<a data-toggle="tooltip" data-placement="top" title="Edit Income Category" href="<?php echo site_url('income_category/edit/'.$income_category['id']); ?>"><i class="fa fa-pencil"> </i></a>
											<?php
												}
											?>
											<?php
												if($delete_role == 'yes'){
											?>
													&nbsp;&nbsp;&nbsp;<a data-toggle="tooltip" data-placement="top" title="Delete Income Category" href="javascript:void(0)" onclick="delete_item(<?php echo $income_category['id'] ?>)"><i class="fa fa-trash"> </i></a>
											<?php
												}
											?>
										</td>
									</tr>
						<?php
									$cnt++;
								}
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="income_category_id" id="income_category_id" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete Income Category</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure to delete the income category?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDeleteYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(income_category_id){
		$('#income_category_id').val(income_category_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDeleteYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("income_category/delete") ?>',
				type: 'POST',
				data: {
						income_category_id: $('#income_category_id').val()
				},
				success: function(){
					window.location = '<?php echo current_url() ?>'
				}
			})
		}
	)
</script>