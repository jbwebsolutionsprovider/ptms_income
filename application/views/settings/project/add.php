<!-- Main content -->
<link href="<?php echo base_url('plugins/select2/css/select2.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/select2/js/select2.min.js') ?>"></script>
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	$('#transaction_date').datepicker();
</script>
<section class="content">
	<div class="row">
		<div class="col-lg-10">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="project_name" class="col-sm-4 control-label">Project Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo set_value('project_name') ?>" name="project_name" id="project_name" />
							</div>
						</div>
						<div class="form-group">
							<label for="client_id" class="col-sm-4 control-label">Client <span class="required">*</span></label>
							<div class="col-sm-8">
								<select name="client_id" id="client_id" class="form-control client">
									<option value=""> - Select Client - </option>
									<?php
										if(is_array($clients)){
											foreach($clients as $client){
									?>
												<option value="<?php echo $client['id'] ?>" <?php if(set_value("client_id") == $client['id']){ echo 'selected="selected"'; } ?>><?php echo $client['client_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
								<script type="text/javascript">
									$(".client").select2({dropdownCssClass : 'form-control'});
								</script>
							</div>
						</div>
						<div class="form-group">
							<label for="project_code" class="col-sm-4 control-label">Code</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo set_value('project_code') ?>" name="project_code" id="project_code" />
							</div>
						</div>
						<div class="form-group">
							<label for="project_status" class="col-sm-4 control-label">Status</label>
							<div class="col-sm-8">
								<select name="project_status" id="project_status" class="form-control">
									<option value=""> - Select Status - </option>
									<option value="open" <?php if(set_value('project_status') == 'open'){ echo 'selected="selected"'; } ?>>Open</option>
									<option value="closed" <?php if(set_value('project_status') == 'closed'){ echo 'selected="selected"'; } ?>>Closed</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="project_type" class="col-sm-4 control-label">Type</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo set_value('project_type') ?>" name="project_type" id="project_type" />
							</div>
						</div>
						<div class="form-group">
							<label for="transaction_date" class="col-sm-4 control-label">Date of Transaction</label>
							<div class="col-sm-8">
								<input type="text" data-provide="datepicker" class="form-control transaction_date" value="<?php echo set_value('transaction_date') ?>" name="transaction_date" id="transaction_date" />
							</div>
						</div>
						<div class="form-group">
							<label for="remarks" class="col-sm-4 control-label">Remarks</label>
							<div class="col-sm-8">
								<textarea name="remarks" id="remarks" class="form-control"><?php echo set_value('remarks') ?></textarea>
							</div>
						</div>
					</div>
					<div class="box-body hide" id="student_ctrl">
						<div id="student_message" class="hide">
							<div class="alert alert-danger alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-ban"></i> Error</h4>
								<p>&nbsp;</p>
							</div>
						</div>
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th width="30%">Student Lastname</th>
									<th width="30%">Student Firstname</th>
									<th width="30%">Hauler Company</th>
									<th width="10%">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><input type="text" name="lastname" id="lastname" class="form-control" /></td>
									<td><input type="text" name="firstname" id="firstname" class="form-control" /></td>
									<td><input type="text" name="hauler_company" id="hauler_company" class="form-control" /></td>
									<td><button type="button" class="btn btn-primary btn-flat" name="cmdAdd" id="cmdAdd">Add</button></td>
								</tr>
							</tbody>
						</table>
						<div id="display_student">
							<?php
								if($this->cart->total_items() > 0){
							?>
									<table class="table table-striped table-bordered">
										<tbody>
											<?php
												foreach($this->cart->contents() as $students){
											?>
													<tr>
														<td width="30%"><?php echo $students['options']['lastname'] ?></td>
														<td width="30%"><?php echo $students['options']['firstname'] ?></td>
														<td width="30%"><?php echo $students['options']['hauler_company'] ?></td>
														<td width="20%"><a data-toggle="tooltip" data-placement="top" title="Delete Bank Details" href="javascript:void(0)" onclick="delete_item('<?php echo $students['rowid'] ?>')"><i class="fa fa-trash"> </i></a></td>
													</tr>
											<?php
												}
											?>
										</tbody>
									</table>
							<?php
								}
							?>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$('#client_id').change(
		function(){
			if($('#client_id').val() == 1){
				// students client, display the details
				$('#student_ctrl').removeClass('hide');
				$('#student_ctrl').addClass('show');
			} else {
				$('#student_ctrl').removeClass('show');
				$('#student_ctrl').addClass('hide');
			}
		}
	)

	$('#cmdAdd').click(
		function(){
			var error = '';
			if($('#lastname').val() == ''){
				error = 1;
				$('#student_message p').append('Please enter Lastname.<br/>');
			}

			if($('#firstname').val() == ''){
				error = 1;
				$('#student_message p').append('Please enter Firstname.');
			}

			if(error == 1){
				$('#student_message').removeClass('hide');
				$('#student_message').addClass('show');
			} else {
				$.ajax({
					url: '<?php echo site_url("project/add_student") ?>',
					type: 'POST',
					data: {
							firstname: $('#firstname').val(),
							lastname: $('#lastname').val(),
							hauler_company: $('#hauler_company').val()
					},
					success: function(data){
						$('#display_student').html(data);
						$('#firstname').val('');
						$('#lastname').val('');
						$('#hauler_company').val('');
					}
				})
			}
		}
	)

	function delete_item(rowid){
		$.ajax({
			url: '<?php echo site_url("project/remove_student") ?>',
			type: 'POST',
			data: {
					rowid: rowid
			},
			success: function(data){
				$('#display_student').html(data);
				$('#firstname').val('');
				$('#lastname').val('');
			}
		})
	}
</script>