<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="row">
						<div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
							<h3 class="box-title"><?php echo $page_sub_title ?></h3>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<?php
								if($closed_role == 'yes' && $project_status == 'open'){
							?>
									<a class="btn btn-sm btn-info btn-flat" href="javascript:void(0)" onclick="closed_item(<?php echo $id ?>)">Closed</a>
							<?php
								}
							?>
							<?php
								if($edit_role == 'yes' && $project_status == 'open'){
							?>
									<a class="btn btn-sm btn-info btn-flat" href="<?php echo site_url('project/edit/'.$id); ?>">Edit</a>
							<?php
								}
							?>
							<?php
								if($delete_role == 'yes' && $project_status == 'open'){
							?>
									<a class="btn btn-sm btn-info btn-flat" href="javascript:void(0)" onclick="delete_item(<?php echo $id ?>)">Delete</a>
							<?php
								}
							?>
						</div>
					</div>
				</div>
				<div class="box-body">
					<?php
						if($message){
					?>
							<div class="alert alert-success alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-check"></i> Save</h4>
								<p><?php echo $message ?></p>
							</div>
					<?php
						}
					?>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td width="25%"><strong>Project Name</strong></td>
								<td width="25%"><?php echo $project_name ?></td>
								<td width="25%"><strong>Code</strong></td>
								<td width="25%"><?php echo $project_code ?></td>
							</tr>
							<tr>
								<td><strong>Client Name</strong></td>
								<td><?php echo $client_name ?></td>
								<td><strong>Type</strong></td>
								<td><?php echo $project_type ?></td>
							</tr>
							<tr>
								<td><strong>Status</strong></td>
								<td><?php echo ucfirst($project_status) ?></td>
								<td><strong>Date of Transaction</strong></td>
								<td><?php echo date('F d, Y', strtotime($transaction_date)) ?></td>
							</tr>
							<tr>
								<td width="25%"><strong>Remarks</strong></td>
								<td width="75%" colspan="3"><?php echo $remarks ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<?php
				if(is_array($students)){
			?>
					<div class="box box-primary">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-lg-10 col-md-10 col-xs-12 col-sm-12">
									<h3 class="box-title">List of Students</h3>
								</div>
								<?php
									if($add_student_role == 'yes'){
								?>
										<div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
											<a href="<?php echo site_url('student/add/'.$id) ?>" class="btn btn-info btn-sm btn-flat">Add Student</a>
										</div>
								<?php
									}
								?>
							</div>
						</div>
						<div class="box-body">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>&nbsp;</th>
										<th>First Name</th>
										<th>Last Name</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$cnt = 1;
										foreach($students as $student){
									?>
											<tr>
												<td><?php echo $cnt ?></td>
												<td><?php echo $student['firstname'] ?></td>
												<td><?php echo $student['lastname'] ?></td>
											</tr>
									<?php
											$cnt++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
			<?php
				}
			?>
		</div>
	</div>
</section>

<input type="hidden" name="project_id" id="project_id" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete Project</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure to delete the project?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDeleteYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-primary" id="closed_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Closed Project</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure you want to closed the project?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdClosedYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(project_id){
		$('#project_id').val(project_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDeleteYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("project/delete") ?>',
				type: 'POST',
				data: {
						project_id: $('#project_id').val()
				},
				success: function(){
					window.location = '<?php echo site_url("project/listing") ?>'
				}
			})
		}
	)

	function closed_item(project_id){
		$('#project_id').val(project_id);
		$('#closed_modal').modal('show');
	}

	$('#cmdClosedYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("project/closed") ?>',
				type: 'POST',
				data: {
						project_id: $('#project_id').val()
				},
				success: function(){
					window.location = '<?php echo current_url() ?>'
				}
			})
		}
	)
</script>