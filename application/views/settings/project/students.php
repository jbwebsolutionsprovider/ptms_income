<?php
	if($this->cart->total_items() > 0){
?>
		<table class="table table-striped table-bordered">
			<tbody>
				<?php
					foreach($this->cart->contents() as $students){
				?>
						<tr>
							<td width="30%"><?php echo $students['options']['lastname'] ?></td>
							<td width="30%"><?php echo $students['options']['firstname'] ?></td>
							<td width="30%"><?php echo $students['options']['hauler_company'] ?></td>
							<td width="10%"><a data-toggle="tooltip" data-placement="top" title="Delete Student" href="javascript:void(0)" onclick="delete_item('<?php echo $students['rowid'] ?>')"><i class="fa fa-trash"> </i></a></td>
						</tr>
				<?php
					}
				?>
			</tbody>
		</table>
<?php
	}
?>