<!-- Main content -->
<link href="<?php echo base_url('plugins/select2/css/select2.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/select2/js/select2.min.js') ?>"></script>
<link href="<?php echo base_url('plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" />
<script src="<?php echo base_url('plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	$('#transaction_date').datepicker();
</script>
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="project_name" class="col-sm-4 control-label">Project Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('project_name')){
										$value = set_value('project_name');
									} else {
										$value = $project_name;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="project_name" id="project_name" />
							</div>
						</div>
						<div class="form-group">
							<label for="client_id" class="col-sm-4 control-label">Client <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('client_id')){
										$value = set_value('client_id');
									} else {
										$value = $client_id;
									}
								?>
								<select name="client_id" id="client_id" class="form-control client">
									<option value=""> - Select Client - </option>
									<?php
										if(is_array($clients)){
											foreach($clients as $client){
									?>
												<option value="<?php echo $client['id'] ?>" <?php if($value == $client['id']){ echo 'selected="selected"'; } ?>><?php echo $client['client_name'] ?></option>
									<?php
											}
										}
									?>
								</select>
								<script type="text/javascript">
									$(".client").select2({dropdownCssClass : 'form-control'});
								</script>
							</div>
						</div>
						<div class="form-group">
							<label for="project_code" class="col-sm-4 control-label">Code</label>
							<div class="col-sm-8">
								<?php
									if(set_value('project_code')){
										$value = set_value('project_code');
									} else {
										$value = $project_code;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="project_code" id="project_code" />
							</div>
						</div>
						<div class="form-group">
							<label for="project_status" class="col-sm-4 control-label">Status</label>
							<div class="col-sm-8">
								<?php
									if(set_value('project_status')){
										$value = set_value('project_status');
									} else {
										$value = $project_status;
									}
								?>
								<select name="project_status" id="project_status" class="form-control">
									<option value=""> - Select Status - </option>
									<option value="open" <?php if($value == 'open'){ echo 'selected="selected"'; } ?>>Open</option>
									<option value="closed" <?php if($value == 'closed'){ echo 'selected="selected"'; } ?>>Closed</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="project_type" class="col-sm-4 control-label">Type</label>
							<div class="col-sm-8">
								<?php
									if(set_value('project_type')){
										$value = set_value('project_type');
									} else {
										$value = $project_type;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="project_type" id="project_type" />
							</div>
						</div>
						<div class="form-group">
							<label for="transaction_date" class="col-sm-4 control-label">Date of Transaction</label>
							<div class="col-sm-8">
								<?php
									if(set_value('transaction_date')){
										$value = set_value('transaction_date');
									} else {
										$value = $transaction_date;
									}
								?>
								<input type="text" data-provide="datepicker" class="form-control transaction_date" value="<?php echo date('m/d/Y', strtotime($value)) ?>" name="transaction_date" id="transaction_date" />
							</div>
						</div>
						<div class="form-group">
							<label for="remarks" class="col-sm-4 control-label">Remarks</label>
							<div class="col-sm-8">
								<?php
									if(set_value('remarks')){
										$value = set_value('remarks');
									} else {
										$value = $remarks;
									}
								?>
								<textarea name="remarks" id="remarks" class="form-control"><?php echo $value ?></textarea>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>