<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
								<h3 class="box-title"><?php echo $page_sub_title ?></h3>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
								<a href="<?php echo site_url('project/view/'.$id) ?>" class="btn btn-info btn-sm btn-flat">View Project</a>
							</div>
						</div>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="project_name" class="col-sm-4 control-label">Project Name</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo $project_name ?>" disabled="disabled" />
							</div>
						</div>
						<div class="form-group">
							<label for="firstname" class="col-sm-4 control-label">First Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo set_value('firstname') ?>" name="firstname" id="firstname" />
							</div>
						</div>
						<div class="form-group">
							<label for="lastname" class="col-sm-4 control-label">Last Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo set_value('lastname') ?>" name="lastname" id="lastname" />
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>