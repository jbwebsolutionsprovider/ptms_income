<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<form name="frmListing" id="frmListing" method="POST">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th width="30%">Firstname</th>
									<th width="30%">Lastname</th>
									<th width="30%">Company</th>
									<th width="10%">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><input type="text" name="firstname" id="firstname" value="<?php echo $this->input->post("firstname") ?>" class="form-control"/></td>
									<td><input type="text" name="lastname" id="lastname" value="<?php echo $this->input->post("lastname") ?>" class="form-control"/></td>
									<td><input type="text" name="hauler_company" id="hauler_company" value="<?php echo $this->input->post("hauler_company") ?>" class="form-control"/></td>
									<td><button type="submit" name="cmdSearch" id="cmdSearch" class="btn btn-primary btn-flat">Search</button></td>
								</tr>
							<?php
								if(is_array($students)){
									foreach($students as $student){
							?>
										<tr>
											<td><?php echo ucfirst($student['firstname']) ?></td>
											<td><?php echo ucfirst($student['lastname']) ?></td>
											<td><?php echo ucfirst($student['hauler_company']) ?></td>
											<td>&nbsp;</td>
										</tr>
							<?php
									}
								}
							?>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>