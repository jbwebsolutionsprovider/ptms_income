<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<?php
						if($message){
					?>
							<div class="alert alert-success alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-check"></i> Delete</h4>
								<p><?php echo $message ?></p>
							</div>
					<?php
						}
					?>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="5%">#</th>
								<th width="30%">Company Name</th>
								<th width="30%">Description</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$cnt = 1;
							if(is_array($companies)){
								foreach($companies as $company){
						?>
									<tr>
										<td><?php echo $cnt ?>.</td>
										<td><?php echo ucfirst($company['company_name']) ?></td>
										<td><?php echo ucfirst($company['description']) ?></td>
										<td>
											<?php
												if($edit_role == 'yes'){
											?>
													<a data-toggle="tooltip" data-placement="top" title="Edit Company" href="<?php echo site_url('company/edit/'.$company['id']); ?>"><i class="fa fa-pencil"> </i></a>
											<?php
												}
											?>
											<?php
												if($delete_role == 'yes'){
											?>
													&nbsp;&nbsp;&nbsp;<a data-toggle="tooltip" data-placement="top" title="Delete Company" href="javascript:void(0)" onclick="delete_item(<?php echo $company['id'] ?>)"><i class="fa fa-trash"> </i></a>
											<?php
												}
											?>
										</td>
									</tr>
						<?php
									$cnt++;
								}
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="company_id" id="company_id" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete Company</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure to delete the company?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDeleteYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(company_id){
		$('#company_id').val(company_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDeleteYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("company/delete") ?>',
				type: 'POST',
				data: {
						company_id: $('#company_id').val()
				},
				success: function(){
					window.location = '<?php echo current_url() ?>'
				}
			})
		}
	)
</script>