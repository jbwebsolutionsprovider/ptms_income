<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="5%">#</th>
								<th width="45%">Bank</th>
								<th width="45%">Branch</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$cnt = 1;
							if(is_array($banks)){
								foreach($banks as $bank){
						?>
									<tr>
										<td><?php echo $cnt ?>.</td>
										<td><?php echo ucfirst($bank['bank_name']) ?></td>
										<td><?php echo ucfirst($bank['branch']) ?></td>
										<td>
											<?php
												if($edit_role == 'yes'){
											?>
													<a data-toggle="tooltip" data-placement="top" title="Edit Bank" href="<?php echo site_url('bank/edit/'.$bank['id']); ?>"><i class="fa fa-pencil"> </i></a>
											<?php
												}
											?>
											<?php
												if($delete_role == 'yes'){
											?>
													&nbsp;&nbsp;&nbsp;<a data-toggle="tooltip" data-placement="top" title="Delete Bank" href="javascript:void(0)" onclick="delete_item(<?php echo $bank['id'] ?>)"><i class="fa fa-trash"> </i></a>
											<?php
												}
											?>
										</td>
									</tr>
						<?php
									$cnt++;
								}
							}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="bank_id" id="bank_id" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete Bank</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure to delete the bank?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDeleteYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(bank_id){
		$('#bank_id').val(bank_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDeleteYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("bank/delete") ?>',
				type: 'POST',
				data: {
						bank_id: $('#bank_id').val()
				},
				success: function(){
					window.location = '<?php echo current_url() ?>'
				}
			})
		}
	)
</script>