<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="bank_name" class="col-sm-4 control-label">Bank Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('bank_name')){
										$value = set_value('bank_name');
									} else {
										$value = $bank_name;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="bank_name" id="bank_name" />
							</div>
						</div>
						<div class="form-group">
							<label for="branch" class="col-sm-4 control-label">Branch</label>
							<div class="col-sm-8">
								<?php
									if(set_value('branch')){
										$value = set_value('branch');
									} else {
										$value = $branch;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="branch" id="branch" />
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>