<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<div class="row">
						<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
							<h3 class="box-title"><?php echo $page_sub_title ?></h3>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
							<?php
									if($edit_role == 'yes'){
								?>
										<a class="btn btn-sm btn-info btn-flat" href="<?php echo site_url('client/edit/'.$id); ?>">Edit</a>
								<?php
									}
								?>
								<?php
									if($delete_role == 'yes'){
								?>
										<a class="btn btn-sm btn-info btn-flat" href="javascript:void(0)" onclick="delete_item(<?php echo $id ?>)">Delete</a>
								<?php
									}
								?>
						</div>
					</div>
				</div>
				<div class="box-body">
					<?php
						if($message){
					?>
							<div class="alert alert-success alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-check"></i> Message</h4>
								<p><?php echo $message ?></p>
							</div>
					<?php
						}
					?>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td width="25%"><strong>Client Name</strong></td>
								<td width="25%"><?php echo $client_name ?></td>
								<td width="25%"><strong>Business Type</strong></td>
								<td width="25%"><?php echo $business_type ?></td>
							</tr>
							<tr>
								<td><strong>Address</strong></td>
								<td><?php echo $address ?></td>
								<td><strong>Person In Charge</strong></td>
								<td><?php echo $person_in_charge ?></td>
							</tr>
							<tr>
								<td><strong>Contacts</strong></td>
								<td colspan="3"><?php echo $contacts ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="box-header with-border">
					<div class="row">
						<div class="col-lg-10">
							<h3 class="box-title">Bank Account Details</h3>
						</div>
						<?php
							if($add_bank_role == 'yes'){
						?>
								<div class="col-lg-2">
									<a href="<?php echo site_url('client/bank_add/'.$id) ?>" class="btn btn-info btn-sm btn-flat">Add Bank Account</a>
								</div>
						<?php
							}
						?>
					</div>
				</div>
				<div class="box-body">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Bank Name</th>
								<th>Branch</th>
								<th>Account Number</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if(is_array($bank_accounts)){
									foreach($bank_accounts as $account){
							?>
										<tr>
											<td><?php echo $account['bank_name'] ?></td>
											<td><?php echo $account['branch'] ?></td>
											<td><?php echo $account['account_number'] ?></td>
											<td>
												<?php
													if($edit_bank_role == 'yes'){
												?>
														<a data-toggle="tooltip" data-placement="top" title="Edit Bank Account" href="<?php echo site_url('client/bank_edit/'.$account['id']) ?>"><i class="fa fa-pencil"> </i></a>
												<?php
													}
												?>

												<?php
													if($delete_bank_role == 'yes'){
												?>
														&nbsp;&nbsp;&nbsp;<a data-toggle="tooltip" data-placement="top" title="Delete Bank Account" href="javascript:void(0)" onclick="delete_item(<?php echo $account['id'] ?>)"><i class="fa fa-trash"> </i></a>
												<?php
													}
												?>
											</td>
										</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="bank_account_id" id="bank_account_id" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete Bank Account</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure to delete the bank account?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDeleteYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(bank_account_id){
		$('#bank_account_id').val(bank_account_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDeleteYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("client/bank_delete") ?>',
				type: 'POST',
				data: {
						bank_account_id: $('#bank_account_id').val()
				},
				success: function(){
					window.location = '<?php echo current_url() ?>'
				}
			})
		}
	)
</script>