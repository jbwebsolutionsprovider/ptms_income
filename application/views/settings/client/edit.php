<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="client_name" class="col-sm-4 control-label">Client Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(set_value('client_name')){
										$value = set_value('client_name');
									} else {
										$value = $client_name;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="client_name" id="client_name" />
							</div>
						</div>
						<div class="form-group">
							<label for="address" class="col-sm-4 control-label">Address</label>
							<div class="col-sm-8">
								<?php
									if(set_value('address')){
										$value = set_value('address');
									} else {
										$value = $address;
									}
								?>
								<textarea name="address" id="address" class="form-control"><?php echo $value ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="business_type" class="col-sm-4 control-label">Business Type</label>
							<div class="col-sm-8">
								<?php
									if(set_value('business_type')){
										$value = set_value('business_type');
									} else {
										$value = $business_type;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="business_type" id="business_type" />
							</div>
						</div>
						<div class="form-group">
							<label for="person_in_charge" class="col-sm-4 control-label">Person In Charge</label>
							<div class="col-sm-8">
								<?php
									if(set_value('person_in_charge')){
										$value = set_value('person_in_charge');
									} else {
										$value = $person_in_charge;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="person_in_charge" id="person_in_charge" />
							</div>
						</div>
						<div class="form-group">
							<label for="contacts" class="col-sm-4 control-label">Contacts</label>
							<div class="col-sm-8">
								<?php
									if(set_value('contacts')){
										$value = set_value('contacts');
									} else {
										$value = $contacts;
									}
								?>
								<input type="text" class="form-control" value="<?php echo $value ?>" name="contacts" id="contacts" />
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>