<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $page_sub_title ?></h3>
				</div>
				<div class="box-body">
					<?php
						if($message){
					?>
							<div class="alert alert-success alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-check"></i> Delete</h4>
								<p><?php echo $message ?></p>
							</div>
					<?php
						}
					?>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th width="2%">#</th>
								<th width="20%">Client Name</th>
								<th width="20%">Address</th>
								<th width="15%">Business Type</th>
								<th width="15%">Person In Charge</th>
								<th width="15%">Contacts</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$cnt = 1;
							if(is_array($clients)){
								foreach($clients as $client){
						?>
									<tr>
										<td><?php echo $cnt ?>.</td>
										<td><?php echo $client['client_name'] ?></td>
										<td><?php echo ucfirst($client['address']) ?></td>
										<td><?php echo ucfirst($client['business_type']) ?></td>
										<td><?php echo ucfirst($client['person_in_charge']) ?></td>
										<td><?php echo ucfirst($client['contacts']) ?></td>
										<td>
											<?php
												if($view_role == 'yes'){
											?>
													<a data-toggle="tooltip" data-placement="top" title="View Client" href="<?php echo site_url('client/view/'.$client['id']); ?>"><i class="fa fa-search"> </i></a>
											<?php
												}
											?>
											<?php
												if($edit_role == 'yes'){
											?>
													&nbsp;&nbsp;&nbsp;<a data-toggle="tooltip" data-placement="top" title="Edit Client" href="<?php echo site_url('client/edit/'.$client['id']); ?>"><i class="fa fa-pencil"> </i></a>
											<?php
												}
											?>
											<?php
												if($delete_role == 'yes'){
											?>
													&nbsp;&nbsp;&nbsp;<a data-toggle="tooltip" data-placement="top" title="Delete Client" href="javascript:void(0)" onclick="delete_item(<?php echo $client['id'] ?>)"><i class="fa fa-trash"> </i></a>
											<?php
												}
											?>
										</td>
									</tr>
						<?php
									$cnt++;
								}
							}
						?>
						</tbody>
					</table>
				</div>
				<?php
					if($pages){
				?>
						<div class="box-footer clearfix">
							<?php echo $pages ?>
						</div>
				<?php
					}
				?>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="client_id" id="client_id" />
<div class="modal fade modal-primary" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Delete Client</h4>
			</div>
			<div class="modal-body">
				<p id="message_approved">Are you sure to delete the client?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="cmdDeleteYes">Yes</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function delete_item(client_id){
		$('#client_id').val(client_id);
		$('#delete_modal').modal('show');
	}

	$('#cmdDeleteYes').click(
		function(){
			$.ajax({
				url: '<?php echo site_url("client/delete") ?>',
				type: 'POST',
				data: {
						client_id: $('#client_id').val()
				},
				success: function(){
					window.location = '<?php echo current_url() ?>'
				}
			})
		}
	)
</script>