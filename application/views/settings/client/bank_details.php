<?php
	if($this->cart->total_items() > 0){
?>
		<table class="table table-striped table-bordered">
			<tbody>
				<?php
					foreach($this->cart->contents() as $items){
				?>
						<tr>
							<td><?php echo $items['name'] ?></td>
							<td><?php echo $items['options']['account_number'] ?></td>
							<td><a data-toggle="tooltip" data-placement="top" title="Delete Bank Details" href="javascript:void(0)" onclick="delete_item('<?php echo $items['rowid'] ?>')"><i class="fa fa-trash"> </i></a></td>
						</tr>
				<?php
					}
				?>
			</tbody>
		</table>
<?php
	}
?>