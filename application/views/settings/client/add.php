<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo $page_sub_title ?></h3>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="client_name" class="col-sm-4 control-label">Client Name <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo set_value('client_name') ?>" name="client_name" id="client_name" />
							</div>
						</div>
						<div class="form-group">
							<label for="address" class="col-sm-4 control-label">Address</label>
							<div class="col-sm-8">
								<textarea name="address" id="address" class="form-control"><?php echo set_value('address') ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="business_type" class="col-sm-4 control-label">Business Type</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo set_value('business_type') ?>" name="business_type" id="business_type" />
							</div>
						</div>
						<div class="form-group">
							<label for="person_in_charge" class="col-sm-4 control-label">Person In Charge</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo set_value('person_in_charge') ?>" name="person_in_charge" id="person_in_charge" />
							</div>
						</div>
						<div class="form-group">
							<label for="contacts" class="col-sm-4 control-label">Contacts</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" value="<?php echo set_value('contacts') ?>" name="contacts" id="contacts" />
							</div>
						</div>
					</div>
					<div class="box-body">
						<div id="bank_message" class="hide">
							<div class="alert alert-danger alert-dismissable">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
								<h4><i class="icon fa fa-ban"></i> Error</h4>
								<p>&nbsp;</p>
							</div>
						</div>
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th width="50%">Bank Name and Branch</th>
									<th width="30%">Account Number</th>
									<th width="20%">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<?php
											if(is_array($banks)){
										?>
											<select name="bank" id="bank" class="form-control">
												<option value=""> - select bank - </option>
												<?php
													foreach($banks as $bank){
												?>
														<option value="<?php echo $bank['id'].'-'.$bank['bank_name'].' '.$bank['branch'] ?>"><?php echo $bank['bank_name'].' - '.$bank['branch'] ?></option>
												<?php
													}
												?>
											</select>
										<?php
											}
										?>
									</td>
									<td><input type="text" name="account_number" id="account_number" class="form-control" /></td>
									<td><button type="button" class="btn btn-primary btn-flat" name="cmdAdd" id="cmdAdd">Add</button></td>
								</tr>
							</tbody>
						</table>
						<div id="display_bank">
							<?php
								if($this->cart->total_items() > 0){
							?>
									<table class="table table-striped table-bordered">
										<tbody>
											<?php
												foreach($this->cart->contents() as $items){
											?>
													<tr>
														<td><?php echo $items['name'] ?></td>
														<td><?php echo $items['options']['account_number'] ?></td>
														<td><a data-toggle="tooltip" data-placement="top" title="Delete Bank Details" href="javascript:void(0)" onclick="delete_item('<?php echo $items['rowid'] ?>')"><i class="fa fa-trash"> </i></a></td>
													</tr>
											<?php
												}
											?>
										</tbody>
									</table>
							<?php
								}
							?>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$('#cmdAdd').click(
		function(){
			var error = '';
			if($('#bank').val() == ''){
				error = 1;
				$('#bank_message p').append('Please select bank.<br/>');
			}

			if($('#account_number').val() == ''){
				error = 1;
				$('#bank_message p').append('Please enter the bank account number.');
			}

			if(error == 1){
				$('#bank_message').removeClass('hide');
				$('#bank_message').addClass('show');
			} else {
				$.ajax({
					url: '<?php echo site_url("client/add_bank") ?>',
					type: 'POST',
					data: {
							bank_id: $('#bank').val(),
							account_number: $('#account_number').val()
					},
					success: function(data){
						$('#display_bank').html(data);
						$('#bank').val('');
						$('#account_number').val('');
					}
				})
			}
		}
	)

	function delete_item(rowid){
		$.ajax({
			url: '<?php echo site_url("client/remove_bank") ?>',
			type: 'POST',
			data: {
					rowid: rowid
			},
			success: function(data){
				$('#display_bank').html(data);
				$('#bank').val('');
				$('#account_number').val('');
			}
		})
	}
</script>