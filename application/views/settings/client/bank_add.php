<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-8">
			<div class="box box-primary">
				<form role="form" class="form-horizontal" method="POST">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
								<h3 class="box-title"><?php echo $page_sub_title ?></h3>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
								<a href="<?php echo site_url('client/view/'.$id) ?>" class="btn btn-info btn-sm btn-flat">View Client Details</a>
							</div>
						</div>
					</div>
					<div class="box-body">
						<?php
							if($message){
						?>
								<div class="alert alert-success alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-check"></i> Save</h4>
									<p><?php echo $message ?></p>
								</div>
						<?php
							}
						?>
						<?php
							$error_message = strlen(validation_errors());
							if($error_message > 0){
						?>
								<div class="alert alert-danger alert-dismissable">
									<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
									<h4><i class="icon fa fa-ban"></i> Error</h4>
									<?php echo validation_errors(); ?>
								</div>
						<?php
							}
						?>
						<div class="form-group">
							<label for="client_name" class="col-sm-4 control-label">Client Name</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" disabled="disabled" value="<?php echo $client_name ?>"/>
							</div>
						</div>
						<div class="form-group">
							<label for="bank" class="col-sm-4 control-label">Bank and Branch <span class="required">*</span></label>
							<div class="col-sm-8">
								<?php
									if(is_array($banks)){
								?>
									<select name="bank" id="bank" class="form-control">
										<option value=""> - select bank - </option>
										<?php
											foreach($banks as $bank){
										?>
												<option value="<?php echo $bank['id'].'-'.$bank['bank_name'].' '.$bank['branch'] ?>"><?php echo $bank['bank_name'].' - '.$bank['branch'] ?></option>
										<?php
											}
										?>
									</select>
								<?php
									}
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="account_number" class="col-sm-4 control-label">Account Number <span class="required">*</span></label>
							<div class="col-sm-8">
								<input type="text" name="account_number" id="account_number" class="form-control" />
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div align="right"><button type="submit" class="btn btn-primary btn-flat">Save</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>