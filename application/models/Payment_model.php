<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Payment_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $payment
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($payment){
			if(is_array($payment)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						payment
						(
							company_id,
							client_id,
							date_created,
							total,
							payment_no,
							payment_year,
							series_no
						)
					VALUES
						(
							'.$payment['company_id'].',
							'.$payment['client_id'].',
							"'.$payment['date_created'].'",
							"'.$payment['total'].'",
							"'.$payment['payment_no'].'",
							"'.$payment['payment_year'].'",
							'.$payment['series_no'].'
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return $this->db->insert_id();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* save_detail()
		* @access public
		* @param array $payment_detail
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save_detail($payment_detail){
			if(is_array($payment_detail)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						payment_details
						(
							payment_id,
							billing_detail_id,
							paid_amount,
							mode_payment_id
						)
					VALUES
						(
							'.$payment_detail['payment_id'].',
							'.$payment_detail['billing_detail_id'].',
							"'.$payment_detail['paid_amount'].'",
							'.$payment_detail['mode_payment_id'].'
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_total()
		* @access public
		* @param array $search
		* @return int total number of payment records
		* @return boolean FALSE if fail
		*/
		public function get_total($search){
			// generate query
			$query = '
				SELECT
					*
				FROM
					payment
				WHERE
					id != 0
			';

			if($search["company_id"]){
				$query .= '
					AND
						company_id
					IN
						('.$this->db->escape_str($search["company_id"]).')
				';
			}

			if($search['client_id']){
				$query .= '
					AND
						client_id = '.$this->db->escape_str($search["client_id"]).'
				';
			}

			if($search['date_from'] && $search['date_to']){
				$query .= '
					AND
						(date_done BETWEEN "'.$this->db->escape_str($search['date_from']).'" AND "'.$this->db->escape_str($search['date_to']).'")
				';
			}

			$result = $this->db->query($query);
			return $result->num_rows();
		}

		/**
		* get_list()
		* @access public
		* @param int $offset
		* @param array $search
		* @return array list of clients
		* @return boolean FALSE if fail
		*/
		public function get_list($offset, $search){
			// generate and execute query
			$query = '
				SELECT
					payment.*,
					company.company_name,
					client.client_name
				FROM
					payment
				LEFT JOIN
					client
				ON
					client.id = payment.client_id
				LEFT JOIN
					company
				ON
					company.id = payment.company_id
				WHERE
					payment.id != 0
			';

			if($search["company_id"]){
				$query .= '
					AND
						payment.company_id
					IN
						('.$this->db->escape_str($search["company_id"]).')
				';
			}

			if($search['client_id']){
				$query .= '
					AND
						payment.client_id = '.$this->db->escape_str($search["client_id"]).'
				';
			}

			if($search['date_from'] && $search['date_to']){
				$query .= '
					AND
						(payment.date_created BETWEEN "'.$this->db->escape_str($search['date_from']).'" AND "'.$this->db->escape_str($search['date_to']).'")
				';
			}

			$query .= '
				ORDER BY
					payment.id
				DESC
				LIMIT
					'.$offset.', '.PAYMENT_PER_PAGE.'
			';

			// execute query
			$result = $this->db->query($query);

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array payment information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						payment.*,
						company.company_name,
						client.client_name
					FROM
						payment
					LEFT JOIN
						client
					ON
						client.id = payment.client_id
					LEFT JOIN
						company
					ON
						company.id = payment.company_id
					WHERE
						payment.id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_details()
		* @access public
		* @param int $payment_id
		* @return array payment details information
		* @return boolean FALSE if not found
		*/
		public function get_details($payment_id){
			if($payment_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						income.date_done,
						mode_payment.mode_payment,
						project.project_name,
						project.project_code,
						project.transaction_date,
						billing_details.id AS billing_detail_id,
						billing_details.income_id,
						billing.date_created,
						payment_details.*
					FROM
						payment_details
					LEFT JOIN
						billing_details
					ON
						billing_details.id = payment_details.billing_detail_id
					LEFT JOIN
						billing
					ON
						billing.id = billing_details.billing_id
					LEFT JOIN
						income
					ON
						billing_details.income_id = income.id
					LEFT JOIN
						project
					ON
						project.id = income.project_id
					LEFT JOIN
						mode_payment
					ON
						mode_payment.id = payment_details.mode_payment_id
					WHERE
						payment_details.payment_id = '.$payment_id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete_payment_detail()
		* @access public
		* @param int $income_id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete_payment_detail($income_id){
			if($income_id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						payment_details
					WHERE
						income_id = '.$income_id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $payment
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($payment){
			if(is_array($payment)){
				// generate and execute query
				$this->db->query('
					UPDATE
						payment
					SET
						company_id = '.$payment['company_id'].',
						total = "'.$payment['total'].'"
					WHERE
						id = '.$payment['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update_detail()
		* @access public
		* @param array $payment_detail
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update_detail($payment_detail){
			if(is_array($payment_detail)){
				// generate and execute query
				$this->db->query('
					UPDATE
						payment_details
					SET
						paid_amount = "'.$payment_detail['paid_amount'].'"
					WHERE
						payment_id = '.$payment_detail['payment_id'].'
					AND
						billing_detail_id = '.$payment_detail['billing_detail_id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						payment
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_last_series()
		* @access public
		* @param int $year
		* @return array last series
		* @return boolean FALSE if fail
		*/
		public function get_last_series($year){
			if(strlen($year) == 4){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						series_no
					FROM
						payment
					WHERE
						payment_year = '.$year.'
					ORDER BY
						series_no
					DESC
					LIMIT 1
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* payment_monitoring()
		* @access public
		* @param array $search
		* @return array list of payment records
		* @return boolean FALSE if fail
		*/
		public function payment_monitoring($search){
			if(is_array($search)){
				// generate and execute query
				$query = '
					SELECT
						payment.date_created,
						payment.payment_no,
						payment.payment_year,
						payment.series_no,
						project.project_name,
						income.date_done,
						payment_details.paid_amount,
						billing_details.billed_amount
					FROM
						payment
					LEFT JOIN
						payment_details
					ON
						payment_details.payment_id = payment.id
					LEFT JOIN
						billing_details
					ON
						billing_details.id = payment_details.billing_detail_id
					LEFT JOIN
						income
					ON
						income.id = billing_details.income_id
					LEFT JOIN
						project
					ON
						project.id = income.project_id
					WHERE
						payment.company_id = '.$search['company_id'].'
				';

				if($search['client_id']){
					$query .= '
						AND
							payment.client_id = '.$search['client_id'].'
					';
				}

				if($search['date_from'] && $search['date_to']){
					$query .= '
						AND
							(payment.date_created BETWEEN "'.$this->db->escape_str($search['date_from']).'" AND "'.$this->db->escape_str($search['date_to']).'")
					';
				}
				$result = $this->db->query($query);

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* save_file()
		* @access public
		* @param array $file_info
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save_file($file_info){
			if(is_array($file_info)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						payment_file
						(
							payment_id,
							filename
						)
					VALUES
						(
							'.$file_info['payment_id'].',
							"'.$this->db->escape_str($file_info['filename']).'"
						)
				');

				// check query result
				if($this->db->affected_rows() > 0){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_files()
		* @access public
		* @param int $payment_id
		* @return array list of files
		* @return boolean FALSE if fail
		*/
		public function get_files($payment_id){
			if($payment_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						payment_file
					WHERE
						payment_id = '.$payment_id.'
				');

				// check query result
				if($result->num_rows() > 0){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

	}

?>