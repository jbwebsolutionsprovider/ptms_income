<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Access_role_model extends CI_Model{

		/**
		* has_role()
		* @access public
		* @param string $module_name
		* @param string $role_name
		* @return boolean TRUE if allowed to access
		* @return boolean FALSE if not allowed to access
		*/
		public function has_role($user_id, $module_name, $role_name){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					module_roles.module_id,
					module_roles.role_id
				FROM
					user_roles
				LEFT JOIN
					module_roles
				ON
					module_roles.id = user_roles.module_roles_id
				LEFT JOIN
					modules
				ON
					modules.id = module_roles.module_id
				LEFT JOIN
					roles
				ON
					roles.id = module_roles.role_id
				WHERE
					user_roles.user_id = '.$user_id.'
				AND
					modules.alias_name = "'.$this->db->escape_str($module_name).'"
				AND
					roles.role_name = "'.$this->db->escape_str($role_name).'"
			');

			// check query result
			if($result->num_rows()){
				return TRUE;
			} else {
				return FALSE;
			}
		}

	}

?>