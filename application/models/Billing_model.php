<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Billing_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $billing
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($billing){
			if(is_array($billing)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						billing
						(
							company_id,
							client_id,
							billed_by,
							date_created,
							total,
							billing_no,
							series_no,
							billing_year
						)
					VALUES
						(
							'.$billing['company_id'].',
							'.$billing['client_id'].',
							'.$billing['billed_by'].',
							"'.$billing['date_created'].'",
							"'.$billing['total'].'",
							"'.$billing['billing_no'].'",
							"'.$billing['series_no'].'",
							"'.$billing['billing_year'].'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return $this->db->insert_id();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* save_detail()
		* @access public
		* @param array $billing_detail
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save_detail($billing_detail){
			if(is_array($billing_detail)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						billing_details
						(
							billing_id,
							income_id,
							billed_amount,
							mode_payment_id
						)
					VALUES
						(
							'.$billing_detail['billing_id'].',
							'.$billing_detail['income_id'].',
							"'.$billing_detail['billed_amount'].'",
							'.$billing_detail['mode_payment_id'].'
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_total()
		* @access public
		* @param array $search
		* @return int total number of billing records
		* @return boolean FALSE if fail
		*/
		public function get_total($search){
			// generate query
			$query = '
				SELECT
					*
				FROM
					billing
				WHERE
					id != 0
			';

			if($search["company_id"]){
				$query .= '
					AND
						company_id
					IN
						('.$this->db->escape_str($search["company_id"]).')
				';
			}

			if($search['client_id']){
				$query .= '
					AND
						client_id = '.$this->db->escape_str($search["client_id"]).'
				';
			}

			if($search['date_from'] && $search['date_to']){
				$query .= '
					AND
						(date_created BETWEEN "'.$this->db->escape_str($search['date_from']).'" AND "'.$this->db->escape_str($search['date_to']).'")
				';
			}

			$result = $this->db->query($query);
			return $result->num_rows();
		}

		/**
		* get_list()
		* @access public
		* @param int $offset
		* @param array $search
		* @return array list of clients
		* @return boolean FALSE if fail
		*/
		public function get_list($offset, $search = ''){
			// generate and execute query
			$query = '
				SELECT
					billing.*,
					company.company_name,
					client.client_name,
					user.firstname,
					user.lastname
				FROM
					billing
				LEFT JOIN
					client
				ON
					client.id = billing.client_id
				LEFT JOIN
					company
				ON
					company.id = billing.company_id
				LEFT JOIN
					user
				ON
					user.id = billing.billed_by
				WHERE
					billing.id != 0
			';

			if($search["company_id"]){
				$query .= '
					AND
						billing.company_id
					IN
						('.$this->db->escape_str($search["company_id"]).')
				';
			}

			if($search['client_id']){
				$query .= '
					AND
						billing.client_id = '.$this->db->escape_str($search["client_id"]).'
				';
			}

			if($search['date_from'] && $search['date_to']){
				$query .= '
					AND
						(billing.date_created BETWEEN "'.$this->db->escape_str($search['date_from']).'" AND "'.$this->db->escape_str($search['date_to']).'")
				';
			}

			$query .= '

				ORDER BY
					billing.id
				DESC
				LIMIT
					'.$offset.', '.BILLING_PER_PAGE.'
			';

			// execute query
			$result = $this->db->query($query);

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array billing information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						billing.*,
						company.company_name,
						client.client_name,
						user.firstname,
						user.lastname
					FROM
						billing
					LEFT JOIN
						client
					ON
						client.id = billing.client_id
					LEFT JOIN
						company
					ON
						company.id = billing.company_id
					LEFT JOIN
						user
					ON
						user.id = billing.billed_by
					WHERE
						billing.id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_details()
		* @access public
		* @param int $billing_id
		* @return array list billing details information
		* @return boolean FALSE if not found
		*/
		public function get_details($billing_id){
			if($billing_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						billing_details.billed_amount,
						billing_details.income_id,
						project.transaction_date,
						project.project_code,
						project.project_name,
						income.date_done,
						income.contract_price,
						mode_payment.mode_payment
					FROM
						billing_details
					LEFT JOIN
						income
					ON
						income.id = billing_details.income_id
					LEFT JOIN
						project
					ON
						project.id = income.project_id
					LEFT JOIN
						mode_payment
					ON
						mode_payment.id = billing_details.mode_payment_id
					WHERE
						billing_details.billing_id = '.$billing_id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_details_info()
		* @access public
		* @param int $billing_detail_id
		* @return array billing details information
		* @return boolean FALSE if not found
		*/
		public function get_details_info($billing_detail_id){
			if($billing_detail_id > 0){
				$result = $this->db->query('
					SELECT
						*
					FROM
						billing_details
					WHERE
						id = '.$billing_detail_id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete_billing_detail()
		* @access public
		* @param int $income_id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete_billing_detail($income_id){
			if($income_id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						billing_details
					WHERE
						income_id = '.$income_id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $billing
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($billing){
			if(is_array($billing)){
				// generate and execute query
				$this->db->query('
					UPDATE
						billing
					SET
						company_id = '.$billing['company_id'].',
						billed_by = '.$billing['billed_by'].',
						total = "'.$billing['total'].'"
					WHERE
						id = '.$billing['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update_detail()
		* @access public
		* @param array $billing_details
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update_detail($billing_details){
			if(is_array($billing_details)){
				// generate and execute query
				$this->db->query('
					UPDATE
						billing_details
					SET
						billed_amount = "'.$billing_details['billed_amount'].'"
					WHERE
						billing_id = '.$billing_details['billing_id'].'
					AND
						income_id = '.$billing_details['income_id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						billing
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_client_projects_for_payment()
		* @access public
		* @param int $client_id
		* @param int $company_id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function get_client_projects_for_payment($client_id, $company_id){
			if($client_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						income.date_done,
						billing_details.mode_payment_id,
						project.project_name,
						project.project_code,
						project.transaction_date,
						billing_details.id AS billing_detail_id,
						billing_details.income_id,
						billing_details.billing_id,
						billing_details.billed_amount,
						billing.date_created
					FROM
						billing
					LEFT JOIN
						billing_details
					ON
						billing.id = billing_details.billing_id
					LEFT JOIN
						income
					ON
						billing_details.income_id = income.id
					LEFT JOIN
						project
					ON
						project.id = income.project_id
					WHERE
						billing.is_paid = "no"
					AND
						billing.client_id = '.$this->db->escape_str($client_id).'
					AND
						billing.company_id = '.$this->db->escape_str($company_id).'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update_paid_status()
		* @access public
		* @param array $paid_status
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update_paid_status($paid_status){
			if(is_array($paid_status)){
				// generate and execute query
				$this->db->query('
					UPDATE
						billing
					SET
						is_paid = "'.$paid_status['is_paid'].'",
						date_paid = "'.$paid_status['date_paid'].'"
					WHERE
						id = '.$paid_status['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_last_series()
		* @access public
		* @param int $year
		* @return array last series
		* @return boolean FALSE if fail
		*/
		public function get_last_series($year){
			if(strlen($year) == 4){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						series_no
					FROM
						billing
					WHERE
						billing_year = '.$year.'
					ORDER BY
						series_no
					DESC
					LIMIT 1
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* billing_monitoring()
		* @access public
		* @param array $search
		* @return array list of billing records
		* @return boolean FALSE if fail
		*/
		public function billing_monitoring($search){
			if(is_array($search)){
				// generate and execute query
				$query = '
					SELECT
						billing.date_created,
						billing.billing_no,
						billing.billing_year,
						billing.series_no,
						project.project_name,
						income.date_done,
						user.firstname,
						user.lastname,
						billing.total,
						billing.is_paid
					FROM
						billing
					LEFT JOIN
						billing_details
					ON
						billing_details.billing_id = billing.id
					LEFT JOIN
						income
					ON
						income.id = billing_details.income_id
					LEFT JOIN
						project
					ON
						project.id = income.project_id
					LEFT JOIN
						user
					ON
						user.id = billing.billed_by
					WHERE
						billing.company_id = '.$search['company_id'].'
					AND
						billing.billing_status = "'.$search['billing_status'].'"
				';

				if($search['client_id']){
					$query .= '
						AND
							billing.client_id = '.$search['client_id'].'
					';
				}

				if($search['date_from'] && $search['date_to']){
					$query .= '
						AND
							(billing.date_created BETWEEN "'.$this->db->escape_str($search['date_from']).'" AND "'.$this->db->escape_str($search['date_to']).'")
					';
				}

				if($search['is_paid']){
					$query .= '
						AND
							billing.is_paid = "'.$this->db->escape_str($search['is_paid']).'"
					';
				}

				$result = $this->db->query($query);

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* save_file()
		* @access public
		* @param array $file_info
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save_file($file_info){
			if(is_array($file_info)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						billing_file
						(
							billing_id,
							filename
						)
					VALUES
						(
							'.$file_info['billing_id'].',
							"'.$this->db->escape_str($file_info['filename']).'"
						)
				');

				// check query result
				if($this->db->affected_rows() > 0){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_files()
		* @access public
		* @param int $billing_id
		* @return array list of files
		* @return boolean FALSE if fail
		*/
		public function get_files($billing_id){
			if($billing_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						billing_file
					WHERE
						billing_id = '.$billing_id.'
				');

				// check query result
				if($result->num_rows() > 0){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

	}

?>