<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Report_model extends CI_Model{

		/**
		* get_summary()
		* @access public
		* @param array $summary
		* @return none
		*/
		public function get_summary($summary){
			if(is_array($summary)){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						project_id,
						contract_price,
						paid_amount,
						unpaid_amount
					FROM
						income
					WHERE
						date_done
					BETWEEN
						"'.$summary['date_from'].'"
					AND
						"'.$summary['date_to'].'"
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_company_summary()
		* @access public
		* @param int $company_id
		* @return none
		*/
		public function get_company_summary($company_id){
			if($company_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						income_category.income_category_name,
						income.date_done,
						billing.date_created,
						income.paid_amount,
						income.unpaid_amount,
						income.contract_price
					FROM
						income
					LEFT JOIN
						income_category
					ON
						income_category.id = income.income_category_id
					LEFT JOIN
						billing_details
					ON
						billing_details.income_id = income.id
					LEFT JOIN
						billing
					ON
						billing.id = billing_details.billing_id
					WHERE
						income.company_id = '.$company_id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_transport_summary()
		* @access public
		* @param array $summary
		* @return none
		*/
		public function get_transport_summary($summary){
			if(is_array($summary)){
				$query = '
					SELECT
						income.quantity,
						income.total_income,
						income.paid_amount,
						income.unpaid_amount,
						transport.transport_name
					FROM
						income
					LEFT JOIN
						transport
					ON
						income.transport = transport.id
					WHERE
				';

				if(is_array($summary)){
					if($summary['transport']){
						$query .= '
							income.transport = '.$summary['transport'].'
							AND
						';
					}

					if($summary['date_from'] && $summary['date_to']){
						$query .= '
								(income.date_done BETWEEN "'.$this->db->escape_str($summary['date_from']).'" AND "'.$this->db->escape_str($summary['date_to']).'")
							AND
						';
					}
				}

				$query .='
							income_category_id = 3
				';

				$result = $this->db->query($query);

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_transport_summary_monthly()
		* @access public
		* @param array $summary
		* @return none
		*/
		public function get_transport_summary_monthly($summary){
			if(is_array($summary)){
				// generate and execute query
				$query = '
					SELECT
						income.quantity,
						unit_type.unit_type_name,
						income.place_from,
						income.place_to
					FROM
						income
					LEFT JOIN
						unit_type
					ON
						unit_type.id = income.unit_type_id
					WHERE
				';

				if(is_array($summary) && $summary['transport'] != ''){
					if($summary['transport']){
						$query .= '
							income.transport = '.$summary['transport'].'
							AND
						';
					}
				}

				$query .= '
						income.income_category_id = 3
					AND
						date_done = "'.$summary['date_done'].'"
				';
				$result = $this->db->query($query);

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

	}

?>