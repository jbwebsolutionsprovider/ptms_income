<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Unit_type_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $unit_type
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($unit_type){
			if(is_array($unit_type)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						unit_type
						(
							unit_type_name,
							description
						)
					VALUES
						(
							"'.$this->db->escape_str($unit_type['unit_type_name']).'",
							"'.$this->db->escape_str($unit_type['description']).'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_list()
		* @access public
		* @param none
		* @return array list of unit types
		* @return boolean FALSE if fail
		*/
		public function get_list(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					unit_type
				ORDER BY
					unit_type_name
				ASC
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array unit type information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						unit_type
					WHERE
						id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $unit_type
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($unit_type){
			if(is_array($unit_type)){
				// generate and execute query
				$this->db->query('
					UPDATE
						unit_type
					SET
						unit_type_name = "'.$this->db->escape_str($unit_type['unit_type_name']).'",
						description = "'.$this->db->escape_str($unit_type['description']).'"
					WHERE
						id = '.$unit_type['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						unit_type
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

	}

?>