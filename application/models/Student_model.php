<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Student_model extends CI_Model{

		/**
		* get_list()
		* @access public
		* @param none
		* @return array list of students
		* @return boolean FALSE if fail
		*/
		public function get_list($search){
			// generate and execute query
			$query = '
				SELECT
					*
				FROM
					project_student
				WHERE
					id != ""
			';

			if(is_array($search)){
				if($search['firstname']){
					$query .= '
						AND
							firstname LIKE "%'.$this->db->escape_str($search["firstname"]).'%"
					';
				}

				if($search['lastname']){
					$query .= '
						AND
							lastname LIKE "%'.$this->db->escape_str($search["lastname"]).'%"
					';
				}

				if($search['hauler_company']){
					$query .= '
						AND
							hauler_company LIKE "%'.$this->db->escape_str($search["hauler_company"]).'%"
					';
				}
			}
			$result = $this->db->query($query);

			// check query result
			if($result->num_rows() > 0){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

	}

?>