<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Income_category_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $income_category
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($income_category){
			if(is_array($income_category)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						income_category
						(
							income_category_name,
							description
						)
					VALUES
						(
							"'.$this->db->escape_str($income_category['income_category_name']).'",
							"'.$this->db->escape_str($income_category['description']).'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_list()
		* @access public
		* @param none
		* @return array list of income categories
		* @return boolean FALSE if fail
		*/
		public function get_list(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					income_category
				ORDER BY
					income_category_name
				ASC
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array income category information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						income_category
					WHERE
						id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $income_category
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($income_category){
			if(is_array($income_category)){
				// generate and execute query
				$this->db->query('
					UPDATE
						income_category
					SET
						income_category_name = "'.$this->db->escape_str($income_category['income_category_name']).'",
						description = "'.$this->db->escape_str($income_category['description']).'"
					WHERE
						id = '.$income_category['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						income_category
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

	}

?>