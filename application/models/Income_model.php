<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Income_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $income
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($income){
			if(is_array($income)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						income
						(
							company_id,
							date_done,
							client_id,
							project_id,
							income_type_id,
							income_category_id,
							contract_price,
							total_income,
							unpaid_amount,
							total_income_type,
							transport,
							unit_type_id,
							quantity,
							place_from,
							place_to,
							cargoes,
							price_per_unit,
							remarks
						)
					VALUES
						(
							'.$income['company_id'].',
							"'.$this->db->escape_str($income['date_done']).'",
							'.$income['client_id'].',
							'.$income['project_id'].',
							'.$income['income_type_id'].',
							'.$income['income_category_id'].',
							"'.$income['contract_price'].'",
							"'.$income['total_income'].'",
							"'.$income['contract_price'].'",
							"'.$this->db->escape_str($income['total_income_type']).'",
							"'.$this->db->escape_str($income['transport']).'",
							'.$income['unit_type_id'].',
							"'.$income['quantity'].'",
							"'.$this->db->escape_str($income['place_from']).'",
							"'.$this->db->escape_str($income['place_to']).'",
							"'.$this->db->escape_str($income['cargoes']).'",
							"'.$income['price_per_unit'].'",
							"'.$this->db->escape_str($income['remarks']).'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return $this->db->insert_id();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_total()
		* @access public
		* @param array $search
		* @return int total number of income records
		* @return boolean FALSE if fail
		*/
		public function get_total($search){
			// generate query
			$query = '
				SELECT
					id
				FROM
					income
				WHERE
					id != 0
			';

			if($search["company_id"]){
				$query .= '
					AND
						income.company_id
					IN
						('.$this->db->escape_str($search["company_id"]).')
				';
			}

			if($search['client_id']){
				$query .= '
					AND
						income.client_id = '.$this->db->escape_str($search["client_id"]).'
				';
			}

			if($search['date_from'] && $search['date_to']){
				$query .= '
					AND
						(income.date_done BETWEEN "'.$this->db->escape_str($search['date_from']).'" AND "'.$this->db->escape_str($search['date_to']).'")
				';
			}

			$result = $this->db->query($query);
			return $result->num_rows();
		}

		/**
		* get_list()
		* @access public
		* @param int $offset
		* @param string $company
		* @return array list of clients
		* @return boolean FALSE if fail
		*/
		public function get_list($offset, $search){
			// generate and execute query
			$query = '
				SELECT
					income.*,
					client.client_name,
					project.project_name,
					company.company_name
				FROM
					income
				LEFT JOIN
					client
				ON
					client.id = income.client_id
				LEFT JOIN
					project
				ON
					project.id = income.project_id
				LEFT JOIN
					company
				ON
					company.id = income.company_id
				WHERE
					income.id != 0
			';

			if($search["company_id"]){
				$query .= '
					AND
						income.company_id
					IN
						('.$this->db->escape_str($search["company_id"]).')
				';
			}

			if($search['client_id']){
				$query .= '
					AND
						income.client_id = '.$this->db->escape_str($search["client_id"]).'
				';
			}

			if($search['date_from'] && $search['date_to']){
				$query .= '
					AND
						(income.date_done BETWEEN "'.$this->db->escape_str($search['date_from']).'" AND "'.$this->db->escape_str($search['date_to']).'")
				';
			}

			$query .= '
				ORDER BY
					income.date_done,
					company.company_name,
					client.client_name,
					project.project_name
				ASC
				LIMIT
					'.$offset.', '.INCOME_PER_PAGE.'
			';
			$result = $this->db->query($query);
			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array income information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						income.*,
						client.client_name,
						company.company_name,
						project.project_name,
						income_type.income_type_name,
						income_category.income_category_name,
						unit_type.unit_type_name,
						transport.transport_name,
						cargo.cargo_name,
						place.place_name
					FROM
						income
					LEFT JOIN
						client
					ON
						client.id = income.client_id
					LEFT JOIN
						company
					ON
						company.id = income.company_id
					LEFT JOIN
						project
					ON
						project.id = income.project_id
					LEFT JOIN
						income_type
					ON
						income_type.id = income.income_type_id
					LEFT JOIN
						income_category
					ON
						income_category.id = income.income_category_id
					LEFT JOIN
						unit_type
					ON
						unit_type.id = income.unit_type_id
					LEFT JOIN
						transport
					ON
						income.transport = transport.id
					LEFT JOIN
						cargo
					ON
						income.cargoes = cargo.id
					LEFT JOIN
						place
					ON
						place.id = income.place_from
					WHERE
						income.id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $income
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($income){
			if(is_array($income)){
				// generate and execute query
				$this->db->query('
					UPDATE
						income
					SET
						company_id = '.$income['company_id'].',
						date_done = "'.$this->db->escape_str($income['date_done']).'",
						client_id = '.$income['client_id'].',
						project_id = '.$income['project_id'].',
						income_type_id = '.$income['income_type_id'].',
						income_category_id = '.$income['income_category_id'].',
						contract_price = "'.$this->db->escape_str($income['contract_price']).'",
						total_income = "'.$this->db->escape_str($income['total_income']).'",
						unpaid_amount = "'.$this->db->escape_str($income['total_income']).'",
						total_income_type = "'.$this->db->escape_str($income['total_income_type']).'",
						transport = "'.$this->db->escape_str($income['transport']).'",
						unit_type_id = '.$income['unit_type_id'].',
						quantity = "'.$this->db->escape_str($income['quantity']).'",
						place_from = "'.$this->db->escape_str($income['place_from']).'",
						place_to = "'.$this->db->escape_str($income['place_to']).'",
						cargoes = "'.$this->db->escape_str($income['cargoes']).'",
						price_per_unit = "'.$this->db->escape_str($income['price_per_unit']).'",
						remarks = "'.$this->db->escape_str($income['remarks']).'"
					WHERE
						id = '.$income['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						income
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update_status()
		* @access public
		* @param array $project
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update_status($project){
			if(is_array($project)){
				// generate and execute query
				$this->db->query('
					UPDATE
						project
					SET
						project_status = "'.$this->db->escape_str($project['project_status']).'"
					WHERE
						id = '.$project['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_client_projects()
		* @access public
		* @param int $client_id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function get_client_projects($client_id){
			if($client_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						project
					WHERE
						client_id = '.$client_id.'
					AND
						project_status = "closed"
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_client_projects_for_billing()
		* @access public
		* @param int $client_id
		* @param int $company_id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function get_client_projects_for_billing($client_id, $company_id){
			if($client_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						income.*,
						project.project_name,
						project.project_code,
						project.transaction_date
					FROM
						income
					LEFT JOIN
						project
					ON
						project.id = income.project_id
					WHERE
						income.client_id = '.$client_id.'
					AND
						income.company_id = '.$company_id.'
					AND
						unpaid_amount > 0
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update_billed_status()
		* @access public
		* @param array $income_status
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update_billed_status($income_status){
			if(is_array($income_status)){
				// generate and execute query
				$this->db->query('
					UPDATE
						income
					SET
						billed_status = "'.$income_status['billed_status'].'"
					WHERE
						id = '.$income_status["id"].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update_paid_status()
		* @access public
		* @param array $income_status
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update_paid_status($income_status){
			if(is_array($income_status)){
				// generate and execute query
				$this->db->query('
					UPDATE
						income
					SET
						payment_status = "'.$income_status['payment_status'].'"
					WHERE
						id = '.$income_status["id"].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_client_projects_for_payment()
		* @access public
		* @param int $client_id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function get_client_projects_for_payment($client_id){
			if($client_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						income.*,
						project.project_name,
						project.project_code,
						project.transaction_date
					FROM
						income
					LEFT JOIN
						project
					ON
						project.id = income.project_id
					WHERE
						income.client_id = '.$client_id.'
					AND
						payment_status = "unpaid"
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update_paid_and_unpaid()
		* @access public
		* @param array $income
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update_paid_and_unpaid($income){
			if(is_array($income)){
				// generate and execute query
				$this->db->query('
					UPDATE
						income
					SET
						unpaid_amount = "'.$income['unpaid_amount'].'",
						paid_amount = "'.$income['paid_amount'].'"
					WHERE
						id = '.$income['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* income_summary()
		* @access public
		* @param array $search
		* @return array list of income
		* @return boolean FALSE if fail
		*/
		public function income_summary($search){
			if(is_array($search)){
				// generate and execute query
				$query = '
					SELECT
						income.date_done,
						project.project_name,
						income_category.income_category_name,
						income.contract_price,
						income.paid_amount,
						income.unpaid_amount,
						client.client_name
					FROM
						income
					LEFT JOIN
						income_category
					ON
						income_category.id = income.income_category_id
					LEFT JOIN
						project
					ON
						project.id = income.project_id
					LEFT JOIN
						client
					ON
						client.id = income.client_id
					WHERE
						income.company_id = '.$search['company_id'].'
					
				';

				// check if there is client selected
				if($search['client_id']){
					$query .= '
						AND
							income.client_id = '.$search['client_id'].'
					';
				}

				if($search['date_from'] && $search['date_to']){
					$query .= '
						AND
							(income.date_done BETWEEN "'.$this->db->escape_str($search['date_from']).'" AND "'.$this->db->escape_str($search['date_to']).'")
					';
				}
				$result = $this->db->query($query);

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_company_monthly_income()
		* @access public
		* @param int $company_id
		* @param string $month
		* @return array list of income
		* @return boolean FALSE if fail
		*/
		public function get_company_monthly_income($company_id, $month){
			if($company_id > 0 && $month){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						income.total_income
					FROM
						income
					WHERE
						company_id = '.$company_id.'
					AND
						date_done
					BETWEEN
						"'.$month.'-01'.'"
					AND
						"'.$month.'-31'.'"
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* save_file()
		* @access public
		* @param array $file_info
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save_file($file_info){
			if(is_array($file_info)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						income_file
						(
							income_id,
							filename
						)
					VALUES
						(
							'.$file_info['income_id'].',
							"'.$this->db->escape_str($file_info['filename']).'"
						)
				');

				// check query result
				if($this->db->affected_rows() > 0){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_files()
		* @access public
		* @param int $income_id
		* @return array list of files
		* @return boolean FALSE if fail
		*/
		public function get_files($income_id){
			if($income_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						income_file
					WHERE
						income_id = '.$income_id.'
				');

				// check query result
				if($result->num_rows() > 0){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

	}

?>