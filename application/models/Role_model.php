<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Role_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param $role array
		* @return boolean TRUE if success
		* @return boolean FALSE if failed
		*/
		public function save($role){
			// generate and execute query
			$this->db->query('
				INSERT INTO
					roles
					(
						role_name
					)
				VALUES
					(
						"'.$this->db->escape_str($role['role_name']).'"
					)
			');

			// check query result
			if($this->db->affected_rows()){
				return TRUE;
			} else {
				return FALSE;
			}
		}

		/**
		* get_roles()
		* @access public
		* @param $role array
		* @return array list of roles
		* @return boolean FALSE if failed
		*/
		public function get_roles(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					roles
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_roles_unassigned()
		* @access public
		* @param string $not_in
		* @return array list of unassigned roles
		* @return boolean FALSE if not found
		*/
		public function get_roles_unassigned($not_in){
			// generate and execute query
			if($not_in){
				$result = $this->db->query('
					SELECT
						*
					FROM
						roles
					WHERE
						id 
					NOT IN('.$not_in.')
				');
			} else {
				$result = $this->db->query('
					SELECT
						*
					FROM
						roles
				');
			}

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

	}

?>