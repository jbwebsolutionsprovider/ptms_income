<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Bank_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $bank
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($bank){
			if(is_array($bank)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						bank
						(
							bank_name,
							branch
						)
					VALUES
						(
							"'.$this->db->escape_str($bank['bank_name']).'",
							"'.$this->db->escape_str($bank['branch']).'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_list()
		* @access public
		* @param none
		* @return array list of cargos
		* @return boolean FALSE if fail
		*/
		public function get_list(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					bank
				ORDER BY
					bank_name
				ASC
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array bank information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						bank
					WHERE
						id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $bank
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($bank){
			if(is_array($bank)){
				// generate and execute query
				$this->db->query('
					UPDATE
						bank
					SET
						bank_name = "'.$this->db->escape_str($bank['bank_name']).'",
						branch = "'.$this->db->escape_str($bank['branch']).'"
					WHERE
						id = '.$bank['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						bank
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

	}

?>