<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class User_group_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $user_group
		* @return int user group ID
		* @return boolean FALSE if fail
		*/
		public function save($user_group){
			// generate and execute query
			$this->db->query('
				INSERT INTO
					user_group
					(
						user_group_name
					)
				VALUES
					(
						"'.$this->db->escape_str($user_group['user_group_name']).'"
					)
			');

			// check query result
			if($this->db->affected_rows()){
				return $this->db->insert_id();
			} else {
				return FALSE;
			}
		}

		/**
		* save_user_group_role()
		* @access public
		* @param array $user_group_role
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save_user_group_role($user_group_role){
			// generate and execute query
			$this->db->query('
				INSERT INTO
					user_group_roles
					(
						user_group_id,
						module_roles_id
					)
				VALUES
					(
						'.$user_group_role['user_group_id'].',
						'.$user_group_role['module_roles_id'].'
					)
			');

			// check query result
			if($this->db->affected_rows()){
				return TRUE;
			} else {
				return FALSE;
			}
		}

		/**
		* get_list()
		* @access public
		* @param none
		* @return array list of user groups
		* @return boolean FALSE if fail
		*/
		public function get_list(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					user_group
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $user_group_id
		* @return array user group information
		* @return boolean FALSE if not found
		*/
		public function get_info($user_group_id){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					user_group
				WHERE
					id = '.$user_group_id.'
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_user_group_roles()
		* @access public
		* @param int $user_group_id
		* @return array list of user group roles
		* @return boolean FALSE if not found
		*/
		public function get_user_group_roles($user_group_id){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					modules.module_name,
					roles.role_name,
					user_group_roles.module_roles_id,
					user_group_roles.id
				FROM
					user_group_roles
				LEFT JOIN
					module_roles
				ON
					module_roles.id = user_group_roles.module_roles_id
				LEFT JOIN
					roles
				ON
					roles.id = module_roles.role_id
				LEFT JOIN
					modules
				ON
					modules.id = module_roles.module_id
				WHERE
					user_group_roles.user_group_id = '.$user_group_id.'
				ORDER BY
					modules.module_name, roles.role_name
				ASC
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* remove_role()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if not found
		*/
		public function remove_role($id){
			// generate and execute query
			$this->db->query('
				DELETE FROM
					user_group_roles
				WHERE
					id = '.$id.'
			');

			// check query result
			if($this->db->affected_rows()){
				return TRUE;
			} else {
				return FALSE;
			}
		}

	}

?>