<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Module_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param $role array
		* @return boolean TRUE if success
		* @return boolean FALSE if failed
		*/
		public function save($module){
			// generate and execute query
			$this->db->query('
				INSERT INTO
					modules
					(
						module_name,
						alias_name
					)
				VALUES
					(
						"'.$this->db->escape_str($module['module_name']).'",
						"'.$this->db->escape_str($module['alias_name']).'"
					)
			');

			// check query result
			if($this->db->affected_rows()){
				return $this->db->insert_id();
			} else {
				return FALSE;
			}
		}

		/**
		* get_modules()
		* @access public
		* @param $role array
		* @return array list of modules
		* @return boolean FALSE if failed
		*/
		public function get_modules(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					modules
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* save_role()
		* @access public
		* @param $module_role array
		* @return boolean TRUE if success
		* @return boolean FALSE if failed
		*/
		public function save_role($module_role){
			// generate and execute query
			$this->db->query('
				INSERT INTO
					module_roles
					(
						module_id,
						role_id
					)
				VALUES
					(
						'.$module_role['module_id'].',
						'.$module_role['role_id'].'
					)
			');

			// check query result
			if($this->db->affected_rows()){
				return TRUE;
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param $module_id int
		* @return array module information
		* @return boolean FALSE if failed
		*/
		public function get_info($module_id){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					modules
				WHERE
					id = '.$module_id.'
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_module_roles()
		* @access public
		* @param $module_id int
		* @return array list of module roles
		* @return boolean FALSE if failed
		*/
		public function get_module_roles($module_id){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					roles.role_name,
					module_roles.role_id,
					module_roles.module_id,
					module_roles.id AS module_roles_id
				FROM
					module_roles
				LEFT JOIN
					roles
				ON
					roles.id = module_roles.role_id
				WHERE
					module_roles.module_id = '.$module_id.'
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* remove_role()
		* @access public
		* @param int $module_id
		* @param int $role_id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function remove_role($module_id, $role_id){
			// generate and execute query
			$this->db->query('
				DELETE FROM
					module_roles
				WHERE
					module_id = '.$module_id.'
				AND
					role_id = '.$role_id.'
			');

			// check query result
			if($this->db->affected_rows()){
				return TRUE;
			} else {
				return FALSE;
			}
		}

		/**
		* get_roles_unassigned()
		* @access public
		* @param string $not_in
		* @return array list of unassigned module roles
		* @return boolean FALSE if not found
		*/
		public function get_roles_unassigned($not_in){
			// generate and execute query
			if($not_in){
				$result = $this->db->query('
					SELECT
						modules.module_name,
						roles.role_name,
						roles.id AS role_id,
						module_roles.id
					FROM
						module_roles
					LEFT JOIN
						modules
					ON
						modules.id = module_roles.module_id
					LEFT JOIN
						roles
					ON
						roles.id = module_roles.role_id
					WHERE
						module_roles.id
					NOT IN('.$not_in.')
					ORDER BY
						modules.module_name, roles.role_name
					ASC
				');
			} else {
				$result = $this->db->query('
					SELECT
						modules.module_name,
						roles.role_name,
						roles.id AS role_id
					FROM
						module_roles
					LEFT JOIN
						modules
					ON
						modules.id = module_roles.module_id
					LEFT JOIN
						roles
					ON
						roles.id = module_roles.role_id
					ORDER BY
						modules.module_name, roles.role_name
					ASC
				');
			}

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}
	}

?>