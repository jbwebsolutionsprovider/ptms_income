<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Place_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $place
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($place){
			if(is_array($place)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						place
						(
							place_name,
							description
						)
					VALUES
						(
							"'.$this->db->escape_str($place['place_name']).'",
							"'.$this->db->escape_str($place['description']).'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_list()
		* @access public
		* @param none
		* @return array list of places
		* @return boolean FALSE if fail
		*/
		public function get_list(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					place
				ORDER BY
					place_name
				ASC
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array place information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						place
					WHERE
						id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $place
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($place){
			if(is_array($place)){
				// generate and execute query
				$this->db->query('
					UPDATE
						place
					SET
						place_name = "'.$this->db->escape_str($place['place_name']).'",
						description = "'.$this->db->escape_str($place['description']).'"
					WHERE
						id = '.$place['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						place
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

	}

?>