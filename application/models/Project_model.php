<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Project_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $project
		* @return int project ID if success
		* @return boolean FALSE if fail
		*/
		public function save($project){
			if(is_array($project)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						project
						(
							project_name,
							client_id,
							project_code,
							project_status,
							project_type,
							transaction_date,
							remarks
						)
					VALUES
						(
							"'.$this->db->escape_str($project['project_name']).'",
							'.$project['client_id'].',
							"'.$this->db->escape_str($project['project_code']).'",
							"'.$this->db->escape_str($project['project_status']).'",
							"'.$this->db->escape_str($project['project_type']).'",
							"'.$this->db->escape_str($project['transaction_date']).'",
							"'.$this->db->escape_str($project['remarks']).'"
						)
				');

				// check query result
				if($this->db->affected_rows() > 0){
					return $this->db->insert_id();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* save_students()
		* @access public
		* @param array $student
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save_students($student){
			if(is_array($student)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						project_student
						(
							project_id,
							firstname,
							lastname,
							hauler_company
						)
					VALUES
						(
							'.$student['project_id'].',
							"'.$this->db->escape_str($student['firstname']).'",
							"'.$this->db->escape_str($student['lastname']).'",
							"'.$this->db->escape_str($student['hauler_company']).'"
						)
				');

				// check query result
				if($this->db->affected_rows() > 0){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_total()
		* @access public
		* @param none
		* @return int total number of clients
		* @return boolean FALSE if fail
		*/
		public function get_total(){
			// generate and execute query
			return $this->db->count_all('project');
		}

		/**
		* get_list()
		* @access public
		* @param int $offset
		* @return array list of clients
		* @return boolean FALSE if fail
		*/
		public function get_list($offset){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					project.*,
					client.client_name
				FROM
					project
				LEFT JOIN
					client
				ON
					client.id = project.client_id
				ORDER BY
					client_name
				ASC
				LIMIT
					'.$offset.', '.CLIENT_PER_PAGE.'
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array project information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						project.*,
						client.client_name
					FROM
						project
					LEFT JOIN
						client
					ON
						client.id = project.client_id
					WHERE
						project.id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $project
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($project){
			if(is_array($project)){
				// generate and execute query
				$this->db->query('
					UPDATE
						project
					SET
						project_name = "'.$this->db->escape_str($project['project_name']).'",
						client_id = '.$project['client_id'].',
						project_code = "'.$this->db->escape_str($project['project_code']).'",
						project_status = "'.$this->db->escape_str($project['project_status']).'",
						project_type = "'.$this->db->escape_str($project['project_type']).'",
						transaction_date = "'.$this->db->escape_str($project['transaction_date']).'",
						remarks = "'.$this->db->escape_str($project['remarks']).'"
					WHERE
						id = '.$project['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						project
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update_status()
		* @access public
		* @param array $project
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update_status($project){
			if(is_array($project)){
				// generate and execute query
				$this->db->query('
					UPDATE
						project
					SET
						project_status = "'.$this->db->escape_str($project['project_status']).'"
					WHERE
						id = '.$project['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_client_projects()
		* @access public
		* @param int $client_id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function get_client_projects($client_id){
			if($client_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						project
					WHERE
						client_id = '.$client_id.'
					AND
						is_used = "no"
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update_is_used()
		* @access public
		* @param array $is_used
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update_is_used($is_used){
			if(is_array($is_used)){
				// generate and execute query
				$this->db->query('
					UPDATE
						project
					SET
						is_used = "'.$this->db->escape_str($is_used['is_used']).'"
					WHERE
						id = '.$is_used['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_students()
		* @access public
		* @param int $project_id
		* @return array list of students
		* @return boolean FALSE if fail
		*/
		public function get_students($project_id){
			if($project_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						project_student.*
					FROM
						project_student
					WHERE
						project_id = '.$project_id.'
				');

				// check query result
				if($result->num_rows() > 0){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

	}

?>