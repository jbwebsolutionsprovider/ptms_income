<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Client_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $client
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($client){
			if(is_array($client)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						client
						(
							client_name,
							address,
							business_type,
							person_in_charge,
							contacts
						)
					VALUES
						(
							"'.$this->db->escape_str($client['client_name']).'",
							"'.$this->db->escape_str($client['address']).'",
							"'.$this->db->escape_str($client['business_type']).'",
							"'.$this->db->escape_str($client['person_in_charge']).'",
							"'.$this->db->escape_str($client['contacts']).'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return $this->db->insert_id();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* save_bank_detail()
		* @access public
		* @param array $bank_detail
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save_bank_detail($bank_detail){
			if(is_array($bank_detail)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						client_bank
						(
							client_id,
							bank_id,
							account_number
						)
					VALUES
						(
							'.$bank_detail['client_id'].',
							'.$bank_detail['bank_id'].',
							"'.$bank_detail['account_number'].'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_total()
		* @access public
		* @param none
		* @return int total number of clients
		* @return boolean FALSE if fail
		*/
		public function get_total(){
			// generate and execute query
			return $this->db->count_all('client');
		}

		/**
		* get_list()
		* @access public
		* @param int $offset
		* @return array list of clients
		* @return boolean FALSE if fail
		*/
		public function get_list($offset){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					client
				ORDER BY
					client_name
				ASC
				LIMIT
					'.$offset.', '.CLIENT_PER_PAGE.'
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_all()
		* @access public
		* @param none
		* @return array list of clients
		* @return boolean FALSE if fail
		*/
		public function get_all(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					client
				ORDER BY
					client_name
				ASC
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array client information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						client
					WHERE
						id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $client
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($client){
			if(is_array($client)){
				// generate and execute query
				$this->db->query('
					UPDATE
						client
					SET
						client_name = "'.$this->db->escape_str($client['client_name']).'",
						address = "'.$this->db->escape_str($client['address']).'",
						business_type = "'.$this->db->escape_str($client['business_type']).'",
						person_in_charge = "'.$this->db->escape_str($client['person_in_charge']).'",
						contacts = "'.$this->db->escape_str($client['contacts']).'"
					WHERE
						id = '.$client['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						client
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_bank_accounts()
		* @access public
		* @param int $client_id
		* @return array list of client bank accounts
		* @return boolean FALSE if fail
		*/
		public function get_bank_accounts($client_id){
			if($client_id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						client_bank.*,
						bank.bank_name,
						bank.branch
					FROM
						client_bank
					LEFT JOIN
						bank
					ON
						bank.id = client_bank.bank_id
					WHERE
						client_bank.client_id = '.$client_id.'
				');

				// check query result
				if($result->num_rows() > 0){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_bank_account_info()
		* @access public
		* @param int $id
		* @return array client bank account information
		* @return boolean FALSE if fail
		*/
		public function get_bank_account_info($id){
			if($id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						client_bank.*,
						client.client_name,
						bank.bank_name,
						bank.branch
					FROM
						client_bank
					LEFT JOIN
						client
					ON
						client.id = client_bank.client_id
					LEFT JOIN
						bank
					ON
						bank.id = client_bank.bank_id
					WHERE
						client_bank.id = '.$id.'
				');

				// check query result
				if($result->num_rows() > 0){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update_bank_detail()
		* @access public
		* @param array $bank_detail
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update_bank_detail($bank_detail){
			if(is_array($bank_detail)){
				// generate and execute query
				$this->db->query('
					UPDATE
						client_bank
					SET
						account_number = "'.$this->db->escape_str($bank_detail['account_number']).'"
					WHERE
						id = '.$bank_detail['id'].'
				');

				// check query result
				if($this->db->affected_rows() > 0){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete_bank()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete_bank($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						client_bank
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows() > 0){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

	}

?>