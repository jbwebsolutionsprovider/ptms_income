<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Company_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $company
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($company){
			if(is_array($company)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						company
						(
							company_name,
							description
						)
					VALUES
						(
							"'.$this->db->escape_str($company['company_name']).'",
							"'.$this->db->escape_str($company['description']).'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_list()
		* @access public
		* @param none
		* @return array list of companies
		* @return boolean FALSE if fail
		*/
		public function get_list(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					company
				ORDER BY
					company_name
				ASC
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array income type information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						company
					WHERE
						id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $company
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($company){
			if(is_array($company)){
				// generate and execute query
				$this->db->query('
					UPDATE
						company
					SET
						company_name = "'.$this->db->escape_str($company['company_name']).'",
						description = "'.$this->db->escape_str($company['description']).'"
					WHERE
						id = '.$company['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						company
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

	}

?>