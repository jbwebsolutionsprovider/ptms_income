<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Cargo_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $cargo
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($cargo){
			if(is_array($cargo)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						cargo
						(
							cargo_name,
							description
						)
					VALUES
						(
							"'.$this->db->escape_str($cargo['cargo_name']).'",
							"'.$this->db->escape_str($cargo['description']).'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_list()
		* @access public
		* @param none
		* @return array list of cargos
		* @return boolean FALSE if fail
		*/
		public function get_list(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					cargo
				ORDER BY
					cargo_name
				ASC
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array cargo information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						cargo
					WHERE
						id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $cargo
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($cargo){
			if(is_array($cargo)){
				// generate and execute query
				$this->db->query('
					UPDATE
						cargo
					SET
						cargo_name = "'.$this->db->escape_str($cargo['cargo_name']).'",
						description = "'.$this->db->escape_str($cargo['description']).'"
					WHERE
						id = '.$cargo['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						cargo
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

	}

?>