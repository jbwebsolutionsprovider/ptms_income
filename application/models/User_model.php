<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	/*
	 * user_exist()
	 * @access public
	 * @param string $username
	 * @param string $password md5 password
	 * @return array user info if account exist
	 * */
	public function user_exist($username, $password){
		$result = $this->db->query('
			SELECT
				*
			FROM
				user
			WHERE
				username = "'.$this->db->escape_str($username).'"
			AND
				userpassword = "'.$this->db->escape_str($password).'"
		');

		// check the result
		if($result->num_rows() > 0){
			return $result->result_array();
		} else {
			return FALSE;
		}
	}

	/*
	 * save_user_token()
	 * @access public
	 * @param int $user_id user ID
	 * @param string $token user generated token
	 * @return void
	 * */
	public function save_user_token($user_id, $token){
		if($user_id && $token){
			// generate and execute query
			$this->db->query('
				UPDATE
					user
				SET
					token = "'.$token.'"
				WHERE
					id = '.$user_id.'
			');

			// check query result
			if($this->db->affected_rows()){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * valid_user_token()
	 * @access public
	 * @param int $user_id user ID
	 * @param string $token user current token
	 * @return boolean TRUE if token is valid
	 * @return boolean FALSE if token is not valid
	 */
	public function valid_user_token($user_id, $token){
		if($user_id && $token){
			$result = $this->db->query('
				SELECT
					token
				FROM
					user
				WHERE
					id = '.$user_id.'
				AND
					token = "'.$this->db->escape_str($token).'"
			');

			// check the result
			if($result->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/*
	 * empty_user_token()
	 * @access public
	 * @param int $user_id user ID
	 * @return boolean TRUE if success
	 * @return boolean FALSE if fail
	 * */
	public function empty_user_token($user_id){
		if($user_id){
			$result = $this->db->query('
				UPDATE
					user
				SET
					token = ""
				WHERE
					id = '.$this->db->escape_str($user_id).'
			');

			// check the result
			if($this->db->affected_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	 * is_email_address_registered()
	 * @access public
	 * @param string $email_address user / diveshop email address
	 * @param int $diveshop_id diveshop ID
	 * @return boolean TRUE if email address is exist
	 * @return boolean FALSE if email address does not exist
	 */
	public function is_email_address_registered($email_address, $diveshop_id = 0){
		if($email_address){
			if($diveshop_id){
				// generate query and result
				$result = $this->db->query('
					SELECT
						email
					FROM
						diveshop
					WHERE
						email = "'.$this->db->escape_str($email_address).'"
					AND
						id != '.$this->db->escape_str($diveshop_id).'
				');
			} else {
				// generate query and result
				$result = $this->db->query('
					SELECT
						email
					FROM
						diveshop
					WHERE
						email = "'.$this->db->escape_str($email_address).'"
				');
			}

			// check the query result
			if($result->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/*
	* save()
	* @access public
	* @param array $user_data user data
	* @return boolean TRUE if success
	* @return boolean FALSE if fail
	*/
	public function save($user_data){
		if(is_array($user_data) && count($user_data) > 0){
			// generate and execute query
			$this->db->query('
				INSERT INTO
					user
					(
						user_group_id,
						username,
						userpassword,
						company_id,
						firstname,
						lastname,
						expiry
					)
				VALUES
					(
						"'.$user_data['user_group_id'].'",
						"'.$this->db->escape_str($user_data['username']).'",
						"'.$this->db->escape_str($user_data['userpassword']).'",
						"'.$user_data['company_id'].'",
						"'.$this->db->escape_str($user_data['firstname']).'",
						"'.$this->db->escape_str($user_data['lastname']).'",
						"'.$user_data['expiry'].'"
					)
			');

			// check query result
			if($this->db->affected_rows() > 0){
				return $this->db->insert_id();
			} else {
				return FALSE;
			}
		}
	}

	/*
	* save_user_role()
	* @access public
	* @param array $user_role user data
	* @return boolean TRUE if success
	* @return boolean FALSE if fail
	*/
	public function save_user_role($user_role){
		// generate and execute query
		$this->db->query('
			INSERT INTO
				user_roles
				(
					user_id,
					module_roles_id
				)
			VALUES
				(
					'.$user_role['user_id'].',
					'.$user_role['module_roles_id'].'
				)
		');

		// check query result
		if($this->db->affected_rows()){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * get_users()
	 * @access public
	 * @param int $company_id
	 * @return array list of users
	 * @return boolean FALSE if no found
	 */
	public function get_users($company_id){
		// generate and execute query
		if($company_id > 0){
			$result = $this->db->query('
				SELECT
					user.*,
					user_group.user_group_name,
					company.company_name
				FROM
					user
				LEFT JOIN
					user_group
				ON
					user_group.id = user.user_group_id
				LEFT JOIN
					company
				ON
					company.id = user.company_id
				WHERE
					company_id = '.$company_id.'
			');
		} else {
			$result = $this->db->query('
				SELECT
					user.*,
					user_group.user_group_name,
					company.company_name
				FROM
					user
				LEFT JOIN
					user_group
				ON
					user_group.id = user.user_group_id
				LEFT JOIN
					company
				ON
					company.id = user.company_id
				WHERE
					is_admin = "no"
			');
		}

		// check query result
		if($result->num_rows() > 0){
			return $result->result_array();
		} else {
			return FALSE;
		}
	}

	/**
	 * delete()
	 * @access public
	 * @param int $user_id user ID
	 * @return boolean TRUE if success
	 * @return boolean FALSE if fail
	 */
	public function delete($user_id){
		// generate and execute query
		$this->db->query('
			DELETE FROM
				user
			WHERE
				id = '.$user_id.'
		');

		// check query result
		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * is_username_exist()
	 * @access public
	 * @param string $username username
	 * @param int $user_id user ID
	 * @return boolean TRUE if exist
	 * @return boolean FALSE if not exist
	 */
	public function is_username_exist($username, $user_id = ''){
		if($username){
			if($user_id){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						user
					WHERE
						username = "'.$this->db->escape_str($username).'"
					AND
						id != '.$user_id.'
				');
			} else {
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						user
					WHERE
						username = "'.$this->db->escape_str($username).'"
				');
			}

			// check query result
			if($result->num_rows() > 0){
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	/**
	* get_user_info()
	* @access public
	* @param int $id
	* @return array user information
	* @return boolean FALSE if not exist
	*/
	public function get_user_info($id){
		// generate and execute query
		if($id){
			$result = $this->db->query('
				SELECT
					user.*,
					user_group.user_group_name,
					company.company_name
				FROM
					user
				LEFT JOIN
					user_group
				ON
					user_group.id = user.user_group_id
				LEFT JOIN
					company
				ON
					company.id = user.company_id
				WHERE
					user.id = '.$id.'
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}
	}

	/**
	* update()
	* @access public
	* @param array $user
	* @return boolean TRUE if success
	* @return boolean FALSE if fail
	*/
	public function update($user){
		// generate and execute query
		$this->db->query('
			UPDATE
				user
			SET
				firstname = "'.$this->db->escape_str($user['firstname']).'",
				lastname = "'.$this->db->escape_str($user['lastname']).'",
				company_id = "'.$user['company_id'].'",
				expiry = "'.$user['expiry'].'"
			WHERE
				id = '.$user['id'].'
		');

		// check the query result
		if($this->db->affected_rows()){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	* update_password()
	* @access public
	* @param array $password
	* @return boolean TRUE if success
	* @return boolean FALSE if fail
	*/

	/**
	* get_user_roles()
	* @access public
	* @param int $user_id
	* @return array list of user roles
	* @return boolean FALSE if fail
	*/
	public function get_user_roles($user_id){
		// generate and execute query
		$result = $this->db->query('
			SELECT
				roles.role_name,
				modules.module_name,
				user_roles.module_roles_id,
				user_roles.id
			FROM
				user_roles
			LEFT JOIN
				module_roles
			ON
				module_roles.id = user_roles.module_roles_id
			LEFT JOIN
				roles
			ON
				roles.id = module_roles.role_id
			LEFT JOIN
				modules
			ON
				modules.id = module_roles.module_id
			WHERE
				user_roles.user_id = '.$user_id.'
			ORDER BY
				modules.module_name, roles.role_name
			ASC
		');

		// check query result
		if($result->num_rows()){
			return $result->result_array();
		} else {
			return FALSE;
		}
	}

	/**
	* save_login()
	* @access public
	* @param array $login
	* @return boolean TRUE if success
	* @return boolean FAIL if fail
	*/
	public function save_login($login){
		// generate and eexcute query
		$this->db->query('
			INSERT INTO
				user
				(
					company_id,
					username,
					userpassword,
					group_id
				)
			VALUES
				(
					'.$this->db->escape_str($login['company_id']).',
					"'.$this->db->escape_str($login['username']).'",
					"'.$this->db->escape_str($login['userpassword']).'",
					'.$this->db->escape_str($login['group_id']).'
				)
		');

		// check query result
		if($this->db->affected_rows()){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	* remove_role()
	* @access public
	* @param int $id
	* @return boolean TRUE if success
	* @return boolean FALSE if fail
	*/
	public function remove_role($id){
		// generate and execute query
		$this->db->query('
			DELETE FROM
				user_roles
			WHERE
				id = '.$id.'
		');

		// check query result
		if($this->db->affected_rows()){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	* change_password()
	* @access public
	* @param array $password
	* @return boolean TRUE if success
	* @return boolean FALSE if fail
	*/
	public function change_password($password){
		// generate and execute query
		$this->db->query('
			UPDATE 
				user
			SET
				userpassword = "'.$this->db->escape_str($password['userpassword']).'"
			WHERE
				id = '.$password['id'].'
		');

		// check query result
		if($this->db->affected_rows()){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	* update_account_status()
	* @access public
	* @param array $account_status
	* @return boolean TRUE if success
	* @return boolean FALSE if fail
	*/
	public function update_account_status($account_status){
		// generate and execute query
		$this->db->query('
			UPDATE
				user
			SET
				status = "'.$this->db->escape_str($account_status['status']).'"
			WHERE
				id = '.$account_status['id'].'
		');

		// check query result
		if($this->db->affected_rows()){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	* get_user_modules()
	* @access public
	* @param int $user_id
	* @return array list of user modules
	* @return boolean FALSE if fail
	*/
	public function get_user_modules($user_id){
		// generate and execute query
		$result = $this->db->query('
			SELECT
				module_roles.module_id,
				modules.module_name
			FROM
				user_roles
			LEFT JOIN
				module_roles
			ON
				module_roles.id = user_roles.module_roles_id
			LEFT JOIN
				modules
			ON
				modules.id = module_roles.module_id
			WHERE
				user_roles.user_id = '.$user_id.'
			GROUP BY
				module_roles.module_id
		');

		// check query result
		if($result->num_rows()){
			return $result->result_array();
		} else {
			return FALSE;
		}
	}

}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */