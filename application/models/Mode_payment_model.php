<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Mode_payment_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $mode_payment
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($mode_payment){
			if(is_array($mode_payment)){
				// generate and execute query
				$this->db->query('
					INSERT INTO
						mode_payment
						(
							mode_payment,
							description
						)
					VALUES
						(
							"'.$this->db->escape_str($mode_payment['mode_payment']).'",
							"'.$this->db->escape_str($mode_payment['description']).'"
						)
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* get_list()
		* @access public
		* @param none
		* @return array list of cargos
		* @return boolean FALSE if fail
		*/
		public function get_list(){
			// generate and execute query
			$result = $this->db->query('
				SELECT
					*
				FROM
					mode_payment
				ORDER BY
					mode_payment
				ASC
			');

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

		/**
		* get_info()
		* @access public
		* @param int $id
		* @return array mode payment information
		* @return boolean FALSE if not found
		*/
		public function get_info($id){
			if($id && $id > 0){
				// generate and execute query
				$result = $this->db->query('
					SELECT
						*
					FROM
						mode_payment
					WHERE
						id = '.$id.'
				');

				// check query result
				if($result->num_rows()){
					return $result->result_array();
				} else {
					return FALSE;
				}
			}
		}

		/**
		* update()
		* @access public
		* @param array $mode_payment
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function update($mode_payment){
			if(is_array($mode_payment)){
				// generate and execute query
				$this->db->query('
					UPDATE
						mode_payment
					SET
						mode_payment = "'.$this->db->escape_str($mode_payment['mode_payment']).'",
						description = "'.$this->db->escape_str($mode_payment['description']).'"
					WHERE
						id = '.$mode_payment['id'].'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

		/**
		* delete()
		* @access public
		* @param int $id
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function delete($id){
			if($id > 0){
				// generate and execute query
				$this->db->query('
					DELETE FROM
						mode_payment
					WHERE
						id = '.$id.'
				');

				// check query result
				if($this->db->affected_rows()){
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}

	}

?>