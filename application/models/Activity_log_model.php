<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Activity_log_model extends CI_Model{

		/**
		* save()
		* @access public
		* @param array $log
		* @return boolean TRUE if success
		* @return boolean FALSE if fail
		*/
		public function save($log){
			// generate and execute query
			$this->db->query('
				INSERT INTO
					logs
					(
						user_id,
						activity,
						action,
						log_time
					)
				VALUES
					(
						'.$log['user_id'].',
						"'.$this->db->escape_str($log['activity']).'",
						"'.$this->db->escape_str($log['action']).'",
						"'.$this->db->escape_str($log['log_time']).'"

					)
			');

			// check query result
			if($this->db->affected_rows()){
				return TRUE;
			} else {
				return FALSE;
			}
		}

		/**
		* total_logs()
		* @access public
		* @param string $is_admin
		* @param int $user_id
		* @return int total number of logs
		* @return boolean FALSE if fail
		*/
		public function total_logs($is_admin, $user_id){
			// generate and execute query
			if($is_admin == 'yes'){
				$result = $this->db->query('
					SELECT
						logs.*,
						user.username
					FROM
						logs
					LEFT JOIN
						user
					ON
						user.id = logs.user_id
					ORDER BY
						logs.id DESC
				');
			} else {
				$result = $this->db->query('
					SELECT
						logs.*,
						user.username
					FROM
						logs
					LEFT JOIN
						user
					ON
						user.id = logs.user_id
					WHERE
						user_id = '.$user_id.'
					ORDER BY
						logs.id DESC
				');
			}

			// check query result
			if($result->num_rows()){
				return $result->num_rows();
			} else {
				return FALSE;
			}
		}

		/**
		* get_logs()
		* @access public
		* @param string $is_admin
		* @param int $user_id
		* @return array list of activity logs
		* @return boolean FALSE if fail
		*/
		public function get_logs($offset, $is_admin, $user_id){
			// generate and execute query
			if($is_admin == 'yes'){
				$result = $this->db->query('
					SELECT
						logs.*,
						user.username
					FROM
						logs
					LEFT JOIN
						user
					ON
						user.id = logs.user_id
					ORDER BY
						logs.id DESC
					LIMIT
						'.$offset.', '.LOGS_PER_PAGE.'
				');
			} else {
				$result = $this->db->query('
					SELECT
						logs.*,
						user.username
					FROM
						logs
					LEFT JOIN
						user
					ON
						user.id = logs.user_id
					WHERE
						user_id = '.$user_id.'
					ORDER BY
						logs.id DESC
					LIMIT
						'.$offset.', '.LOGS_PER_PAGE.'
				');
			}

			// check query result
			if($result->num_rows()){
				return $result->result_array();
			} else {
				return FALSE;
			}
		}

	}

?>