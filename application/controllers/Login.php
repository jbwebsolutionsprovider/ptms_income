<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/login
	 *	- or -
	 * 		http://example.com/index.php/login/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/login/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Activity_log_model');
	}

	public function index(){
		// variable initialization
		$data['message'] = '';
		// set default role
		$newdata = array(
			'has_role_dashboard' => 'no',
			'has_role_forms' => 'no',
			'has_role_settings' => 'no',
			'has_role_reports' => 'no',
			'has_role_admin' => 'no'
		);
		$this->session->set_userdata($newdata);
		// run the form validation
		$this->validate_login();
		if ($this->form_validation->run() == TRUE){
			// check if user account exist
			$is_exist = $this->User_model->user_exist($this->input->post('username'), md5($this->input->post('userpassword')));
			// check result
			if(is_array($is_exist) && count($is_exist) > 0){
				// generate valid token
				$token = md5(time().$is_exist[0]['username']);
				// check if login user is admin
				if($is_exist[0]['is_admin'] == 'yes'){
					// store the information to session
					$newdata = array(
						'username'  => $is_exist[0]['username'],
						'firstname'  => $is_exist[0]['firstname'],
						'lastname'  => $is_exist[0]['lastname'],
						'logged_in' => TRUE,
						'user_group' => $is_exist[0]['user_group_id'],
						'is_admin' => $is_exist[0]['is_admin'],
						'company_id' => $is_exist[0]['company_id'],
						'token' => $token,
						'user_id' => $is_exist[0]['id'],
						'has_role_dashboard' => 'yes',
						'has_role_forms' => 'yes',
						'has_role_settings' => 'yes',
						'has_role_reports' => 'yes',
						'has_role_students' => 'yes',
						'has_role_user' => 'yes',
						'has_role_admin' => 'yes'
					);
				} else {
					// store the information to session
					$newdata = array(
						'username'  => $is_exist[0]['username'],
						'firstname'  => $is_exist[0]['firstname'],
						'lastname'  => $is_exist[0]['lastname'],
						'logged_in' => TRUE,
						'user_group' => $is_exist[0]['user_group_id'],
						'company_id' => $is_exist[0]['company_id'],
						'is_admin' => $is_exist[0]['is_admin'],
						'token' => $token,
						'user_id' => $is_exist[0]['id']
					);

					// get the user modules
					$user_modules = $this->User_model->get_user_modules($is_exist[0]['id']);
					if(is_array($user_modules)){
						foreach($user_modules as $user_module){
							if($user_module['module_name'] == 'Dashboard'){
								$this->session->set_userdata('has_role_dashboard', 'yes');
							}

							if($user_module['module_name'] == 'Income' || $user_module['module_name'] == 'Billing' || $user_module['module_name'] == 'Payment'){
								$this->session->set_userdata('has_role_forms', 'yes');
							}

							if($user_module['module_name'] == 'Project' || $user_module['module_name'] == 'Company' || $user_module['module_name'] == 'Client' || $user_module['module_name'] == 'Income Type' || $user_module['module_name'] == 'Income Category' || $user_module['module_name'] == 'Unit Type'){
								$this->session->set_userdata('has_role_settings', 'yes');
							}

							if($user_module['module_name'] == 'Report'){
								$this->session->set_userdata('has_role_reports', 'yes');
							}

							if($user_module['module_name'] == 'User' || $user_module['module_name'] == 'User Group'){
								$this->session->set_userdata('has_role_user', 'yes');
							}
						}
					}
				}
				$this->session->set_userdata($newdata);

				// store the user logs
				$log = array(
					'user_id' => $this->session->userdata('user_id'),
					'activity' => 'User '.$is_exist[0]['username'].' login',
					'action' => 'login',
					'log_time' => date('Y-m-d H:i:s')
				);
				$this->Activity_log_model->save($log);

				// save the user token
				$is_update = $this->User_model->save_user_token($is_exist[0]['id'], $token);
				if($is_update){
					// redirect to user dashboard
					redirect(site_url('dashboard/view'));
				} else {
					$data['message'] = 'There was an error in saving the user token. Please contact the System Administrator.';
				}

			} else {
				$data['message'] = 'User Account does not exist';
			}
		}

		$this->load->view('admin/login', $data);
	}

	/**
	* validate_login()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_login(){
		$config = array(
			array(
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter your %s correctly.'
				),
			),
			array(
				'field' => 'userpassword',
				'label' => 'User Password',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter your %s correctly.'
				),
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */