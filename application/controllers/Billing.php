<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/billing
	 *	- or -
	 * 		http://example.com/index.php/billing/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/billing/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();

		// load the necessary models and libraries
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Activity_log_model');
		$this->load->model('Access_role_model');
		$this->load->model('Billing_model');
		$this->load->model('Company_model');
		$this->load->model('Client_model');
		$this->load->model('Income_model');
		$this->load->model('Mode_payment_model');
		$this->load->library('pagination');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Billing';
			$data['page_sub_title'] = 'Add Billing';
			$data['module_name'] = 'Billing';
			$data['message'] = $this->session->flashdata('message');
			$data['companies'] = '';
			$data['clients'] = '';
			$data['billed_by'] = '';
			$data['company_id'] = '';
			$data['company_name'] = '';
			$data['files'] = $this->cart->contents();

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// get the last series for the current year
				$series_no = $this->Billing_model->get_last_series(date('Y'));
				if(is_array($series_no)){
					$new_number = $series_no[0]['series_no'] + 1;
				} else {
					$new_number = 1;
				}
				// save the billing
				$billing = array(
					'company_id' => (int) $this->input->post('company_id'),
					'client_id' => (int) $this->input->post('client_id'),
					'billed_by' => (int) $this->input->post('billed_by'),
					'total' => (float) $this->input->post('total'),
					'date_created' => date('Y-m-d'),
					'billing_no' => 'SS',
					'billing_year' => date('Y'),
					'series_no' => $new_number
				);
				$is_save = $this->Billing_model->save($billing);
				if($is_save){
					// check if there are file attachments
					if($this->cart->total_items()){
						foreach($this->cart->contents() as $files){
							// save the files
							$file = array(
								'billing_id' => $is_save,
								'filename' => $files['name']
							);
							$this->Billing_model->save_file($file);
						}

						// clear the cart
						$this->cart->destroy();
					}

					// save the billing details
					$projects = $this->input->post('projects');
					if(is_array($projects)){
						foreach($projects as $project){
							$income = explode('-', $project);
							$billing_detail = array(
								'billing_id' => $is_save,
								'income_id' => $income[1],
								'billed_amount' => $this->input->post('unpaid_amount_'.$income[1]),
								'mode_payment_id' => $this->input->post('mode_payment_'.$income[1])
							);
							$is_save_detail = $this->Billing_model->save_detail($billing_detail);
						}
					}
					$this->session->set_flashdata('message', 'Billing successfully added.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in saving the billing information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// check if login user under a company
			if($this->session->userdata('company_id') == '' || $this->session->userdata('company_id') == 0){
				$companies = $this->Company_model->get_list();
			} else {
				$user_companies = explode('-', $this->session->userdata('company_id'));
				foreach($user_companies as $company_selected){
					$company_info = $this->Company_model->get_info($company_selected);
					if(is_array($company_info)){
						$companies[] = array(
							'id' => $company_selected,
							'company_name' => $company_info[0]['company_name']
						);
					}
				}
			}

			$data['companies'] = $companies;

			// get all the clients
			$clients = $this->Client_model->get_all();
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// get all the users
			$billed_by = $this->User_model->get_users('');
			if(is_array($billed_by)){
				$data['billed_by'] = $billed_by;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('forms/billing/add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* add_file()
	* @access public
	* @param none
	* @return none
	*/
	public function add_file(){
		// store to cart session
		$data = array(
			'id'      => uniqid(),
			'qty'     => 1,
			'price'   => 1,
			'name'    => $this->input->post('filename')
		);
		$this->cart->insert($data);

		// display the files
		$this->display_files();
	}

	/**
	* display_files()
	* @access public
	* @param none
	* @return none
	*/
	public function display_files(){
		// load the view file
		$data['files'] = $this->cart->contents();
		$this->load->view('forms/billing/display_files', $data);
	}

	/**
	* delete_file()
	* @access public
	* @param none
	* @return none
	*/
	public function delete_file(){
		$data = array(
			'rowid' => $this->input->post('rowid'),
			'qty'   => 0
		);

		$this->cart->update($data);

		// delete the file
		unlink('allfiles/files/billing/'.$this->input->post('filename'));

		// display the files
		$this->display_files();
	}

	/**
	* get_client_projects()
	* @access public
	* @param none
	* @return none
	*/
	public function get_client_projects(){
		// variable initialization
		$data['mode_payments'] = '';

		// get the mode of payment
		$mode_payments = $this->Mode_payment_model->get_list();
		if(is_array($mode_payments)){
			$data['mode_payments'] = $mode_payments;
		}

		$projects = $this->Income_model->get_client_projects_for_billing((int) $this->input->post('client_id'), (int) $this->input->post('company_id'));
		if(is_array($projects)){
			$newdata = array(
				'billing_projects' => $projects
			);
			$this->session->set_userdata($newdata);
			$data['projects'] = $projects;
			$this->load->view('forms/billing/income_projects', $data);
		} else {
			echo 'NONE';
		}
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Billing';
			$data['page_sub_title'] = 'List of Billing';
			$data['module_name'] = 'Billing';
			$data['billings'] = '';
			$data['pages'] = '';
			$data['message'] = $this->session->flashdata('message');
			$data['edit_role'] = '';
			$data['delete_role'] = '';
			$data['view_role'] = '';
			$user_company = '';

			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			$has_role_view = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'view');
			if($has_role_view || $this->session->userdata('is_admin') == 'yes'){
				$data['view_role'] = 'yes';
			}

			// get the filters stored in session
			$company_selected = $this->session->userdata('billing_search_company');
			if($this->input->post('company_id')){
				$company_selected = $this->input->post('company_id');
				$this->session->set_userdata('billing_search_company', $this->input->post('company_id'));
			}

			$client_selected = $this->session->userdata('billing_search_client');
			if($this->input->post('client_id')){
				$client_selected = $this->input->post('client_id');
				$this->session->set_userdata('billing_search_client', $this->input->post('client_id'));
			}

			$date_from_selected = $this->session->userdata('billing_search_date_from');
			if($this->input->post('date_from')){
				$date_from_selected = date('Y-m-d', strtotime($this->input->post('date_from')));
				$this->session->set_userdata('billing_search_date_from', date('Y-m-d', strtotime($this->input->post('date_from'))));
			}

			$date_to_selected = $this->session->userdata('billing_search_date_to');
			if($this->input->post('date_to')){
				$date_to_selected = date('Y-m-d', strtotime($this->input->post('date_to')));
				$this->session->set_userdata('billing_search_date_to', strtotime($this->input->post('date_to')));
			}

			if($company_selected){
				$user_company = $company_selected;
			} else {
				if($this->session->userdata('company_id')){
					$user_company = str_replace('-', ',', $this->session->userdata('company_id'));
				}
			}

			$search = array(
				'company_id' => $user_company,
				'client_id' => $client_selected,
				'date_from' => $date_from_selected,
				'date_to' => $date_to_selected
			);

			// for view
			$data['company_id'] = $user_company;
			$data['client_id'] = $client_selected;
			$data['date_from'] = $date_from_selected;
			$data['date_to'] = $date_to_selected;

			// get the total billing records
			$total_billing = $this->Billing_model->get_total($search);

			// setup the pagination
			$offset = 0;
			if($this->uri->segment(3)){
				$offset = $this->uri->segment(3);
			}

			$config['base_url'] = site_url('billing/listing/');
			$config['total_rows'] = $total_billing;
			$config['per_page'] = BILLING_PER_PAGE;
			$config['full_tag_open'] = '<ul class="pagination no-margin pull-right">';
			$config['full_tag_close'] = '</ul>';
			$config['prev_link'] = 'Previous';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li class="next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li class="next">';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li class="next">';
			$config['first_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pages'] =  $this->pagination->create_links();

			// get the list of billing records
			$billings = $this->Billing_model->get_list($offset, $search);
			if(is_array($billings)){
				$data['billings'] = $billings;
			}

			// check if login user under a company
			if($this->session->userdata('company_id') == '' || $this->session->userdata('company_id') == 0){
				$companies = $this->Company_model->get_list();
			} else {
				$user_companies = explode('-', $this->session->userdata('company_id'));
				foreach($user_companies as $company_selected){
					$company_info = $this->Company_model->get_info($company_selected);
					if(is_array($company_info)){
						$companies[] = array(
							'id' => $company_selected,
							'company_name' => $company_info[0]['company_name']
						);
					}
				}
			}

			$data['companies'] = $companies;

			// get all the clients
			$clients = $this->Client_model->get_all();
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('forms/billing/listing');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* clear_search()
	* @access public
	* @param none
	* @return none
	*/
	public function clear_search(){
		// clear the session
		$this->session->set_userdata('billing_search_company', '');
		$this->session->set_userdata('billing_search_client', '');
		$this->session->set_userdata('billing_search_date_from', '');
		$this->session->set_userdata('billing_search_date_to', '');
	}

	/**
	* view()
	* @access public
	* @param int $id
	* @return none
	*/
	public function view($id = ''){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('billing/listing'));

			// variable initialization
			$data['page_title'] = 'Billing';
			$data['page_sub_title'] = 'View Billing';
			$data['module_name'] = 'Billing';
			$data['message'] = $this->session->flashdata('message');
			$data['id'] = (int) $id;
			$data['company_name'] = '';
			$data['client_id'] = '';
			$data['client_name'] = '';
			$data['billed'] = '';
			$data['date_created'] = '';
			$data['total'] = '';
			$data['billing_no'] = '';
			$data['billing_details'] = '';
			$data['files'] = '';
			$data['edit_role'] = '';
			$data['delete_role'] = '';

			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			// get the billing information
			$billing = $this->Billing_model->get_info((int) $id);
			if(is_array($billing)){
				$data['company_name'] = $billing[0]['company_name'];
				$data['client_name'] = $billing[0]['client_name'];
				$data['billed'] = $billing[0]['firstname'].' '.$billing[0]['lastname'];
				$data['date_created'] = $billing[0]['date_created'];
				$data['total'] = $billing[0]['total'];
				$data['billing_no'] = $billing[0]['billing_no'].'-'.$billing[0]['billing_year'].'-'.str_pad($billing[0]['series_no'], 3, '0', STR_PAD_LEFT);

				// get the billing details
				$billing_details = $this->Billing_model->get_details((int) $id);
				if(is_array($billing_details)){
					$data['billing_details'] = $billing_details;
				}

				// get the file attachments
				$files = $this->Billing_model->get_files((int) $id);
				if(is_array($files)){
					$data['files'] = $files;
				}
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('forms/billing/view');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* save_file()
	* @access public
	* @param none
	* @return none
	*/
	public function save_file(){
		$file_info = array(
			'billing_id' => (int) $this->input->post('billing_id'),
			'filename' => (string) $this->input->post('filename')
		);

		$is_save = $this->Billing_model->save_file($file_info);
		if($is_save){
			// display the files
			$this->display_file((int) $this->input->post('billing_id'));
		}
	}

	/**
	* display_file()
	* @access private
	* @param none
	* @return none
	*/
	private function display_file($billing_id){
		if($billing_id){
			// variable initialization
			$data['files'] = '';

			// get the files
			$files = $this->Billing_model->get_files($billing_id);
			if(is_array($files)){
				$data['files'] = $files;
			}

			// display the view file
			$this->load->view('forms/billing/files', $data);
		}
	}

	/**
	* edit()
	* @access public
	* @param int $id
	* @return none
	*/
	public function edit($id = ''){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('billing/listing'));

			// variable initialization
			$data['page_title'] = 'Billing';
			$data['page_sub_title'] = 'Edit Billing';
			$data['module_name'] = 'Billing';
			$data['message'] = $this->session->flashdata('message');
			$data['id'] = (int) $id;
			$data['company_id'] = '';
			$data['client_name'] = '';
			$data['billed_id'] = '';
			$data['date_created'] = '';
			$data['total'] = '';
			$data['billing_details'] = '';
			$data['projects'] = '';
			$data['company_id'] = '';
			$data['company_name'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the billing
				$billing = array(
					'company_id' => (int) $this->input->post('company_id'),
					'billed_by' => (int) $this->input->post('billed_by'),
					'total' => (float) $this->input->post('total'),
					'id' => (int) $id
				);
				$is_save = $this->Billing_model->update($billing);

				// update the billing details
				$projects = $this->input->post('projects');
				if(is_array($projects)){
					foreach($projects as $project){
						$income = explode('-', $project);
						$billing_detail = array(
							'billing_id' => (int) $id,
							'income_id' => $income[1],
							'billed_amount' => $this->input->post('unpaid_amount_'.$income[1])
						);
						$is_save_detail = $this->Billing_model->update_detail($billing_detail);
					}
				}

				if($is_save || is_save_detail){
					$this->session->set_flashdata('message', 'Billing successfully updated.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in saving the billing information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the billing information
			$billing = $this->Billing_model->get_info((int) $id);
			if(is_array($billing)){
				$data['company_id'] = $billing[0]['company_id'];
				$data['company_name'] = $billing[0]['company_name'];
				$data['client_name'] = $billing[0]['client_name'];
				$data['client_id'] = $billing[0]['client_id'];
				$data['billed'] = $billing[0]['billed_by'];
				$data['date_created'] = date('m/d/Y',strtotime($billing[0]['date_created']));
				$data['total'] = $billing[0]['total'];

				// get the billing details
				$billing_details = $this->Billing_model->get_details((int) $id);
				if(is_array($billing_details)){
					$data['billing_details'] = $billing_details;
				}

				// get the income projects from the client
				$projects = $this->Income_model->get_client_projects_for_billing((int) $billing[0]['client_id']);
				if(is_array($projects)){
					$data['projects'] = $projects;
				}
			}

			// check if login user under a company
			if($this->session->userdata('company_id')){
				$data['company_id'] = $this->session->userdata('company_id');
				// get the company information
				$company = $this->Company_model->get_info((int) $this->session->userdata('company_id'));
				if(is_array($company)){
					$data['company_name'] = $company[0]['company_name'];
				}
			} else {
				// get all the companies
				$companies = $this->Company_model->get_list();
				if(is_array($companies)){
					$data['companies'] = $companies;
				}
			}

			// get all the users
			$billed_by = $this->User_model->get_users('');
			if(is_array($billed_by)){
				$data['billed_by'] = $billed_by;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('forms/billing/edit');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* delete()
	* @access public
	* @param none
	* @return none
	*/
	public function delete(){
		// delete the details first
		$billing_details = $this->Billing_model->get_details((int) $this->input->post('billing_id'));
		if(is_array($billing_details)){
			foreach($billing_details as $billing_detail){
				// change the status of the income to unbilled
				$income_status = array(
					'billed_status' => 'unbilled',
					'id' => $billing_detail['income_id']
				);
				$is_update = $this->Income_model->update_billed_status($income_status);
			}

			if($is_update){
				// delete the billing details
				$this->Billing_model->delete_billing_detail((int) (int) $this->input->post('billing_id'));
				// delete the billing
				$is_delete = $this->Billing_model->delete((int) (int) $this->input->post('billing_id'));
			}
		}
		if($is_delete){
			$this->session->set_flashdata('message', 'Income information successfully deleted.');
			echo 'SUCCESS';
		} else {
			$this->session->set_flashdata('message', 'There was an error in deleting the income. Please contact the Administrator.');
			echo 'ERROR';
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'company_id',
				'label' => 'Company Name',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'client_id',
				'label' => 'Client Name',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'billed_by',
				'label' => 'Billied By',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'total',
				'label' => 'Projects',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}

}
