<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/module
	 *	- or -  
	 * 		http://example.com/index.php/module/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/module/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();

		// load the form validation
		$this->load->library('form_validation');
		// load the user model
		$this->load->model('User_model');
		// load the activity log model
		$this->load->model('Activity_log_model');
		// load the module model
		$this->load->model('Module_model');
		// load the role model
		$this->load->model('Role_model');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $this->session->userdata('is_admin') == 'yes' && $is_valid_token == TRUE){

			// variable initialization
			$data['page_title'] = 'Module';
			$data['page_sub_title'] = 'Add Module';
			$data['module_name'] = 'Module';
			$data['message'] = $this->session->flashdata('message');
			$data['roles'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// check if there are roles selected
				if($this->input->post('role')){
					// save the module
					$module = array(
						'module_name' => (string) $this->input->post('module_name'),
						'alias_name' => (string) $this->input->post('alias_name')
					);
					$module_id = $this->Module_model->save($module);
					if($module_id){
						$module_roles = $this->input->post('role');
						foreach($module_roles as $module_role){
							$save_module_role = array(
								'module_id' => $module_id,
								'role_id' => $module_role
							);
							$this->Module_model->save_role($save_module_role);
						}
						// store the user logs
						$log = array(
							'user_id' => $this->session->userdata('user_id'),
							'activity' => 'User '.$this->session->userdata('username').' create module name and added roles for '.$this->input->post('module_name'),
							'action' => 'module',
							'log_time' => date('Y-m-d H:i:s')
						);
						$this->Activity_log_model->save($log);
						$this->session->set_flashdata('message', 'Module name successfully saved.');
					} else {
						$this->session->set_flashdata('message', 'There was an error found in saving the module name.');
					}

					// redirect to current page
					redirect(current_url());
				} else {
					$data['message'] = 'Please select a role(s) for the module.';
				}
			}

			// get the roles
			$roles = $this->Role_model->get_roles();
			if(is_array($roles)){
				$data['roles'] = $roles;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('module/add');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $this->session->userdata('is_admin') == 'yes' && $is_valid_token == TRUE){

			// variable initialization
			$data['page_title'] = 'Module';
			$data['page_sub_title'] = 'List of Modules';
			$data['module_name'] = 'Module';
			$data['modules'] = '';

			// get the modules
			$modules = $this->Module_model->get_modules();
			if(is_array($modules)){
				$data['modules'] = $modules;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('module/listing');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* view()
	* @access public
	* @param none
	* @return none
	*/
	public function view($id = ''){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $this->session->userdata('is_admin') == 'yes' && $is_valid_token == TRUE){

			if($id == '')
				redirect(site_url('module/listing'));

			// variable initialization
			$data['page_title'] = 'Module';
			$data['page_sub_title'] = 'View Module';
			$data['module_name'] = 'Module';
			$data['module'] = '';
			$data['module_roles'] = '';
			$data['module_id'] = $id;
			$data['roles'] = '';
			$not_in = '';

			// get the module infotmation
			$module = $this->Module_model->get_info((int) $id);
			if(is_array($module)){
				$data['module'] = $module[0]['module_name'];

				// get the module roles
				$module_roles = $this->Module_model->get_module_roles((int) $id);
				if(is_array($module_roles)){
					$data['module_roles'] = $module_roles;
					$total_module_roles = count($module_roles);
					$cnt = 1;
					foreach($module_roles as $module_role){
						$not_in .= $module_role['role_id'];
						if($cnt < $total_module_roles){
							$not_in .= ',';
						}
						$cnt++;
					}
				}

				// get the roles unassigned
				$roles = $this->Role_model->get_roles_unassigned($not_in);
				if(is_array($roles)){
					$data['roles'] = $roles;
				}
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('module/view');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* remove_role()
	* @access public
	* @param none
	* @return none
	*/
	public function remove_role(){
		$is_remove = $this->Module_model->remove_role((int) $this->input->post('module_id'), (int) $this->input->post('role_id'));
		if($is_remove){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* add_role()
	* @access public
	* @param none
	* @return none
	*/
	public function add_role(){
		$module_role = array(
			'module_id' => (int) $this->input->post('module_id'),
			'role_id' => (int) $this->input->post('role_id')
		);
		$is_add = $this->Module_model->save_role($module_role);
		if($is_add){
			echo 'SUCCESS';
		} else {
			echo  'ERROR';
		}
	}

	/**
	* display_module_roles()
	* @access public
	* @param int $module_id
	* @return none
	*/
	public function display_module_roles($module_id){
		// variable initialization
		$data['module_roles'] = '';

		// get the module roles
		$module_roles = $this->Module_model->get_module_roles($module_id);
		if(is_array($module_roles)){
			$data['module_roles'] = $module_roles;
		}

		// load the view file
		$this->load->view('module/display_module_roles', $data);
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'module_name', 
				'label' => 'Module Name', 
				'rules' => 'required|trim|is_unique[modules.module_name]',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.',
					'is_unique' => 'Sorry. '.$this->input->post('module_name').' already exist. Please type the %s again.'
				)
			),
			array(
				'field' => 'alias_name', 
				'label' => 'Alias Name', 
				'rules' => 'required|trim|is_unique[modules.alias_name]',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.',
					'is_unique' => 'Sorry. '.$this->input->post('alias_name').' already exist. Please type the %s again.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file module.php */
/* Location: ./application/controllers/module.php */