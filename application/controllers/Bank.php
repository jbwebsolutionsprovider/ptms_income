<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bank extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/bank
	 *	- or -  
	 * 		http://example.com/index.php/bank/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/bank/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		parent::__construct();

		// load the necessary models and libraries
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Activity_log_model');
		$this->load->model('Access_role_model');
		$this->load->model('Bank_model');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Bank';
			$data['page_sub_title'] = 'Add Bank';
			$data['module_name'] = 'Bank';
			$data['message'] = $this->session->flashdata('message');

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the bank
				$bank = array(
					'bank_name' => (string) $this->input->post('bank_name'),
					'branch' => (string) $this->input->post('branch')
				);
				$is_save = $this->Bank_model->save($bank);
				if($is_save){
					$this->session->set_flashdata('message', 'Bank successfully added.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in saving the bank information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/bank/add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Bank';
			$data['page_sub_title'] = 'List of Bank';
			$data['module_name'] = 'Bank';
			$data['banks'] = '';
			$data['edit_role'] = '';
			$data['delete_role'] = '';

			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			// get the list of banks
			$banks = $this->Bank_model->get_list();
			if(is_array($banks)){
				$data['banks'] = $banks;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/bank/listing');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* edit()
	* @access public
	* @param int $id
	* @return none
	*/
	public function edit($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('bank/listing'));

			// variable initialization
			$data['page_title'] = 'Bank';
			$data['page_sub_title'] = 'Edit Bank';
			$data['module_name'] = 'Bank';
			$data['message'] = $this->session->flashdata('message');
			$data['bank_name'] = '';
			$data['branch'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the bank
				$bank = array(
					'bank_name' => (string) $this->input->post('bank_name'),
					'branch' => (string) $this->input->post('branch'),
					'id' => (int) $id
				);
				$is_save = $this->Bank_model->update($bank);
				if($is_save){
					$this->session->set_flashdata('message', 'Branch successfully updated.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in updating the branch information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the bank information
			$bank = $this->Bank_model->get_info((int) $id);
			if(is_array($bank)){
				$data['bank_name'] = $bank[0]['bank_name'];
				$data['branch'] = $bank[0]['branch'];
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/bank/edit');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* delete()
	* @access public
	* @param none
	* @return none
	*/
	public function delete(){
		$is_delete = $this->Bank_model->delete((int) $this->input->post('bank_id'));
		if($is_delete){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'bank_name', 
				'label' => 'Bank Name', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'branch', 
				'label' => 'Branch', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file bank.php */
/* Location: ./application/controllers/bank.php */