<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/report
	 *	- or -
	 * 		http://example.com/index.php/report/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/report/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();

		// load the necessary models and libraries
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Activity_log_model');
		$this->load->model('Access_role_model');
		$this->load->model('Client_model');
		$this->load->model('Report_model');
		$this->load->model('Company_model');
		$this->load->model('Transport_model');
		$this->load->model('Place_model');
	}

	/**
	* summary()
	* @access public
	* @param none
	* @return none
	*/
	public function summary(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Report Summary';
			$data['page_sub_title'] = 'Report Summary';
			$data['module_name'] = 'Report Summary';
			$data['message'] = $this->session->flashdata('message');
			$data['summary'] = '';

			// run the form validation
			$this->validate_summary();
			if($this->form_validation->run() == TRUE){
				// get all the clients
				$clients = $this->Client_model->get_all();
				if(is_array($clients)){
					foreach($clients as $client){
						// get the total client projects and summary of income, paid and unpaid
						$summary_search = array(
							'date_from' => date('Y-m-d', strtotime($this->input->post('date_from'))),
							'date_to' => date('Y-m-d', strtotime($this->input->post('date_to'))),
							'client_id' => $client['id']
						);
						$income = $this->Report_model->get_summary($summary_search);
						if(is_array($income)){
							$total_projects = 0;
							$total_contract_price = 0;
							$total_paid_amount = 0;
							$total_unpaid_amount = 0;
							foreach($income as $report){
								$total_contract_price = $total_contract_price + $report['contract_price'];
								$total_paid_amount = $total_paid_amount + $report['paid_amount'];
								$total_unpaid_amount = $total_unpaid_amount + $report['unpaid_amount'];
								$total_projects++;
							}
							$summary[] = array(
								'client_name' => $client['client_name'],
								'total_projects' => $total_projects,
								'total_contract_price' => $total_contract_price,
								'total_paid_amount' => $total_paid_amount,
								'total_unpaid_amount' => $total_unpaid_amount
							);
						}
					}
					$data['summary'] = $summary;
				}
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('report/summary');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* company_summary()
	* @access public
	* @param none
	* @return none
	*/
	public function company_summary(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Report Company Summary';
			$data['page_sub_title'] = 'Report Company Summary';
			$data['module_name'] = 'Report Company Summary';
			$data['message'] = $this->session->flashdata('message');
			$data['company_summary'] = '';
			$data['companies'] = '';

			// run the form validation
			$this->validate_company_summary();
			if($this->form_validation->run() == TRUE){
				$company_summary = $this->Report_model->get_company_summary((int) $this->input->post('company_id'));
				if(is_array($company_summary)){
					$data['company_summary'] = $company_summary;
				}
			}

			// get all the companies
			$companies = $this->Company_model->get_list();
			if(is_array($companies)){
				$data['companies'] = $companies;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('report/company_summary');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* transport_summary()
	* @access public
	* @param none
	* @return none
	*/
	public function transport_summary(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Report Transport Summary';
			$data['page_sub_title'] = 'Report Transport Summary';
			$data['module_name'] = 'Report Transport Summary';
			$data['message'] = $this->session->flashdata('message');
			$data['transport_summary'] = '';
			$data['transport'] = '';

			$date_from = '';
			$date_to = '';

			if($this->input->post('date_from')){
				$date_from = date('Y-m-d', strtotime($this->input->post('date_from')));
			}

			if($this->input->post('date_to')){
				$date_to = date('Y-m-d', strtotime($this->input->post('date_to')));
			}

			// run the form validation
			$this->validate_transport_summary();
			if($this->form_validation->run() == TRUE){
				$summary_search = array(
					'transport' => (int) $this->input->post('transport'),
					'date_from' => $date_from,
					'date_to' => $date_to
				);
				$transport_summary = $this->Report_model->get_transport_summary($summary_search);
				if(is_array($transport_summary)){
					$data['transport_summary'] = $transport_summary;
				}
			}

			// get the transport
			$transport = $this->Transport_model->get_list();
			if(is_array($transport)){
				$data['transport'] = $transport;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('report/transport_summary');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* transport_summary_monthly()
	* @access public
	* @param none
	* @return none
	*/
	public function transport_summary_monthly(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Report Transport Summary Monthly';
			$data['page_sub_title'] = 'Report Transport Summary Monthly';
			$data['module_name'] = 'Report Transport Summary Monthly';
			$data['message'] = $this->session->flashdata('message');
			$data['transport_summary_monthly'] = '';

			// run the form validation
			$this->validate_transport_summary_monthly();
			if($this->form_validation->run() == TRUE){
				$diff = abs(strtotime($this->input->post('date_to')) - strtotime($this->input->post('date_from')));
				$years = floor($diff / (365*60*60*24));
				$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
				$days = floor($diff/(60*60*24));
				if($days > 0){
					for($dates = 0; $dates <= $days; $dates++){
						$date[] = strtotime(date("Y-m-d", strtotime($this->input->post('date_from'))) . " +".$dates." day");
					}
					foreach($date as $date_done){
						$summary = array(
							'date_done' => date('Y-m-d', $date_done),
							'transport' => (int) $this->input->post('transport')
						);
						$transport_summaries = $this->Report_model->get_transport_summary_monthly($summary);
						if(is_array($transport_summaries)){
							foreach($transport_summaries as $transport_summary){
								// get the place from
								$place_from_name = '';
								$place_from = $this->Place_model->get_info($transport_summary['place_from']);
								if(is_array($place_from)){
									$place_from_name = $place_from[0]['place_name'];
								}

								$place_to_name = '';
								$place_to = $this->Place_model->get_info($transport_summary['place_to']);
								if(is_array($place_to)){
									$place_to_name = $place_to[0]['place_name'];
								}
								$transport_summary_monthly[] = array(
									'date' => date('F d, Y', $date_done),
									'quantity' => $transport_summary['quantity'],
									'unit_type_name' => $transport_summary['unit_type_name'],
									'place_from' => $place_from_name,
									'place_to' => $place_to_name
								);
							}
						} else {
							$transport_summary_monthly[] = array(
								'date' => date('F d, Y', $date_done),
								'quantity' => 0,
								'unit_type_name' => '',
								'place_from' => '',
								'place_to' => ''
							);
						}
					}
				}
				$data['transport_summary_monthly'] = $transport_summary_monthly;
			}

			// get the transport
			$transport = $this->Transport_model->get_list();
			if(is_array($transport)){
				$data['transport'] = $transport;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('report/transport_summary_monthly');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* validate_summary()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_summary(){
		$config = array(
			array(
				'field' => 'date_from', 
				'label' => 'Date From', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'date_to', 
				'label' => 'Date To', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}

	/**
	* validate_company_summary()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_company_summary(){
		$config = array(
			array(
				'field' => 'company_id', 
				'label' => 'Company', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}

	/**
	* validate_transport_summary()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_transport_summary(){
		$config = array(
			array(
				'field' => 'date_from', 
				'label' => 'Date From', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'date_to', 
				'label' => 'Date To', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}

	/**
	* validate_transport_summary_monthly()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_transport_summary_monthly(){
		$config = array(
			array(
				'field' => 'date_from', 
				'label' => 'Date From', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'date_to', 
				'label' => 'Date To', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}

}
