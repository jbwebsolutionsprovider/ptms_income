<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Place extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/place
	 *	- or -  
	 * 		http://example.com/index.php/place/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/place/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		parent::__construct();

		// load the necessary models and libraries
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Activity_log_model');
		$this->load->model('Access_role_model');
		$this->load->model('Place_model');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Place';
			$data['page_sub_title'] = 'Add Place';
			$data['module_name'] = 'Place';
			$data['message'] = $this->session->flashdata('message');

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the place
				$place = array(
					'place_name' => (string) $this->input->post('place_name'),
					'description' => (string) $this->input->post('description')
				);
				$is_save = $this->Place_model->save($place);
				if($is_save){
					$this->session->set_flashdata('message', 'Place successfully added.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in saving the place information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/place/add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Place';
			$data['page_sub_title'] = 'List of Place';
			$data['module_name'] = 'Place';
			$data['places'] = '';
			$data['edit_role'] = '';
			$data['delete_role'] = '';

			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			// get the list of places
			$places = $this->Place_model->get_list();
			if(is_array($places)){
				$data['places'] = $places;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/place/listing');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* edit()
	* @access public
	* @param int $id
	* @return none
	*/
	public function edit($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('place/listing'));

			// variable initialization
			$data['page_title'] = 'Place';
			$data['page_sub_title'] = 'Edit Place';
			$data['module_name'] = 'Place';
			$data['message'] = $this->session->flashdata('message');
			$data['place_name'] = '';
			$data['description'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the place
				$place = array(
					'place_name' => (string) $this->input->post('place_name'),
					'description' => (string) $this->input->post('description'),
					'id' => (int) $id
				);
				$is_save = $this->Place_model->update($place);
				if($is_save){
					$this->session->set_flashdata('message', 'Place successfully updated.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in updating the place information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the place information
			$place = $this->Place_model->get_info((int) $id);
			if(is_array($place)){
				$data['place_name'] = $place[0]['place_name'];
				$data['description'] = $place[0]['description'];
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/place/edit');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* delete()
	* @access public
	* @param none
	* @return none
	*/
	public function delete(){
		$is_delete = $this->Place_model->delete((int) $this->input->post('place_id'));
		if($is_delete){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'place_name', 
				'label' => 'Place Name', 
				'rules' => 'required|trim|is_unique[place.place_name]',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.',
					'is_unique' => 'Sorry. '.$this->input->post('place_name').' already exist. Please type the %s again.'
				)
			),
			array(
				'field' => 'description', 
				'label' => 'Place Description', 
				'rules' => 'trim'
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file place.php */
/* Location: ./application/controllers/place.php */