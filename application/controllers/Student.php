<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/student
	 *	- or -  
	 * 		http://example.com/index.php/student/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/student/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		parent::__construct();

		// load the necessary models and libraries
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Activity_log_model');
		$this->load->model('Access_role_model');
		$this->load->model('Project_model');
		$this->load->model('Student_model');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// checked if $id has data
			if($id == '')
				redirect(site_url('project/listing'));

			// variable initialization
			$data['page_title'] = 'Project';
			$data['page_sub_title'] = 'Add Student';
			$data['module_name'] = 'Project';
			$data['message'] = $this->session->flashdata('message');
			$data['project_name'] = '';
			$data['id'] = (int) $id;

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the student
				$student = array(
					'project_id' => (int) $id,
					'firstname' => (string) $this->input->post('firstname'),
					'lastname' => (string) $this->input->post('lastname')
				);
				$is_save = $this->Project_model->save_students($student);
				if($is_save){
					$this->session->set_flashdata('message', 'Student successfully added.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in saving the student information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the project information
			$project = $this->Project_model->get_info((int) $id);
			if(is_array($project)){
				$data['project_name'] = $project[0]['project_name'];
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/student/add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Students';
			$data['page_sub_title'] = 'List of Students';
			$data['module_name'] = 'Students';
			$data['students'] = '';

			// get the list of students
			$search = array(
				'firstname' => (string) $this->input->post('firstname'),
				'lastname' => (string) $this->input->post('lastname'),
				'hauler_company' => (string) $this->input->post('hauler_company')
			);
			$students = $this->Student_model->get_list($search);
			if(is_array($students)){
				$data['students'] = $students;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/student/listing');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* edit()
	* @access public
	* @param int $id
	* @return none
	*/
	public function edit($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('unit_type/listing'));

			// variable initialization
			$data['page_title'] = 'Unit Type';
			$data['page_sub_title'] = 'Edit Unit Type';
			$data['module_name'] = 'Unit Type';
			$data['message'] = $this->session->flashdata('message');
			$data['unit_type_name'] = '';
			$data['description'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the unit type
				$unit_type = array(
					'unit_type_name' => (string) $this->input->post('unit_type_name'),
					'description' => (string) $this->input->post('description'),
					'id' => (int) $id
				);
				$is_save = $this->Unit_type_model->update($unit_type);
				if($is_save){
					$this->session->set_flashdata('message', 'Unit Type successfully updated.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in updating the unit type information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the unit type information
			$unit_type = $this->Unit_type_model->get_info((int) $id);
			if(is_array($unit_type)){
				$data['unit_type_name'] = $unit_type[0]['unit_type_name'];
				$data['description'] = $unit_type[0]['description'];
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/unit_type/edit');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* delete()
	* @access public
	* @param none
	* @return none
	*/
	public function delete(){
		$is_delete = $this->Unit_type_model->delete((int) $this->input->post('unit_type_id'));
		if($is_delete){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'firstname', 
				'label' => 'First Name', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'lastname', 
				'label' => 'Last Name', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file student.php */
/* Location: ./application/controllers/student.php */