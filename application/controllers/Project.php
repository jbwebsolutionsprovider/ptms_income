<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/project
	 *	- or -  
	 * 		http://example.com/index.php/project/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/project/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		parent::__construct();

		// load the necessary models and libraries
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Activity_log_model');
		$this->load->model('Access_role_model');
		$this->load->model('Project_model');
		$this->load->model('Client_model');
		$this->load->library('pagination');
		$this->load->library('Services_JSON');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Project';
			$data['page_sub_title'] = 'Add Project';
			$data['module_name'] = 'Project';
			$data['message'] = $this->session->flashdata('message');
			$data['clients'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				$transaction_date = date('Y-m-d');
				if($this->input->post('transaction_date')){
					$transaction_date = date('Y-m-d', strtotime($this->input->post('transaction_date')));
				}
				// save the project
				$project = array(
					'project_name' => (string) $this->input->post('project_name'),
					'client_id' => (int) $this->input->post('client_id'),
					'project_code' => (string) $this->input->post('project_code'),
					'project_status' => (string) $this->input->post('project_status'),
					'project_type' => (string) $this->input->post('project_type'),
					'transaction_date' => $transaction_date,
					'remarks' => (string) $this->input->post('remarks')
				);
				$is_save = $this->Project_model->save($project);
				if($is_save){
					// check if has items
					if($this->cart->total_items() > 0){
						$students = $this->cart->contents();

						// destroy the cart content
						$this->cart->destroy();

						foreach($students as $student){
							if($student['options']['lastname'] && $student['options']['firstname']){
								$student_info = array(
									'firstname' => $student['options']['firstname'],
									'lastname' => $student['options']['lastname'],
									'hauler_company' => $student['options']['hauler_company'],
									'project_id' => $is_save
								);
								$this->Project_model->save_students($student_info);
							}
						}
					}
					$this->session->set_flashdata('message', 'Project successfully added.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in saving the project information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get all the clients
			$clients = $this->Client_model->get_all();
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/project/add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* add_student()
	* @access public
	* @param none
	* @return none
	*/
	public function add_student(){
		$student_data = array(
			'id'      => uniqid(),
			'qty'     => 1,
			'price'   => 200,
			'name'    => uniqid(),
			'options' => array (
				'firstname' => (string) $this->input->post('firstname'),
				'lastname' => (string) $this->input->post('lastname'),
				'hauler_company' => (string) $this->input->post('hauler_company')
			)
		);
		$this->cart->insert($student_data);

		// display the students list
		$this->display_students();
	}

	/**
	* remove_student()
	* @access public
	* @param none
	* @return none
	*/
	public function remove_student(){
		$data = array(
			'rowid' => (string) $this->input->post('rowid'),
			'qty'   => 0
		);
		$this->cart->update($data);

		// display the students list
		$this->display_students();
	}

	/**
	* display_students()
	* @access public
	* @param none
	* @return none
	*/
	public function display_students(){
		$this->load->view('settings/project/students');
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Project';
			$data['page_sub_title'] = 'List of Project';
			$data['module_name'] = 'Project';
			$data['projects'] = '';
			$data['pages'] = '';
			$data['message'] = $this->session->flashdata('message');
			$data['edit_role'] = '';
			$data['delete_role'] = '';
			$data['view_role'] = '';

			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			$has_role_view = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'view');
			if($has_role_view || $this->session->userdata('is_admin') == 'yes'){
				$data['view_role'] = 'yes';
			}

			// get the total projects
			$total_projects = $this->Project_model->get_total();

			// setup the pagination
			$offset = 0;
			if($this->uri->segment(3)){
				$offset = $this->uri->segment(3);
			}

			$config['base_url'] = site_url('project/listing/');
			$config['total_rows'] = $total_projects;
			$config['per_page'] = PROJECT_PER_PAGE; 
			$config['full_tag_open'] = '<ul class="pagination no-margin pull-right">';
			$config['full_tag_close'] = '</ul>';
			$config['prev_link'] = 'Previous';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li class="next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li class="next">';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li class="next">';
			$config['first_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pages'] =  $this->pagination->create_links();

			// get the list of projects
			$projects = $this->Project_model->get_list($offset);
			if(is_array($projects)){
				$data['projects'] = $projects;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/project/listing');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* view()
	* @access public
	* @param int $id
	* @return none
	*/
	public function view($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('project/listing'));

			// variable initialization
			$data['page_title'] = 'Project';
			$data['page_sub_title'] = 'View Project';
			$data['module_name'] = 'Project';
			$data['message'] = $this->session->flashdata('message');
			$data['id'] = (int) $id;
			$data['project_name'] = '';
			$data['client_name'] = '';
			$data['project_code'] = '';
			$data['project_status'] = '';
			$data['project_type'] = '';
			$data['transaction_date'] = '';
			$data['remarks'] = '';
			$data['students'] = '';
			$data['edit_role'] = '';
			$data['delete_role'] = '';
			$data['closed_role'] = '';
			$data['add_student_role'] = '';

			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			$has_role_closed = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'closed');
			if($has_role_closed || $this->session->userdata('is_admin') == 'yes'){
				$data['closed_role'] = 'yes';
			}

			$has_role_add_student = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), 'student', 'add');
			if($has_role_closed || $this->session->userdata('is_admin') == 'yes'){
				$data['add_student_role'] = 'yes';
			}

			// get the project information
			$project = $this->Project_model->get_info((int) $id);
			if(is_array($project)){
				$data['project_name'] = $project[0]['project_name'];
				$data['client_name'] = $project[0]['client_name'];
				$data['project_code'] = $project[0]['project_code'];
				$data['project_status'] = $project[0]['project_status'];
				$data['project_type'] = $project[0]['project_type'];
				$data['transaction_date'] = $project[0]['transaction_date'];
				$data['remarks'] = $project[0]['remarks'];

				if($project[0]['client_id'] == 1){
					// get the students
					$students = $this->Project_model->get_students((int) $id);
					if(is_array($students)){
						$data['students'] = $students;
					}
				}
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/project/view');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* edit()
	* @access public
	* @param int $id
	* @return none
	*/
	public function edit($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('project/listing'));

			// variable initialization
			$data['page_title'] = 'Project';
			$data['page_sub_title'] = 'Edit Project';
			$data['module_name'] = 'Project';
			$data['message'] = $this->session->flashdata('message');
			$data['id'] = (int) $id;
			$data['project_name'] = '';
			$data['client_id'] = '';
			$data['project_code'] = '';
			$data['project_status'] = '';
			$data['project_type'] = '';
			$data['transaction_date'] = '';
			$data['remarks'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the project
				$project = array(
					'project_name' => (string) $this->input->post('project_name'),
					'client_id' => (int) $this->input->post('client_id'),
					'project_code' => (string) $this->input->post('project_code'),
					'project_status' => (string) $this->input->post('project_status'),
					'project_type' => (string) $this->input->post('project_type'),
					'transaction_date' => date('Y-m-d', strtotime($this->input->post('transaction_date'))),
					'remarks' => (string) $this->input->post('remarks'),
					'id' => (int) $id
				);
				$is_save = $this->Project_model->update($project);
				if($is_save){
					$this->session->set_flashdata('message', 'Project successfully updated.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in updating the project information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the project information
			$project = $this->Project_model->get_info((int) $id);
			if(is_array($project)){
				$data['project_name'] = $project[0]['project_name'];
				$data['client_id'] = $project[0]['client_id'];
				$data['project_code'] = $project[0]['project_code'];
				$data['project_status'] = $project[0]['project_status'];
				$data['project_type'] = $project[0]['project_type'];
				$data['transaction_date'] = $project[0]['transaction_date'];
				$data['remarks'] = $project[0]['remarks'];
			}

			// get all the clients
			$clients = $this->Client_model->get_all();
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/project/edit');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* delete()
	* @access public
	* @param none
	* @return none
	*/
	public function delete(){
		// before deleting the project record, check first if client has exiting transactions
		$is_delete = $this->Project_model->delete((int) $this->input->post('project_id'));
		if($is_delete){
			$this->session->set_flashdata('message', 'Project information successfully deleted.');
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* closed()
	* @access public
	* @param none
	* @return none
	*/
	public function closed(){
		$closed = array(
			'project_status' => 'closed',
			'id' => (int) $this->input->post('project_id')
		);
		$is_update = $this->Project_model->update_status($closed);
		if($is_update){
			$this->session->set_flashdata('message', 'Project information successfully changed status to closed.');
			echo 'SUCCESS';
		} else {
			$this->session->set_flashdata('message', 'There was an error in updating the project status. Please contact the Administrator.');
			echo 'ERROR';
		}
	}

	/**
	* get_client_projects()
	* @access public
	* @param none
	* @return none
	*/
	public function get_client_projects(){
		$projects = $this->Project_model->get_client_projects((int) $this->input->post('client_id'));
		if(is_array($projects)){
			echo $this->services_json->encode($projects);
		} else {
			echo 'NONE';
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'project_name', 
				'label' => 'Project Name', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'client_id', 
				'label' => 'Client Name', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'project_code', 
				'label' => 'Project Code', 
				'rules' => 'trim'
			),
			array(
				'field' => 'project_status', 
				'label' => 'Project Status', 
				'rules' => 'trim'
			),
			array(
				'field' => 'project_type', 
				'label' => 'Project Type', 
				'rules' => 'trim'
			),
			array(
				'field' => 'transaction_date', 
				'label' => 'Transaction Date', 
				'rules' => 'trim'
			),
			array(
				'field' => 'remarks', 
				'label' => 'Remarks', 
				'rules' => 'trim'
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file position.php */
/* Location: ./application/controllers/position.php */