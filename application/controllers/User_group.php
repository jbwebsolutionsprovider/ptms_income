<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_group extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/user_group
	 *	- or -  
	 * 		http://example.com/index.php/user_group/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/user_group/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		parent::__construct();

		// load the form validation
		$this->load->library('form_validation');
		// load the user model
		$this->load->model('User_model');
		// load the activity log model
		$this->load->model('Activity_log_model');
		// load the module model
		$this->load->model('Module_model');
		// load the user group model
		$this->load->model('User_group_model');
		// load the module model
		$this->load->model('Module_model');
		// load the access role model
		$this->load->model('Access_role_model');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'User Group';
			$data['page_sub_title'] = 'Add User Group';
			$data['module_name'] = 'User Group';
			$data['message'] = '';
			$data['modules'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// check if there are roles selected
				if($this->input->post('role')){
					// save the user group information
					$user_group = array(
						'user_group_name' => $this->input->post('user_group_name')
					);
					$user_group_id = $this->User_group_model->save($user_group);
					if($user_group_id){
						// save the user group roles
						$roles = $this->input->post('role');
						foreach($roles as $role){
							$user_group_role = array(
								'user_group_id' => $user_group_id,
								'module_roles_id' => $role
							);
							$is_save = $this->User_group_model->save_user_group_role($user_group_role);
						}
						$data['message'] = 'User group and roles successfully saved.';
					} else {
						$data['message'] = 'Error in saving the user group roles.';
					}
				} else {
					$data['message'] = 'Please select a role(s) for the user group.';
				}
			}

			// get all modules
			$modules = $this->Module_model->get_modules();
			$cnt = 0;
			if(is_array($modules)){
				// get the roles per module
				foreach($modules as $module){
					$roles = $this->Module_model->get_module_roles($module['id']);
					$modules[$cnt]['roles'] = $roles;
					$cnt++;
				}
				$data['modules'] = $modules;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('user_group/add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'User Group';
			$data['page_sub_title'] = 'List of User Groups';
			$data['module_name'] = 'User Group';
			$data['message'] = '';
			$data['user_groups'] = '';
			$data['view_role'] = '';

			// view role
			$has_role_view = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'view');
			if($has_role_view || $this->session->userdata('is_admin') == 'yes'){
				$data['view_role'] = 'yes';
			}

			// get the list of user groups
			$user_groups = $this->User_group_model->get_list();
			if(is_array($user_groups)){
				$data['user_groups'] = $user_groups;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('user_group/listing');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* view()
	* @access public
	* @param none
	* @return none
	*/
	public function view($id = ''){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){

			// check if there is ID passed
			if($id == '')
				redirect(site_url('user_group/listing'));

			// variable initialization
			$data['page_title'] = 'User Group';
			$data['page_sub_title'] = 'View User Group';
			$data['module_name'] = 'User Group';
			$data['message'] = '';
			$data['roles'] = '';
			$data['user_group_name'] = '';
			$data['user_group_roles'] = '';
			$data['user_group_id'] = $id;
			$not_in = '';

			// get the user group information and role
			$user_group_info = $this->User_group_model->get_info((int) $id);
			if(is_array($user_group_info)){
				$data['user_group_name'] = $user_group_info[0]['user_group_name'];

				// get the user group roles
				$user_group_roles = $this->User_group_model->get_user_group_roles((int) $id);
				if(is_array($user_group_roles)){
					$data['user_group_roles'] = $user_group_roles;
					$total_user_group_roles = count($user_group_roles);
					$cnt = 1;
					foreach($user_group_roles as $user_group_role){
						$not_in .= $user_group_role['module_roles_id'];
						if($cnt < $total_user_group_roles){
							$not_in .= ',';
						}
						$cnt++;
					}
				}

				// get the roles unassigned
				$roles = $this->Module_model->get_roles_unassigned($not_in);
				if(is_array($roles)){
					$data['roles'] = $roles;
				}
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('user_group/view');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* remove_role()
	* @access public
	* @param none
	* @return none
	*/
	public function remove_role(){
		$is_remove = $this->User_group_model->remove_role((int) $this->input->post('user_group_role_id'));
		if($is_remove){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* add_role()
	* @access public
	* @param none
	* @return none
	*/
	public function add_role(){
		$user_group_role = array(
			'user_group_id' => $this->input->post('user_group_id'),
			'module_roles_id' => $this->input->post('user_group_role_id')
		);
		$is_save = $this->User_group_model->save_user_group_role($user_group_role);
		if($is_save){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'user_group_name', 
				'label' => 'User Group Name', 
				'rules' => 'required|trim|is_unique[user_group.user_group_name]',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.',
					'is_unique' => 'Sorry. '.$this->input->post('user_group_name').' already exist. Please type the %s again.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file user_group.php */
/* Location: ./application/controllers/user_group.php */