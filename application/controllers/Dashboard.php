<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/dashboard
	 *	- or -  
	 * 		http://example.com/index.php/dashboard/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/dashboard/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();

		// load the libraries and models
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Access_role_model');
		$this->load->model('Billing_model');
		$this->load->model('Company_model');
		$this->load->model('Client_model');
		$this->load->model('Payment_model');
		$this->load->model('Income_model');
		
	}

	/**
	* view()
	* @access public
	* @param none
	* @return none
	*/
	public function view(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Dashboard';
			$data['page_sub_title'] = 'My Dashboard';
			$data['module_name'] = 'Dashboard';
			$data['monthly'] = '';
			$year = $data['selected_year'] = date('Y');

			if($this->input->post('select_year')){
				$year = $data['selected_year'] = $this->input->post('select_year');
			}

			if($this->session->userdata('company_id') == '' || $this->session->userdata('company_id') == 0){
				$companies = $this->Company_model->get_list();
			} else {
				$user_companies = explode('-', $this->session->userdata('company_id'));
				foreach($user_companies as $company_selected){
					$company_info = $this->Company_model->get_info($company_selected);
					if(is_array($company_info)){
						$companies[] = array(
							'id' => $company_selected,
							'company_name' => $company_info[0]['company_name']
						);
					}
				}
			}

			// get the companies
			$months = array('01','02','03','04','05','06','07','08','09','10','11','12');
			
			if(is_array($companies)){
				foreach($companies as $company){
					// get the monthly income of the company
					foreach($months as $month){
						$total = 0;
						$company_monthly = $this->Income_model->get_company_monthly_income($company['id'], $year.'-'.$month);
						if(is_array($company_monthly)){
							foreach($company_monthly as $company_monthly_income){
								$total = $company_monthly_income['total_income'] + $total;
							}
							$monthly_summary[] = array(
								'month' => $month,
								'income' => $total
							);
						} else {
							$monthly_summary[] = array(
								'month' => $month,
								'income' => $total
							);
						}
					}
					$monthly[] = array(
						'company_name' => $company['company_name'],
						'monthly_income' => $monthly_summary
					);
					unset($monthly_summary);
				}
				$data['monthly'] = $monthly;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('dashboard/dashboard');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* billing_monitoring()
	* @access public
	* @param none
	* @return none
	*/
	public function billing_monitoring(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Dashboard';
			$data['page_sub_title'] = 'Billing Monitoring';
			$data['module_name'] = 'Billing Monitoring';
			$data['billing_monitoring'] = '';
			$data['companies'] = '';
			$data['clients'] = '';
			$data['company_id'] = '';
			$data['company_name'] = '';

			$date_from = '';
			$date_to = '';

			if($this->input->post('date_from')){
				$date_from = date('Y-m-d', strtotime($this->input->post('date_from')));
			}

			if($this->input->post('date_to')){
				$date_to = date('Y-m-d', strtotime($this->input->post('date_to')));
			}

			//run the form validation
			$this->valid_billing_monitoring();
			if($this->form_validation->run() == TRUE){
				$search = array(
					'company_id' => (int) $this->input->post('company_id'),
					'client_id' => (int) $this->input->post('client_id'),
					'is_paid' => (string) $this->input->post('is_paid'),
					'billing_status' => (string) $this->input->post('billing_status'),
					'date_from' => $date_from,
					'date_to' => $date_to
				);
				$billing_monitoring = $this->Billing_model->billing_monitoring($search);
				if(is_array($billing_monitoring)){
					$data['billing_monitoring'] = $billing_monitoring;
				}
			}

			// check if login user under a company
			if($this->session->userdata('company_id') == '' || $this->session->userdata('company_id') == 0){
				$companies = $this->Company_model->get_list();
			} else {
				$user_companies = explode('-', $this->session->userdata('company_id'));
				foreach($user_companies as $company_selected){
					$company_info = $this->Company_model->get_info($company_selected);
					if(is_array($company_info)){
						$companies[] = array(
							'id' => $company_selected,
							'company_name' => $company_info[0]['company_name']
						);
					}
				}
			}

			$data['companies'] = $companies;

			// get all the clients
			$clients = $this->Client_model->get_all();
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('dashboard/billing_monitoring');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* payment_monitoring()
	* @access public
	* @param none
	* @return none
	*/
	public function payment_monitoring(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Dashboard';
			$data['page_sub_title'] = 'Payment Monitoring';
			$data['module_name'] = 'Payment Monitoring';
			$data['payment_monitoring'] = '';
			$data['clients'] = '';
			$data['companies'] = '';
			$data['company_id'] = '';
			$data['company_name'] = '';

			$date_from = '';
			$date_to = '';

			if($this->input->post('date_from')){
				$date_from = date('Y-m-d', strtotime($this->input->post('date_from')));
			}

			if($this->input->post('date_to')){
				$date_to = date('Y-m-d', strtotime($this->input->post('date_to')));
			}

			//run the form validation
			$this->valid_payment_monitoring();
			if($this->form_validation->run() == TRUE){
				$search = array(
					'company_id' => (int) $this->input->post('company_id'),
					'client_id' => (int) $this->input->post('client_id'),
					'date_from' => $date_from,
					'date_to' => $date_to
				);
				$payment_monitoring = $this->Payment_model->payment_monitoring($search);
				if(is_array($payment_monitoring)){
					$data['payment_monitoring'] = $payment_monitoring;
				}
			}

			// check if login user under a company
			if($this->session->userdata('company_id') == '' || $this->session->userdata('company_id') == 0){
				$companies = $this->Company_model->get_list();
			} else {
				$user_companies = explode('-', $this->session->userdata('company_id'));
				foreach($user_companies as $company_selected){
					$company_info = $this->Company_model->get_info($company_selected);
					if(is_array($company_info)){
						$companies[] = array(
							'id' => $company_selected,
							'company_name' => $company_info[0]['company_name']
						);
					}
				}
			}

			$data['companies'] = $companies;

			// get all the clients
			$clients = $this->Client_model->get_all();
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('dashboard/payment_monitoring');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* income_summary()
	* @access public
	* @param none
	* @return none
	*/
	public function income_summary(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Dashboard';
			$data['page_sub_title'] = 'Income Summary';
			$data['module_name'] = 'Income Summary';
			$data['income_summary'] = '';
			$data['companies'] = '';
			$data['clients'] = '';
			$data['company_id'] = '';
			$data['company_name'] = '';

			$date_from = '';
			$date_to = '';

			if($this->input->post('date_from')){
				$date_from = date('Y-m-d', strtotime($this->input->post('date_from')));
			}

			if($this->input->post('date_to')){
				$date_to = date('Y-m-d', strtotime($this->input->post('date_to')));
			}


			//run the form validation
			$this->valid_payment_monitoring();
			if($this->form_validation->run() == TRUE){
				$search = array(
					'company_id' => (int) $this->input->post('company_id'),
					'client_id' => (int) $this->input->post('client_id'),
					'date_from' => $date_from,
					'date_to' => $date_to
				);
				$income_summary = $this->Income_model->income_summary($search);
				if(is_array($income_summary)){
					$data['income_summary'] = $income_summary;
				}
			}

			// check if login user under a company
			if($this->session->userdata('company_id') == '' || $this->session->userdata('company_id') == 0){
				$companies = $this->Company_model->get_list();
			} else {
				$user_companies = explode('-', $this->session->userdata('company_id'));
				foreach($user_companies as $company_selected){
					$company_info = $this->Company_model->get_info($company_selected);
					if(is_array($company_info)){
						$companies[] = array(
							'id' => $company_selected,
							'company_name' => $company_info[0]['company_name']
						);
					}
				}
			}

			$data['companies'] = $companies;

			// get all the clients
			$clients = $this->Client_model->get_all();
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('dashboard/income_summary');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* valid_billing_monitoring()
	* @access public
	* @param none
	* @return none
	*/
	private function valid_billing_monitoring(){
		$config = array(
			array(
				'field' => 'company_id', 
				'label' => 'Company', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}

	/**
	* valid_payment_monitoring()
	* @access public
	* @param none
	* @return none
	*/
	private function valid_payment_monitoring(){
		$config = array(
			array(
				'field' => 'company_id', 
				'label' => 'Company', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */