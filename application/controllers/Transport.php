<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transport extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/transport
	 *	- or -  
	 * 		http://example.com/index.php/transport/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/transport/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		parent::__construct();

		// load the necessary models and libraries
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Activity_log_model');
		$this->load->model('Access_role_model');
		$this->load->model('Transport_model');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Transport';
			$data['page_sub_title'] = 'Add Transport';
			$data['module_name'] = 'Transport';
			$data['message'] = $this->session->flashdata('message');

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the transport
				$transport = array(
					'transport_name' => (string) $this->input->post('transport_name'),
					'description' => (string) $this->input->post('description')
				);
				$is_save = $this->Transport_model->save($transport);
				if($is_save){
					$this->session->set_flashdata('message', 'Transport successfully added.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in saving the transport information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/transport/add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Transport';
			$data['page_sub_title'] = 'List of Transport';
			$data['module_name'] = 'Transport';
			$data['transports'] = '';
			$data['edit_role'] = '';
			$data['delete_role'] = '';

			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			// get the list of transports
			$transports = $this->Transport_model->get_list();
			if(is_array($transports)){
				$data['transports'] = $transports;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/transport/listing');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* edit()
	* @access public
	* @param int $id
	* @return none
	*/
	public function edit($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('transport/listing'));

			// variable initialization
			$data['page_title'] = 'Transport';
			$data['page_sub_title'] = 'Edit Transport';
			$data['module_name'] = 'Transport';
			$data['message'] = $this->session->flashdata('message');
			$data['transport_name'] = '';
			$data['description'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the transport
				$transport = array(
					'transport_name' => (string) $this->input->post('transport_name'),
					'description' => (string) $this->input->post('description'),
					'id' => (int) $id
				);
				$is_save = $this->Transport_model->update($transport);
				if($is_save){
					$this->session->set_flashdata('message', 'Transport successfully updated.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in updating the transport information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the transport information
			$transport = $this->Transport_model->get_info((int) $id);
			if(is_array($transport)){
				$data['transport_name'] = $transport[0]['transport_name'];
				$data['description'] = $transport[0]['description'];
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/transport/edit');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* delete()
	* @access public
	* @param none
	* @return none
	*/
	public function delete(){
		$is_delete = $this->Transport_model->delete((int) $this->input->post('transport_id'));
		if($is_delete){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'transport_name', 
				'label' => 'Transport Name', 
				'rules' => 'required|trim|is_unique[transport.transport_name]',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.',
					'is_unique' => 'Sorry. '.$this->input->post('transport_name').' already exist. Please type the %s again.'
				)
			),
			array(
				'field' => 'description', 
				'label' => 'Transport Description', 
				'rules' => 'trim'
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file transport.php */
/* Location: ./application/controllers/transport.php */