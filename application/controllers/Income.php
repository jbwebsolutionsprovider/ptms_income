<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Income extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/project
	 *	- or -
	 * 		http://example.com/index.php/project/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/project/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();

		// load the necessary models and libraries
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Activity_log_model');
		$this->load->model('Access_role_model');
		$this->load->model('Project_model');
		$this->load->model('Client_model');
		$this->load->model('Company_model');
		$this->load->model('Income_type_model');
		$this->load->model('Income_category_model');
		$this->load->model('Unit_type_model');
		$this->load->model('Income_model');
		$this->load->model('Transport_model');
		$this->load->model('Place_model');
		$this->load->model('Cargo_model');
		$this->load->library('pagination');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Income';
			$data['page_sub_title'] = 'Add Income';
			$data['module_name'] = 'Income';
			$data['message'] = $this->session->flashdata('message');
			$data['companies'] = '';
			$data['clients'] = '';
			$data['income_types'] = '';
			$data['income_categories'] = '';
			$data['unit_types'] = '';
			$data['projects'] = '';
			$data['company_id'] = '';
			$data['company_name'] = '';
			$data['transports'] = '';
			$data['places'] = '';
			$data['cargoes'] = '';
			$data['files'] = $this->cart->contents();

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the income
				$income = array(
					'company_id' => (int) $this->input->post('company_id'),
					'date_done' => date('Y-m-d', strtotime($this->input->post('date_done'))),
					'client_id' => (int) $this->input->post('client_id'),
					'project_id' => (int) $this->input->post('project_id'),
					'income_type_id' => (int) $this->input->post('income_type_id'),
					'income_category_id' => (int) $this->input->post('income_category_id'),
					'contract_price' => (float) $this->input->post('contract_price'),
					'total_income' => $this->input->post('total_income'),
					'total_income_type' => (string) $this->input->post('total_income_type'),
					'transport' => (int) $this->input->post('transport'),
					'unit_type_id' => (int) $this->input->post('unit_type_id'),
					'quantity' => (int) $this->input->post('quantity'),
					'place_from' => (int) $this->input->post('place_from'),
					'cargoes' => (int) $this->input->post('cargoes'),
					'place_to' => (int) $this->input->post('place_to'),
					'price_per_unit' => (float) $this->input->post('price_per_unit'),
					'remarks' => (string) $this->input->post('remarks')
				);
				$is_save = $this->Income_model->save($income);
				if($is_save){

					// check if there are items in the cart
					if($this->cart->total_items()){
						// save the files
						foreach($this->cart->contents() as $files){
							$file = array(
								'income_id' => $is_save,
								'filename' => $files['name']
							);
							$this->Income_model->save_file($file);
						}

						// clear the cart
						$this->cart->destroy();
					}

					// change the is_used status of the project
					$is_used = array(
						'is_used' => 'yes',
						'id' => (int) $this->input->post('project_id')
					);
					$this->Project_model->update_is_used($is_used);

					$this->session->set_flashdata('message', 'Income successfully added.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in saving the income information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// check if login user under a company
			if($this->session->userdata('company_id') == '' || $this->session->userdata('company_id') == 0){
				$companies = $this->Company_model->get_list();
			} else {
				$user_companies = explode('-', $this->session->userdata('company_id'));
				foreach($user_companies as $company_selected){
					$company_info = $this->Company_model->get_info($company_selected);
					if(is_array($company_info)){
						$companies[] = array(
							'id' => $company_selected,
							'company_name' => $company_info[0]['company_name']
						);
					}
				}
			}

			$data['companies'] = $companies;

			// get all the clients
			$clients = $this->Client_model->get_all();
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// get all the income types
			$income_types = $this->Income_type_model->get_list();
			if(is_array($income_types)){
				$data['income_types'] = $income_types;
			}

			// get all the income categories
			$income_categories = $this->Income_category_model->get_list();
			if(is_array($income_categories)){
				$data['income_categories'] = $income_categories;
			}

			// get all the unit type
			$unit_types = $this->Unit_type_model->get_list();
			if(is_array($unit_types)){
				$data['unit_types'] = $unit_types;
			}

			// get the projects
			if($this->input->post('client_id')){
				// get the client projects
				$projects = $this->Project_model->get_client_projects((int) $this->input->post('client_id'));
				if(is_array($projects)){
					$data['projects'] = $projects;
				}
			}

			// get the transports
			$transports = $this->Transport_model->get_list();
			if(is_array($transports)){
				$data['transports'] = $transports;
			}

			// get the place
			$places = $this->Place_model->get_list();
			if(is_array($places)){
				$data['places'] = $places;
			}

			// get the cargo
			$cargoes = $this->Cargo_model->get_list();
			if(is_array($cargoes)){
				$data['cargoes'] = $cargoes;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('forms/income/add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* add_file()
	* @access public
	* @param none
	* @return none
	*/
	public function add_file(){
		// store to cart session
		$data = array(
			'id'      => uniqid(),
			'qty'     => 1,
			'price'   => 1,
			'name'    => $this->input->post('filename')
		);
		$this->cart->insert($data);

		// display the files
		$this->display_files();
	}

	/**
	* display_files()
	* @access public
	* @param none
	* @return none
	*/
	public function display_files(){
		// load the view file
		$data['files'] = $this->cart->contents();
		$this->load->view('forms/income/display_files', $data);
	}

	/**
	* delete_file()
	* @access public
	* @param none
	* @return none
	*/
	public function delete_file(){
		$data = array(
		'rowid' => $this->input->post('rowid'),
		'qty'   => 0
		);

		$this->cart->update($data);

		// delete the file
		unlink('allfiles/files/income/'.$this->input->post('filename'));

		// display the files
		$this->display_files();
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Income';
			$data['page_sub_title'] = 'List of Income';
			$data['module_name'] = 'Income';
			$data['incomes'] = '';
			$data['pages'] = '';
			$data['message'] = $this->session->flashdata('message');
			$data['edit_role'] = '';
			$data['delete_role'] = '';
			$data['view_role'] = '';
			$user_company = '';

			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			$has_role_view = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'view');
			if($has_role_view || $this->session->userdata('is_admin') == 'yes'){
				$data['view_role'] = 'yes';
			}

			// get the filters stored in session
			$company_selected = $this->session->userdata('income_search_company');
			if($this->input->post('company_id')){
				$company_selected = $this->input->post('company_id');
				$this->session->set_userdata('income_search_company', $this->input->post('company_id'));
			}

			$client_selected = $this->session->userdata('income_search_client');
			if($this->input->post('client_id')){
				$client_selected = $this->input->post('client_id');
				$this->session->set_userdata('income_search_client', $this->input->post('client_id'));
			}

			$date_from_selected = $this->session->userdata('income_search_date_from');
			if($this->input->post('date_from')){
				$date_from_selected = date('Y-m-d', strtotime($this->input->post('date_from')));
				$this->session->set_userdata('income_search_date_from', date('Y-m-d', strtotime($this->input->post('date_from'))));
			}

			$date_to_selected = $this->session->userdata('income_search_date_to');
			if($this->input->post('date_to')){
				$date_to_selected = date('Y-m-d', strtotime($this->input->post('date_to')));
				$this->session->set_userdata('income_search_date_to', strtotime($this->input->post('date_to')));
			}

			if($company_selected){
				$user_company = $company_selected;
			} else {
				if($this->session->userdata('company_id')){
					$user_company = str_replace('-', ',', $this->session->userdata('company_id'));
				}
			}

			$search = array(
				'company_id' => $user_company,
				'client_id' => $client_selected,
				'date_from' => $date_from_selected,
				'date_to' => $date_to_selected
			);

			// for view
			$data['company_id'] = $user_company;
			$data['client_id'] = $client_selected;
			$data['date_from'] = $date_from_selected;
			$data['date_to'] = $date_to_selected;

			// get the total income
			$total_income = $this->Income_model->get_total($search);

			// setup the pagination
			$offset = 0;
			if($this->uri->segment(3)){
				$offset = $this->uri->segment(3);
			}

			$config['base_url'] = site_url('income/listing/');
			$config['total_rows'] = $total_income;
			$config['per_page'] = INCOME_PER_PAGE;
			$config['full_tag_open'] = '<ul class="pagination no-margin pull-right">';
			$config['full_tag_close'] = '</ul>';
			$config['prev_link'] = 'Previous';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li class="next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li class="next">';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li class="next">';
			$config['first_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pages'] =  $this->pagination->create_links();

			// get the list of income
			$incomes = $this->Income_model->get_list($offset, $search);
			if(is_array($incomes)){
				$data['incomes'] = $incomes;
			}

			// check if login user under a company
			if($this->session->userdata('company_id') == '' || $this->session->userdata('company_id') == 0){
				$companies = $this->Company_model->get_list();
			} else {
				$user_companies = explode('-', $this->session->userdata('company_id'));
				foreach($user_companies as $company_selected){
					$company_info = $this->Company_model->get_info($company_selected);
					if(is_array($company_info)){
						$companies[] = array(
							'id' => $company_selected,
							'company_name' => $company_info[0]['company_name']
						);
					}
				}
			}

			$data['companies'] = $companies;

			// get all the clients
			$clients = $this->Client_model->get_all();
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('forms/income/listing');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* clear_search()
	* @access public
	* @param none
	* @return none
	*/
	public function clear_search(){
		// clear the session
		$this->session->set_userdata('income_search_company', '');
		$this->session->set_userdata('income_search_client', '');
		$this->session->set_userdata('income_search_date_from', '');
		$this->session->set_userdata('income_search_date_to', '');
	}

	/**
	* view()
	* @access public
	* @param int $id
	* @return none
	*/
	public function view($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('income/listing'));

			// variable initialization
			$data['page_title'] = 'Income';
			$data['page_sub_title'] = 'View Income';
			$data['module_name'] = 'Income';
			$data['message'] = $this->session->flashdata('message');
			$data['id'] = (int) $id;
			$data['project_name'] = '';
			$data['company_name'] = '';
			$data['date_done'] = '';
			$data['client_name'] = '';
			$data['project_name'] = '';
			$data['income_type_name'] = '';
			$data['income_category_name'] = '';
			$data['contract_price'] = '';
			$data['total_income'] = '';
			$data['total_income_type'] = '';
			$data['transport_name'] = '';
			$data['unit_type_name'] = '';
			$data['quantity'] = '';
			$data['place_name'] = '';
			$data['place_to'] = '';
			$data['cargo_name'] = '';
			$data['price_per_unit'] = '';
			$data['remarks'] = '';
			$data['files'] = '';
			$data['edit_role'] = '';
			$data['delete_role'] = '';

			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			$has_role_closed = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'closed');
			if($has_role_closed || $this->session->userdata('is_admin') == 'yes'){
				$data['closed_role'] = 'yes';
			}

			// get the income information
			$income = $this->Income_model->get_info((int) $id);
			if(is_array($income)){
				$data['project_name'] = $income[0]['project_name'];
				$data['company_name'] = $income[0]['company_name'];
				$data['date_done'] = $income[0]['date_done'];
				$data['client_name'] = $income[0]['client_name'];
				$data['income_type_name'] = $income[0]['income_type_name'];
				$data['income_category_id'] = $income[0]['income_category_id'];
				$data['income_category_name'] = $income[0]['income_category_name'];
				$data['contract_price'] = $income[0]['contract_price'];
				$data['total_income'] = $income[0]['total_income'];
				$data['total_income_type'] = $income[0]['total_income_type'];
				$data['transport_name'] = $income[0]['transport_name'];
				$data['unit_type_name'] = $income[0]['unit_type_name'];
				$data['quantity'] = $income[0]['quantity'];
				$data['place_name'] = $income[0]['place_name'];
				$data['cargo_name'] = $income[0]['cargo_name'];
				$data['price_per_unit'] = $income[0]['price_per_unit'];
				$data['remarks'] = $income[0]['remarks'];

				// get the place to
				$place_to = $this->Place_model->get_info($income[0]['place_to']);
				if(is_array($place_to)){
					$data['place_to'] = $place_to[0]['place_name'];
				}

				// get the file attachments
				$files = $this->Income_model->get_files((int) $id);
				if(is_array($files)){
					$data['files'] = $files;
				}
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('forms/income/view');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* save_file()
	* @access public
	* @param none
	* @return none
	*/
	public function save_file(){
		$file_info = array(
			'income_id' => (int) $this->input->post('income_id'),
			'filename' => (string) $this->input->post('filename')
		);

		$is_save = $this->Income_model->save_file($file_info);
		if($is_save){
			// display the files
			$this->display_file((int) $this->input->post('income_id'));
		}
	}

	/**
	* display_file()
	* @access private
	* @param none
	* @return none
	*/
	private function display_file($income_id){
		if($income_id){
			// variable initialization
			$data['files'] = '';

			// get the files
			$files = $this->Income_model->get_files($income_id);
			if(is_array($files)){
				$data['files'] = $files;
			}

			// display the view file
			$this->load->view('forms/income/files', $data);
		}
	}

	/**
	* edit()
	* @access public
	* @param int $id
	* @return none
	*/
	public function edit($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('income/listing'));

			// variable initialization
			$data['page_title'] = 'Income';
			$data['page_sub_title'] = 'Edit Income';
			$data['module_name'] = 'Income';
			$data['message'] = $this->session->flashdata('message');
			$data['id'] = (int) $id;
			$data['project_id'] = '';
			$data['project_name'] = '';
			$data['company_id'] = '';
			$data['date_done'] = '';
			$data['client_id'] = '';
			$data['client_name'] = '';
			$data['income_type_id'] = '';
			$data['income_category_id'] = '';
			$data['contract_price'] = '';
			$data['total_income'] = '';
			$data['total_income_type'] = '';
			$data['transport'] = '';
			$data['unit_type_id'] = '';
			$data['quantity'] = '';
			$data['place_from'] = '';
			$data['place_to'] = '';
			$data['cargoes'] = '';
			$data['price_per_unit'] = '';
			$data['remarks'] = '';
			$data['company_id'] = '';
			$data['company_name'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the income
				$income = array(
					'company_id' => (int) $this->input->post('company_id'),
					'date_done' => date('Y-m-d', strtotime($this->input->post('date_done'))),
					'client_id' => (int) $this->input->post('client_id'),
					'project_id' => (int) $this->input->post('project_id'),
					'income_type_id' => (int) $this->input->post('income_type_id'),
					'income_category_id' => (int) $this->input->post('income_category_id'),
					'contract_price' => (float) $this->input->post('contract_price'),
					'total_income' => $this->input->post('total_income'),
					'total_income_type' => (string) $this->input->post('total_income_type'),
					'transport' => (int) $this->input->post('transport'),
					'unit_type_id' => (int) $this->input->post('unit_type_id'),
					'quantity' => (int) $this->input->post('quantity'),
					'place_from' => (int) $this->input->post('place_from'),
					'cargoes' => (int) $this->input->post('cargoes'),
					'place_to' => (int) $this->input->post('place_to'),
					'price_per_unit' => (float) $this->input->post('price_per_unit'),
					'remarks' => (string) $this->input->post('remarks'),
					'id' => (int) $id
				);
				$is_save = $this->Income_model->update($income);
				if($is_save){
					$this->session->set_flashdata('message', 'Income successfully updated.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in updating the income information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the income information
			$income = $this->Income_model->get_info((int) $id);
			if(is_array($income)){
				$data['project_id'] = $income[0]['project_id'];
				$data['company_id'] = $income[0]['company_id'];
				$data['company_name'] = $income[0]['company_name'];
				$data['date_done'] = date('m/d/Y', strtotime($income[0]['date_done']));
				$data['client_id'] = $income[0]['client_id'];
				$data['client_name'] = $income[0]['client_name'];
				$data['project_id'] = $income[0]['project_id'];
				$data['project_name'] = $income[0]['project_name'];
				$data['income_type_id'] = $income[0]['income_type_id'];
				$data['income_category_id'] = $income[0]['income_category_id'];
				$data['contract_price'] = $income[0]['contract_price'];
				$data['total_income'] = $income[0]['total_income'];
				$data['total_income_type'] = $income[0]['total_income_type'];
				$data['transport'] = $income[0]['transport'];
				$data['unit_type_id'] = $income[0]['unit_type_id'];
				$data['quantity'] = $income[0]['quantity'];
				$data['place_from'] = $income[0]['place_from'];
				$data['place_to'] = $income[0]['place_to'];
				$data['cargoes_selected'] = $income[0]['cargoes'];
				$data['price_per_unit'] = $income[0]['price_per_unit'];
				$data['remarks'] = $income[0]['remarks'];
			}

			// check if login user under a company
			if($this->session->userdata('company_id')){
				$data['company_id'] = $this->session->userdata('company_id');
				// get the company information
				$company = $this->Company_model->get_info((int) $this->session->userdata('company_id'));
				if(is_array($company)){
					$data['company_name'] = $company[0]['company_name'];
				}
			} else {
				// get all the companies
				$companies = $this->Company_model->get_list();
				if(is_array($companies)){
					$data['companies'] = $companies;
				}
			}

			// get all the clients
			$clients = $this->Client_model->get_all();
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// get all the income types
			$income_types = $this->Income_type_model->get_list();
			if(is_array($income_types)){
				$data['income_types'] = $income_types;
			}

			// get all the income categories
			$income_categories = $this->Income_category_model->get_list();
			if(is_array($income_categories)){
				$data['income_categories'] = $income_categories;
			}

			// get all the unit type
			$unit_types = $this->Unit_type_model->get_list();
			if(is_array($unit_types)){
				$data['unit_types'] = $unit_types;
			}

			// get the transports
			$transports = $this->Transport_model->get_list();
			if(is_array($transports)){
				$data['transports'] = $transports;
			}

			// get the place
			$places = $this->Place_model->get_list();
			if(is_array($places)){
				$data['places'] = $places;
			}

			// get the cargo
			$cargoes = $this->Cargo_model->get_list();
			if(is_array($cargoes)){
				$data['cargoes'] = $cargoes;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('forms/income/edit');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* delete()
	* @access public
	* @param none
	* @return none
	*/
	public function delete(){
		// before deleting the project record, check first if client has exiting transactions
		$is_delete = $this->Income_model->delete((int) $this->input->post('income_id'));
		if($is_delete){
			$this->session->set_flashdata('message', 'Income information successfully deleted.');
			echo 'SUCCESS';
		} else {
			$this->session->set_flashdata('message', 'There was an error in deleting the income. Please contact the Administrator.');
			echo 'ERROR';
		}
	}

	/**
	* closed()
	* @access public
	* @param none
	* @return none
	*/
	public function closed(){
		$closed = array(
			'project_status' => 'closed',
			'id' => (int) $this->input->post('project_id')
		);
		$is_update = $this->Project_model->update_status($closed);
		if($is_update){
			$this->session->set_flashdata('message', 'Project information successfully changed status to closed.');
			echo 'SUCCESS';
		} else {
			$this->session->set_flashdata('message', 'There was an error in updating the project status. Please contact the Administrator.');
			echo 'ERROR';
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'company_id',
				'label' => 'Company Name',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'date_done',
				'label' => 'Date Done',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'client_id',
				'label' => 'Client Name',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'project_id',
				'label' => 'Project',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'income_type_id',
				'label' => 'Income Type',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'income_category_id',
				'label' => 'Income Category',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'contract_price',
				'label' => 'Contract Price',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'total_income',
				'label' => 'Total Income',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'total_income_type',
				'label' => 'Total Income Type',
				'rules' => 'trim'
			),
			array(
				'field' => 'transport',
				'label' => 'Transport',
				'rules' => 'trim'
			),
			array(
				'field' => 'unit_type_id',
				'label' => 'Unit Type',
				'rules' => 'trim'
			),
			array(
				'field' => 'quantity',
				'label' => 'Quantity',
				'rules' => 'trim'
			),
			array(
				'field' => 'place_from',
				'label' => 'Place From',
				'rules' => 'trim'
			),
			array(
				'field' => 'place_to',
				'label' => 'Place To',
				'rules' => 'trim'
			),
			array(
				'field' => 'cargoes',
				'label' => 'Cargoes',
				'rules' => 'trim'
			),
			array(
				'field' => 'price_per_unit',
				'label' => 'Price per Unit',
				'rules' => 'trim'
			),
			array(
				'field' => 'remarks',
				'label' => 'Remarks',
				'rules' => 'trim'
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file position.php */
/* Location: ./application/controllers/position.php */