<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/client
	 *	- or -  
	 * 		http://example.com/index.php/client/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/client/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		parent::__construct();

		// load the necessary models and libraries
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Activity_log_model');
		$this->load->model('Access_role_model');
		$this->load->model('Client_model');
		$this->load->model('Company_model');
		$this->load->model('Bank_model');
		$this->load->library('pagination');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Client';
			$data['page_sub_title'] = 'Add Client';
			$data['module_name'] = 'Client';
			$data['message'] = $this->session->flashdata('message');
			$data['companies'] = '';
			$data['banks'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the client
				$client = array(
					'client_name' => (string) $this->input->post('client_name'),
					'address' => (string) $this->input->post('address'),
					'business_type' => (string) $this->input->post('business_type'),
					'person_in_charge' => (string) $this->input->post('person_in_charge'),
					'contacts' => (string) $this->input->post('contacts')
				);
				$is_save = $this->Client_model->save($client);
				if($is_save){
					// save the bank details
					if($this->cart->total_items() > 0){
						$banks = $this->cart->contents();

						// destroy the cart content
						$this->cart->destroy();

						// save the client bank details
						foreach($banks as $bank){
							$bank_detail = array(
								'client_id' => $is_save,
								'bank_id' => $bank['id'],
								'account_number' => $bank['options']['account_number']
							);
							$this->Client_model->save_bank_detail($bank_detail);
						}
					}
					$this->session->set_flashdata('message', 'Client successfully added.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in saving the client information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the company
			$companies = $this->Company_model->get_list();
			if(is_array($companies)){
				$data['companies'] = $companies;
			}

			// get the banks
			$banks = $this->Bank_model->get_list();
			if(is_array($banks)){
				$data['banks'] = $banks;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/client/add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* add_bank()
	* @access public
	* @param none
	* @return none
	*/
	public function add_bank(){
		$bank = explode('-', $this->input->post('bank_id'));
		$bank_data = array(
			'id'      => $bank[0],
			'qty'     => 1,
			'price'   => 200,
			'name'    => $bank[1],
			'options' => array (
				'account_number' => $this->input->post('account_number')
			)
		);
		$this->cart->insert($bank_data);

		// display the bank account details
		$this->display_bank();
	}

	/**
	* remove_bank()
	* @access public
	* @param none
	* @return none
	*/
	public function remove_bank(){
		$data = array(
			'rowid' => (string) $this->input->post('rowid'),
			'qty'   => 0
		);
		$this->cart->update($data);

		// display the bank account details
		$this->display_bank();
	}

	/**
	* display_bank()
	* @access private
	* @param none
	* @return none
	*/
	private function display_bank(){
		//  load the view files
		$this->load->view('settings/client/bank_details');
	}

	/**
	* bank_add()
	* @access public
	* @param none
	* @return none
	*/
	public function bank_add($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('client/listing'));

			// variable initialization
			$data['page_title'] = 'Client';
			$data['page_sub_title'] = 'Add Bank';
			$data['module_name'] = 'Client';
			$data['message'] = $this->session->flashdata('message');
			$data['id'] = (int) $id;
			$data['client_name'] = '';

			// get the client information
			$client = $this->Client_model->get_info((int) $id);
			if(is_array($client)){
				$data['client_name'] = $client[0]['client_name'];
			}

			// get the banks
			$banks = $this->Bank_model->get_list();
			if(is_array($banks)){
				$data['banks'] = $banks;
			}

			// run the form validation
			$this->validate_add_bank();
			if($this->form_validation->run() == TRUE){
				// save the bank account number
				$bank_detail = array(
					'client_id' => (int) $id,
					'bank_id' => (int) $this->input->post('bank'),
					'account_number' => (string) $this->input->post('account_number')
				);
				$is_save = $this->Client_model->save_bank_detail($bank_detail);
				if(is_save){
					$this->session->set_flashdata('message', 'Client bank account details successfully added.');
				} else {
					$this->session->set_flashdata('message', 'There was an error in adding the client bank account details. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), 'bank', 'add');
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/client/bank_add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* bank_edit()
	* @access public
	* @param none
	* @return none
	*/
	public function bank_edit($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('client/listing'));

			// variable initialization
			$data['page_title'] = 'Client';
			$data['page_sub_title'] = 'Edit Bank';
			$data['module_name'] = 'Client';
			$data['message'] = $this->session->flashdata('message');
			$data['id'] = (int) $id;
			$data['client_name'] = '';
			$data['bank_name'] = '';
			$data['account_number'] = '';

			// get the bank account details
			$bank_account = $this->Client_model->get_bank_account_info((int) $id);
			if(is_array($bank_account)){
				$data['client_name'] = $bank_account[0]['client_name'];
				$data['bank_name'] = $bank_account[0]['bank_name'].' '.$bank_account[0]['branch'];
				$data['account_number'] = $bank_account[0]['account_number'];
			}

			// run the form validation
			$this->validate_edit_bank();
			if($this->form_validation->run() == TRUE){
				// update the bank account number
				$bank_detail = array(
					'id' => (int) $id,
					'account_number' => (string) $this->input->post('account_number')
				);
				$is_save = $this->Client_model->update_bank_detail($bank_detail);
				if(is_save){
					$this->session->set_flashdata('message', 'Client bank account details successfully updated.');
				} else {
					$this->session->set_flashdata('message', 'No changes made on the bank account detail.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), 'bank', 'edit');
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/client/bank_edit');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* bank_delete()
	* @access public
	* @param none
	* @return none
	*/
	public function bank_delete(){
		// check if has role to delete
		$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), 'bank', 'delete');
		if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
			$is_delete = $this->Client_model->delete_bank((int) $this->input->post('bank_account_id'));
			if($is_delete){
				$this->session->set_flashdata('message', 'Client bank account details successfully deleted.');
			} else {
				$this->session->set_flashdata('message', 'There was an error found in deleting the bank account detail. Please contact the Administrator.');
			}
		} else {
			$this->session->set_flashdata('message', 'You do not have the access to delete.');
		}
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'Client';
			$data['page_sub_title'] = 'List of Client';
			$data['module_name'] = 'Client';
			$data['clients'] = '';
			$data['pages'] = '';
			$data['message'] = $this->session->flashdata('message');
			$data['edit_role'] = '';
			$data['delete_role'] = '';
			$data['view_role'] = '';

			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			$has_role_view = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'view');
			if($has_role_view || $this->session->userdata('is_admin') == 'yes'){
				$data['view_role'] = 'yes';
			}

			// get the total clients
			$total_clients = $this->Client_model->get_total();

			// setup the pagination
			$offset = 0;
			if($this->uri->segment(3)){
				$offset = $this->uri->segment(3);
			}

			$config['base_url'] = site_url('client/listing/');
			$config['total_rows'] = $total_clients;
			$config['per_page'] = CLIENT_PER_PAGE; 
			$config['full_tag_open'] = '<ul class="pagination no-margin pull-right">';
			$config['full_tag_close'] = '</ul>';
			$config['prev_link'] = 'Previous';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li class="next">';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li class="next">';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li class="next">';
			$config['first_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pages'] =  $this->pagination->create_links();

			// get the list of clients
			$clients = $this->Client_model->get_list($offset);
			if(is_array($clients)){
				$data['clients'] = $clients;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/client/listing');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* view()
	* @access public
	* @param int $id
	* @return none
	*/
	public function view($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('client/listing'));

			// variable initialization
			$data['page_title'] = 'Client';
			$data['page_sub_title'] = 'View Client';
			$data['module_name'] = 'Client';
			$data['message'] = $this->session->flashdata('message');
			$data['id'] = (int) $id;
			$data['client_name'] = '';
			$data['address'] = '';
			$data['business_type'] = '';
			$data['person_in_charge'] = '';
			$data['contacts'] = '';
			$data['bank_accounts'] = '';
			$data['edit_role'] = '';
			$data['delete_role'] = '';
			$data['add_bank_role'] = '';
			$data['edit_bank_role'] = '';
			$data['delete_bank_role'] = '';

			// check if has edit role
			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			// check if has delete role
			$has_role_delete = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'delete');
			if($has_role_delete || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_role'] = 'yes';
			}

			// check if has add bank role
			$has_role_add_bank = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), 'bank', 'add');
			if($has_role_add_bank || $this->session->userdata('is_admin') == 'yes'){
				$data['add_bank_role'] = 'yes';
			}

			// check if has edit bank role
			$has_role_edit_bank = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), 'bank', 'edit');
			if($has_role_edit_bank || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_bank_role'] = 'yes';
			}

			// check if has delete bank role
			$has_role_delete_bank = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), 'bank', 'delete');
			if($has_role_delete_bank || $this->session->userdata('is_admin') == 'yes'){
				$data['delete_bank_role'] = 'yes';
			}

			// get the client information
			$client = $this->Client_model->get_info((int) $id);
			if(is_array($client)){
				$data['client_name'] = $client[0]['client_name'];
				$data['address'] = $client[0]['address'];
				$data['business_type'] = $client[0]['business_type'];
				$data['person_in_charge'] = $client[0]['person_in_charge'];
				$data['contacts'] = $client[0]['contacts'];

				// get the client bank accounts
				$bank_accounts = $this->Client_model->get_bank_accounts((int) $id);
				if(is_array($bank_accounts)){
					$data['bank_accounts'] = $bank_accounts;
				}
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/client/view');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* edit()
	* @access public
	* @param int $id
	* @return none
	*/
	public function edit($id){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is data passed
			if($id == '')
				redirect(site_url('client/listing'));

			// variable initialization
			$data['page_title'] = 'Client';
			$data['page_sub_title'] = 'Edit Client';
			$data['module_name'] = 'Client';
			$data['message'] = $this->session->flashdata('message');
			$data['id'] = (int) $id;
			$data['client_name'] = '';
			$data['address'] = '';
			$data['business_type'] = '';
			$data['person_in_charge'] = '';
			$data['contacts'] = '';
			$data['bank_accounts'] = '';

			// run the form validation
			$this->validate_edit();
			if($this->form_validation->run() == TRUE){
				// save the client
				$client = array(
					'client_name' => (string) $this->input->post('client_name'),
					'address' => (string) $this->input->post('address'),
					'business_type' => (string) $this->input->post('business_type'),
					'person_in_charge' => (string) $this->input->post('person_in_charge'),
					'contacts' => (string) $this->input->post('contacts'),
					'id' => (int) $id
				);
				$is_save = $this->Client_model->update($client);
				if($is_save){
					$this->session->set_flashdata('message', 'Client successfully updated.');
				} else {
					$this->session->set_flashdata('message', 'There was an error found in updating the client information. Please contact the Administrator.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the client information
			$client = $this->Client_model->get_info((int) $id);
			if(is_array($client)){
				$data['client_name'] = $client[0]['client_name'];
				$data['address'] = $client[0]['address'];
				$data['business_type'] = $client[0]['business_type'];
				$data['person_in_charge'] = $client[0]['person_in_charge'];
				$data['contacts'] = $client[0]['contacts'];
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('settings/client/edit');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* delete()
	* @access public
	* @param none
	* @return none
	*/
	public function delete(){
		// before deleting the client record, check first if client has exiting transactions
		$is_delete = $this->Client_model->delete((int) $this->input->post('client_id'));
		if($is_delete){
			$this->session->set_flashdata('message', 'Client information successfully deleted.');
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'client_name', 
				'label' => 'Client Name', 
				'rules' => 'required|trim|is_unique[client.client_name]',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.',
					'is_unique' => 'Sorry. '.$this->input->post('client_name').' already exist. Please type the %s again.'
				)
			),
			array(
				'field' => 'address', 
				'label' => 'Address', 
				'rules' => 'trim'
			),
			array(
				'field' => 'business_type', 
				'label' => 'Business Type', 
				'rules' => 'trim'
			),
			array(
				'field' => 'person_in_charge', 
				'label' => 'Person In Charge', 
				'rules' => 'trim'
			),
			array(
				'field' => 'contacts', 
				'label' => 'Contacts', 
				'rules' => 'trim'
			),
			array(
				'field' => 'bank_details', 
				'label' => 'Bank Details', 
				'rules' => 'trim'
			)
		);
		$this->form_validation->set_rules($config);
	}

	/**
	* validate_edit()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_edit(){
		$config = array(
			array(
				'field' => 'client_name', 
				'label' => 'Client Name', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'address', 
				'label' => 'Address', 
				'rules' => 'trim'
			),
			array(
				'field' => 'business_type', 
				'label' => 'Business Type', 
				'rules' => 'trim'
			),
			array(
				'field' => 'person_in_charge', 
				'label' => 'Person In Charge', 
				'rules' => 'trim'
			),
			array(
				'field' => 'contacts', 
				'label' => 'Contacts', 
				'rules' => 'trim'
			),
			array(
				'field' => 'bank_details', 
				'label' => 'Bank Details', 
				'rules' => 'trim'
			)
		);
		$this->form_validation->set_rules($config);
	}

	/**
	* validate_add_bank()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add_bank(){
		$config = array(
			array(
				'field' => 'bank', 
				'label' => 'Bank and Branch', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'account_number', 
				'label' => 'Account Number', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}

	/**
	* validate_edit_bank()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_edit_bank(){
		$config = array(
			array(
				'field' => 'account_number', 
				'label' => 'Account Number', 
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file position.php */
/* Location: ./application/controllers/position.php */