<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/user
	 *	- or -
	 * 		http://example.com/index.php/user/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/user/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();

		// load the encryption
		$this->load->library('encrypt');
		// load the form validation
		$this->load->library('form_validation');
		// load the user model
		$this->load->model('User_model');
		// load the user group model
		$this->load->model('User_group_model');
		// load the company model
		$this->load->model('Company_model');
		// load the module model
		$this->load->model('Module_model');
		// load the access role model
		$this->load->model('Access_role_model');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'User';
			$data['page_sub_title'] = 'Add User';
			$data['module_name'] = 'User';
			$data['message'] = '';
			$data['modules'] = '';
			$data['user_groups'] = '';
			$data['companies'] = '';
			$data['message'] = $this->session->flashdata('message');

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the user information
				// expiry
				$expiry = '';
				if($this->input->post('expiry')){
					$expiry = date('Y-m-d', strtotime($this->input->post('expiry')));
				}

				// companies link to user
				$user_companies = $this->input->post('company');
				$total_company_selected = count($user_companies);
				$company_selected = '';
				if($user_companies){
					$cnt = 1;
					foreach($user_companies as $company){
						if($cnt < $total_company_selected){
							$company_selected .= $company.'-';
						} elseif($cnt == $total_company_selected){
							$company_selected .= $company;
						}
						$cnt++;
					}
				}

				$user = array(
					'user_group_id' => $this->input->post('user_group'),
					'username' => $this->input->post('username'),
					'userpassword' => md5($this->input->post('user_password')),
					'company_id' => $company_selected,
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'expiry' => $expiry
				);
				$user_id = $this->User_model->save($user);
				if($user_id){
					// save the default user roles that was define in the user group roles
					$user_group_roles = $this->User_group_model->get_user_group_roles($this->input->post('user_group'));
					if(is_array($user_group_roles)){
						foreach($user_group_roles as $user_group_role){
							$user_role = array(
								'user_id' => $user_id,
								'module_roles_id' => $user_group_role['module_roles_id']
							);
							$this->User_model->save_user_role($user_role);
						}
					}
					$this->session->set_flashdata('message', 'User account successfully created.');
				} else {
					$this->session->set_flashdata('message', 'There was an error in creating the user account. Please contact the System Provider.');
				}

				// redirect to current url
				redirect(current_url());
			}

			// get the user group
			$user_groups = $this->User_group_model->get_list();
			if(is_array($user_groups)){
				$data['user_groups'] = $user_groups;
			}

			// get the companies
			$companies = $this->Company_model->get_list();
			if(is_array($companies)){
				$data['companies'] = $companies;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('user/add');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// variable initialization
			$data['page_title'] = 'User';
			$data['page_sub_title'] = 'List of Users';
			$data['module_name'] = 'User';
			$data['message'] = '';
			$data['users'] = '';
			$data['view_role'] = '';
			$data['edit_role'] = '';

			// view role
			$has_role_view = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'view');
			if($has_role_view || $this->session->userdata('is_admin') == 'yes'){
				$data['view_role'] = 'yes';
			}

			// edit role
			$has_role_edit = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'edit');
			if($has_role_edit || $this->session->userdata('is_admin') == 'yes'){
				$data['edit_role'] = 'yes';
			}

			// get the users
			$users = $this->User_model->get_users('');
			if(is_array($users)){
				$data['users'] = $users;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('user/listing');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* edit()
	* @access public
	* @param none
	* @return none
	*/
	public function edit($id = ''){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is ID passed
			if($id == '')
				redirect(site_url('user/listing'));

			// variable initialization
			$data['page_title'] = 'User';
			$data['page_sub_title'] = 'Edit User';
			$data['module_name'] = 'User';
			$data['message'] = '';
			$data['firstname'] = '';
			$data['lastname'] = '';
			$data['email_address'] = '';
			$data['user_companies'] = '';

			// run the form validation
			$this->validate_edit();
			if($this->form_validation->run() == TRUE){
				$user_companies = $this->input->post('company');
				$total_company_selected = count($user_companies);
				$company_selected = '';
				if($user_companies){
					$cnt = 1;
					foreach($user_companies as $company){
						if($cnt < $total_company_selected){
							$company_selected .= $company.'-';
						} elseif($cnt == $total_company_selected){
							$company_selected .= $company;
						}
						$cnt++;
					}
				}
				// update the user information
				$user = array(
					'id' => $id,
					'company_id' => $company_selected,
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'expiry' => date('Y-m-d', strtotime($this->input->post('expiry')))
				);
				$is_update = $this->User_model->update($user);
				if($is_update){
					$data['message'] = 'User information successfully updated.';
				} else {
					$data['message'] = 'Error in updating the user information.';
				}
			}

			// get the user information
			$users = $this->User_model->get_user_info((int) $id);
			if(is_array($users)){
				$data['firstname'] = $users[0]['firstname'];
				$data['lastname'] = $users[0]['lastname'];
				$data['expiry'] = date('m/d/Y', strtotime($users[0]['expiry']));

				if($users[0]['company_id']){
					$user_companies = explode('-', $users[0]['company_id']);
					foreach($user_companies as $company_selected){
						$company_info = $this->Company_model->get_info($company_selected);
						if(is_array($company_info)){
							$companies[] = array(
								'id' => $company_selected,
								'company_name' => $company_info[0]['company_name']
							);
						}
					}

					$data['user_companies'] = $companies;
				}
			}

			// get the companies
			$companies = $this->Company_model->get_list();
			if(is_array($companies)){
				$data['companies'] = $companies;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('user/edit');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* view()
	* @access public
	* @param none
	* @return none
	*/
	public function view($id = ''){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $is_valid_token == TRUE){
			// check if there is ID passed
			if($id == '')
				redirect(site_url('user/listing'));

			// variable initialization
			$data['page_title'] = 'User';
			$data['page_sub_title'] = 'View User';
			$data['module_name'] = 'User';
			$data['message'] = '';
			$data['username'] = '';
			$data['firstname'] = '';
			$data['lastname'] = '';
			$data['company_name'] = '';
			$data['status'] = '';
			$data['user_group_name'] = '';
			$data['user_roles'] = '';
			$data['user_id'] = $id;
			$not_in = '';
			$data['reset_password_role'] = '';
			$data['deactivate_role'] = '';
			$data['activate_role'] = '';
			$data['roles'] = '';

			// reset password role
			$has_role_reset_password = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'reset');
			if($has_role_reset_password || $this->session->userdata('is_admin') == 'yes'){
				$data['reset_password_role'] = 'yes';
			}

			// deactivate role
			$has_deactivate_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'deactivate');
			if($has_deactivate_role || $this->session->userdata('is_admin') == 'yes'){
				$data['deactivate_role'] = 'yes';
			}

			// activate role
			$has_activate_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), 'activate');
			if($has_activate_role || $this->session->userdata('is_admin') == 'yes'){
				$data['activate_role'] = 'yes';
			}

			// get the user information
			$users = $this->User_model->get_user_info((int) $id);
			if(is_array($users)){
				$data['username'] = $users[0]['username'];
				$data['firstname'] = $users[0]['firstname'];
				$data['lastname'] = $users[0]['lastname'];
				$data['company_name'] = $users[0]['company_name'];
				$data['status'] = $users[0]['status'];
				$data['user_group_name'] = $users[0]['user_group_name'];

				// get the current user roles
				$user_roles = $this->User_model->get_user_roles((int) $id);
				if(is_array($user_roles)){
					$data['user_roles'] = $user_roles;
					$total_user_roles = count($user_roles);
					$cnt = 1;
					foreach($user_roles as $user_role){
						$not_in .= $user_role['module_roles_id'];
						if($cnt < $total_user_roles){
							$not_in .= ',';
						}
						$cnt++;
					}

					// get the roles unassigned
					$roles = $this->Module_model->get_roles_unassigned($not_in);
					if(is_array($roles)){
						$data['roles'] = $roles;
					}
				}

			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');

			// check if user has access to this page
			$has_role = $this->Access_role_model->has_role((int) $this->session->userdata('user_id'), (string) $this->uri->segment(1), (string) $this->uri->segment(2));
			if($has_role || $this->session->userdata('is_admin') == 'yes'){
				$this->load->view('user/view');
			} else {
				$this->load->view('admin/no_access');
			}

			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* remove_role()
	* @access public
	* @param none
	* @retur none
	*/
	public function remove_role(){
		$is_remove = $this->User_model->remove_role((int) $this->input->post('user_role_id'));
		if($is_remove){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* add_role()
	* @access public
	* @param none
	* @return none
	*/
	public function add_role(){
		$user_role = array(
			'user_id' => $this->input->post('user_id'),
			'module_roles_id' => $this->input->post('user_role_id')
		);
		$is_add = $this->User_model->save_user_role($user_role);
		if($is_add){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* change_password()
	* @access public
	* @param none
	* @return none
	*/
	public function change_password(){
		$password = array(
			'id' => (int) $this->input->post('user_id'),
			'userpassword' => md5($this->input->post('password'))
		);
		$is_save = $this->User_model->change_password($password);
		if($is_save){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* deactivate()
	* @access public
	* @param none
	* @return none
	*/
	public function deactivate(){
		$deactivate = array(
			'id' => $this->input->post('user_id'),
			'status' => 'inactive'
		);
		$is_deactivate = $this->User_model->update_account_status($deactivate);
		if($is_deactivate){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* activate()
	* @access public
	* @param none
	* @return none
	*/
	public function activate(){
		$activate = array(
			'id' => $this->input->post('user_id'),
			'status' => 'active'
		);
		$is_deactivate = $this->User_model->update_account_status($activate);
		if($is_deactivate){
			echo 'SUCCESS';
		} else {
			echo 'ERROR';
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'username',
				'label' => 'User Name',
				'rules' => 'required|trim|is_unique[user.username]',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.',
					'is_unique' => 'Sorry. '.$this->input->post('username').' already exist. Please type the %s again.'
				)
			),
			array(
				'field' => 'user_password',
				'label' => 'Password',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'firstname',
				'label' => 'Firstname',
				'rules' => 'trim'
			),
			array(
				'field' => 'lastname',
				'label' => 'Lastname',
				'rules' => 'trim'
			),
			array(
				'field' => 'user_group',
				'label' => 'Account Type',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please select the %s. It is required.'
				)
			),
			array(
				'field' => 'expiry',
				'label' => 'Expiry',
				'rules' => 'trim'
			)
		);
		$this->form_validation->set_rules($config);
	}

	/**
	* validate_edit()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_edit(){
		$config = array(
			array(
				'field' => 'firstname',
				'label' => 'Firstname',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'lastname',
				'label' => 'required|Lastname',
				'rules' => 'trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			),
			array(
				'field' => 'expiry',
				'label' => 'Expiry',
				'rules' => 'required|trim',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */