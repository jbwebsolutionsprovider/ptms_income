<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/role
	 *	- or -  
	 * 		http://example.com/index.php/role/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/role/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();

		// load the form validation
		$this->load->library('form_validation');
		// load the user model
		$this->load->model('User_model');
		// load the activity log model
		$this->load->model('Activity_log_model');
		// load the role model
		$this->load->model('Role_model');
	}

	/**
	* add()
	* @access public
	* @param none
	* @return none
	*/
	public function add(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $this->session->userdata('is_admin') == 'yes' && $is_valid_token == TRUE){

			// variable initialization
			$data['page_title'] = 'Role';
			$data['page_sub_title'] = 'Add Role';
			$data['module_name'] = 'Role';
			$data['message'] = '';

			// run the form validation
			$this->validate_add();
			if($this->form_validation->run() == TRUE){
				// save the role
				$role = array(
					'role_name' => (string) $this->input->post('role_name')
				);
				$is_save = $this->Role_model->save($role);
				if($is_save){

					// store the user logs
					$log = array(
						'user_id' => $this->session->userdata('user_id'),
						'activity' => 'User '.$this->session->userdata('username').' create role name '.$this->input->post('role_name'),
						'action' => 'role',
						'log_time' => date('Y-m-d H:i:s')
					);
					$this->Activity_log_model->save($log);

					$data['message'] = 'Role name successfully saved.';
				} else {
					$data['message'] = 'There was an error found in saving the role name.';
				}
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('role/add');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* listing()
	* @access public
	* @param none
	* @return none
	*/
	public function listing(){
		// please check if user login is admin and has a valid token
		$is_valid_token = $this->User_model->valid_user_token($this->session->userdata('user_id'), $this->session->userdata('token'));
		if($this->session->userdata('logged_in') == TRUE && $this->session->userdata('is_admin') == 'yes' && $is_valid_token == TRUE){

			// variable initialization
			$data['page_title'] = 'Role';
			$data['page_sub_title'] = 'List of Roles';
			$data['module_name'] = 'Role';
			$data['roles'] = '';

			// get the roles
			$roles = $this->Role_model->get_roles();
			if(is_array($roles)){
				$data['roles'] = $roles;
			}

			// load the view files
			$this->load->view('admin/header', $data);
			$this->load->view('admin/sidebar');
			$this->load->view('admin/page-heading');
			$this->load->view('role/listing');
			$this->load->view('admin/footer');
		} else {
			redirect(site_url('login'));
		}
	}

	/**
	* validate_add()
	* @access private
	* @param none
	* @return none
	*/
	private function validate_add(){
		$config = array(
			array(
				'field' => 'role_name', 
				'label' => 'Role Name', 
				'rules' => 'required|trim|is_unique[roles.role_name]',
				'errors' => array(
					'required' => 'Please enter the %s. It is required.',
					'is_unique' => 'Sorry. '.$this->input->post('role_name').' already exist. Please type the %s again.'
				)
			)
		);
		$this->form_validation->set_rules($config);
	}
}

/* End of file role.php */
/* Location: ./application/controllers/role.php */