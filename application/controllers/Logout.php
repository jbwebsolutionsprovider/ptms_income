<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/logout
	 *	- or -  
	 * 		http://example.com/index.php/logout/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/logout/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();

		// load the cart
		$this->load->library('cart');
		// load the activity log model
		$this->load->model('Activity_log_model');
	}

	public function index(){
		// clear cart
		$this->cart->destroy();

		// store the user logs
		$log = array(
			'user_id' => $this->session->userdata('user_id'),
			'activity' => 'User '.$this->session->userdata('username').' logout',
			'action' => 'logout',
			'log_time' => date('Y-m-d H:i:s')
		);
		$this->Activity_log_model->save($log);

		// clear all sessions
		$this->session->sess_destroy();
		redirect(site_url('login'));
	}
}

/* End of file logout.php */
/* Location: ./application/controllers/logout.php */